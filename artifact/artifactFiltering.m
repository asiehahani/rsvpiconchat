%% function artifactFiltering(amplifierStruct,...
%                            dataBufferObject,...
%                            cleanDataBufferObject,...
%                            artifactInfoBufferObject,...
%                            triggerBufferObject,...
%                            fs,...
%                            processingStruct,...
%                            processingStruct.featureExtraction,...
%                            numberOfChannels,...
%                            artifactFilteringParams,...
%                            sessionID);
%
%  gets dataBufferObject, apply artifactRemoval on each sequence and fills
%  cleanDataBufferObject that is cleaned version of dataBuffer. It also
%  gives out artifactInfoBufferObject that has boolean information showing
%  that which samples are detected as contaminated.
%   The inputs of the function:
%          amplifierStruct - A structure that contains the amplifier
%          information.
%
%          dataBufferObject - a dataBuffer class object containing the
%          buffer corresponding to non-trigger data. After the call of the
%          function, the object will be modified using the newly acquired
%          data.
%
%          cleanDataBufferObject - cleaned version of dataBufferObject.
%          Note that lastSampleTimeIndex of these two buffers are different
%          but time Indices are exactly match since we fill cleanDataBuffer
%          each time we processed data buffer and we might be waiting for
%          completeness of a sequence in dataBuffer.
%
%          artifactInfoBufferObject - has Boolean data information. It is
%          exactly syncronized with cleanDataBufferObject.
%          for each sample 0 if good, 1 if contaminated
%
%          triggerBufferObject - a dataBuffer class object containing the
%          buffer corresponding to trigger signal. After the call of the
%          function, the object will be modified using the newly acquired
%          data of the trigger channel.
%
%
%
%   See also dataBuffer, initializeArtifactFiltering, artifactRemoval
%   http://www.sciencedirect.com/science/article/pii/S0165027010003894#
%%
function featureExtractionStruct=artifactFiltering(amplifierStruct,...
    dataBufferObject,...
    cleanDataBufferObject,...
    artifactInfoBufferObject,...
    triggerBufferObject,...
    fs,...
    triggerPartitionerStruct,...
    featureExtractionStruct,...
    numberOfChannels,...
    artifactFilteringParams,...
    rejectSequence,...
    sessionID,...
    enabled)

% disp('Entered ArtifactFiltering Loop')
% artifactInfoBufferObject: for each sample 0 if good, 1 if contaminated
% artifactFilteringParameters;
%               dataMat  =dataBufferObject.getOrderedData;
%               triggerMat =triggerBufferObject.getOrderedData;
% plot(triggerBufferObject.getOrderedData())
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SSVEP & motor imaginary %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%%%%%%%%%%%%%%%%%%
%  RSVP Keyboard %
%%%%%%%%%%%%%%%%%%

persistent lastProcessedSampleTimeIndex;

% for the first run of artifactFiltering function
if isempty(lastProcessedSampleTimeIndex),
    lastProcessedSampleTimeIndex=0;
end
% featureExtractionStruct.rejectSequenceFlag=0;
% if lastProcessedSampleTimeIndex>triggerBufferObject.lastSampleTimeIndex
%     disp('ERROR MSG: lastProcessedSampleTimeIndex>triggerBufferObject.lastSampleTimeIndex')
%     lastProcessedSampleTimeIndex
%     triggerBufferObject.lastSampleTimeIndex
% end

if lastProcessedSampleTimeIndex~=triggerBufferObject.lastSampleTimeIndex && enabled
    %   triggerMat =[0; diff(triggerBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,triggerBufferObject.lastSampleTimeIndex))];
    triggerMat =diff(triggerBufferObject.getOrderedData(lastProcessedSampleTimeIndex,triggerBufferObject.lastSampleTimeIndex));
    if sessionID==1
        %   calibration
        triggerMatSequenceOns=find(triggerMat<=-80);
        trialOns = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
        triggerMatSeqEnds=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
        if ~isempty(triggerMatSeqEnds)
            conventionalTriggerMatSeqEnds=trialOns(find(trialOns<triggerMatSeqEnds(end),1,'last'))+triggerPartitionerStruct.windowLengthinSamples;
            if conventionalTriggerMatSeqEnds<=length(triggerMat)
                triggerMatSeqEnds=conventionalTriggerMatSeqEnds;
            else
                triggerMatSeqEnds=[];
            end
        end
        
        if dataBufferObject.filledStatus
            if length(triggerMatSequenceOns)<1
                cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,dataBufferObject.lastSampleTimeIndex));
                artifactInfoBufferObject.addData(zeros(dataBufferObject.lastSampleTimeIndex-lastProcessedSampleTimeIndex,numberOfChannels));
                lastProcessedSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
%                  disp('a')
                
            elseif (length(triggerMatSequenceOns)==1 && isempty(triggerMatSeqEnds))
                if triggerMatSequenceOns~=1
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,lastProcessedSampleTimeIndex+triggerMatSequenceOns-1));
                    artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns-1,numberOfChannels));
                end
                lastProcessedSampleTimeIndex=lastProcessedSampleTimeIndex+triggerMatSequenceOns-1;
%                  disp('b')
                
            elseif length(triggerMatSequenceOns)>=1 && length(triggerMatSeqEnds)<=length(triggerMatSequenceOns)
                if triggerMatSequenceOns(1)~=1
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,lastProcessedSampleTimeIndex+triggerMatSequenceOns(1)-1));
                    artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns(1)-1,numberOfChannels));
                end
                seqOnTime = lastProcessedSampleTimeIndex+triggerMatSequenceOns(1);
                lastProcessedSampleTimeIndex = lastProcessedSampleTimeIndex+triggerMatSeqEnds(end);
                
                dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
                %                 triggerMat =[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
                triggerMat =diff(triggerBufferObject.getOrderedData(seqOnTime-1,lastProcessedSampleTimeIndex));
                
                %           Calibration
                seqOnNdx=find(triggerMat<=-80);
                
                nonZeroTriggerValues=find((triggerMat)~=0);
                [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
                ndx=ndx+1;
                triggerNdx1=nonZeroTriggerValues(ndx);
                triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
                %             seqEndNdx=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
                triggerNdx=sort([triggerNdx1;triggerNdx2]);
                %   triggerNdx=sort([triggerNdx1;triggerNdx2;seqEndNdx]);
                
                artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
                artifactRemovalDataInput.data=dataMat;
                
                [cleanEEG,artifactInfoMat,eyeBlinkFlag] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
                
                
                cleanDataBufferObject.addData(cleanEEG);
                artifactInfoBufferObject.addData(artifactInfoMat);
                featureExtractionStruct.rejectSequenceFlag=0;
                if rejectSequence
                    if eyeBlinkFlag==1 || (nnz(artifactInfoMat)/numel(artifactInfoMat))>=artifactFilteringParams.contaminatedSamplesPercentage
                        featureExtractionStruct.rejectSequenceFlag=1;
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 1];
                        %                     featureExtractionStruct.completedSequenceCount=0;
                    else
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 0];
                    end
                end
                %     subplot(2,1,1);plot(dataMat);title('rawdata');
                %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
%                  disp('c')
            end
        else
            if length(triggerMatSequenceOns)<1
                cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,dataBufferObject.lastSampleTimeIndex));
                artifactInfoBufferObject.addData(zeros(dataBufferObject.lastSampleTimeIndex-lastProcessedSampleTimeIndex,numberOfChannels));
                lastProcessedSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
%                  disp('a1')
                
            elseif (length(triggerMatSequenceOns)==1 && isempty(triggerMatSeqEnds))
                %             calibration
                triggerMatSequenceOns1=find([0; diff(triggerBufferObject.getOrderedData(1,dataBufferObject.lastSampleTimeIndex))]<=-80);
                if (triggerMatSequenceOns1(end)-1)~=lastProcessedSampleTimeIndex
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,triggerMatSequenceOns1(end)-1));
                    artifactInfoBufferObject.addData(zeros((triggerMatSequenceOns1(end)-1)-lastProcessedSampleTimeIndex,numberOfChannels));
                end
                lastProcessedSampleTimeIndex=triggerMatSequenceOns1(end)-1;
%                  disp('b1')
                
            elseif length(triggerMatSequenceOns)>=1 && length(triggerMatSeqEnds)<=length(triggerMatSequenceOns)
                %           calibration
                triggerMat1=[0; diff(triggerBufferObject.getOrderedData(1,dataBufferObject.lastSampleTimeIndex))];
                triggerMatSequenceOns1=find(triggerMat1<=-80);
                seqOnTime =triggerMatSequenceOns1(triggerMatSequenceOns1>=lastProcessedSampleTimeIndex,1);
                triggerMatSeqEnds1=find(triggerMat1==triggerPartitionerStruct.sequenceEndID);
                trialOns1 = find(0<triggerMat1 & triggerMat1<=triggerPartitionerStruct.sequenceEndID-1);
                
                conventionalTriggerMatSeqEnds1=trialOns1(find(trialOns1<triggerMatSeqEnds1(end),1,'last'))+triggerPartitionerStruct.windowLengthinSamples;
                triggerMatSeqEnds1=conventionalTriggerMatSeqEnds1;
                
                if (seqOnTime-1)~=lastProcessedSampleTimeIndex
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,seqOnTime-1));
                    artifactInfoBufferObject.addData(zeros((seqOnTime-1)-lastProcessedSampleTimeIndex,numberOfChannels));
                end
                lastProcessedSampleTimeIndex = triggerMatSeqEnds1(end);
                dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
                %                 triggerMat =[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
                triggerMat =diff(triggerBufferObject.getOrderedData(seqOnTime-1,lastProcessedSampleTimeIndex));
                %           Calibration
                seqOnNdx=find(triggerMat<=-80);
                nonZeroTriggerValues=find((triggerMat)~=0);
                [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
                ndx=ndx+1;
                triggerNdx1=nonZeroTriggerValues(ndx);
                triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
                seqEndNdx=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
                triggerNdx=sort([triggerNdx1;triggerNdx2]);
                %   triggerNdx=sort([triggerNdx1;triggerNdx2;seqEndNdx]);
                
                artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
                artifactRemovalDataInput.data=dataMat;
                
                [cleanEEG,artifactInfoMat,eyeBlinkFlag] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
                
                cleanDataBufferObject.addData(cleanEEG);
                artifactInfoBufferObject.addData(artifactInfoMat);
                featureExtractionStruct.rejectSequenceFlag=0;
                if rejectSequence
                    if eyeBlinkFlag==1 || (nnz(artifactInfoMat)/numel(artifactInfoMat))>=artifactFilteringParams.contaminatedSamplesPercentage
                        featureExtractionStruct.rejectSequenceFlag=1;
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 1];
                    else
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 0];
                    end
                end
                %     subplot(2,1,1);plot(dataMat);title('rawdata');
                %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
%                  disp('c1')
                
            end
        end
    else % means if sessionID=2,3,4
        %   Spell,copyPhrase,masterytask
        triggerMatSequenceOns=find(triggerMat==triggerPartitionerStruct.fixationID);
        triggerMatSeqEnds=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
        trialOns= find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
        if ~isempty(triggerMatSeqEnds)
            conventionalTriggerMatSeqEnds=trialOns(find(trialOns<triggerMatSeqEnds(end),1,'last'))+triggerPartitionerStruct.windowLengthinSamples;
            if conventionalTriggerMatSeqEnds<=length(triggerMat)
                triggerMatSeqEnds=conventionalTriggerMatSeqEnds;
            else
                triggerMatSeqEnds=[];
            end
        end
        
        if dataBufferObject.filledStatus
            if length(triggerMatSequenceOns)<1
                cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,dataBufferObject.lastSampleTimeIndex));
                artifactInfoBufferObject.addData(zeros(dataBufferObject.lastSampleTimeIndex-lastProcessedSampleTimeIndex,numberOfChannels));
                lastProcessedSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
%                  disp('a')
                
            elseif (length(triggerMatSequenceOns)==1 && isempty(triggerMatSeqEnds))
                if triggerMatSequenceOns~=1
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,lastProcessedSampleTimeIndex+triggerMatSequenceOns-1));
                    artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns-1,numberOfChannels));
                end
                lastProcessedSampleTimeIndex=lastProcessedSampleTimeIndex+triggerMatSequenceOns-1;
%                  disp('b')
                
            elseif length(triggerMatSequenceOns)>=1 && length(triggerMatSeqEnds)<=length(triggerMatSequenceOns)
                if triggerMatSequenceOns(1)~=1
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,lastProcessedSampleTimeIndex+triggerMatSequenceOns(1)-1));
                    artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns(1)-1,numberOfChannels));
                end
                seqOnTime = lastProcessedSampleTimeIndex+triggerMatSequenceOns(1);
                lastProcessedSampleTimeIndex = lastProcessedSampleTimeIndex+triggerMatSeqEnds(end);
                
                dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
                %                 triggerMat =[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
                triggerMat =diff(triggerBufferObject.getOrderedData(seqOnTime-1,lastProcessedSampleTimeIndex));
                %           Spell,copyPhrase,masterytask
                seqOnNdx=find(triggerMat==triggerPartitionerStruct.fixationID);
                
                nonZeroTriggerValues=find((triggerMat)~=0);
                [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
                ndx=ndx+1;
                triggerNdx1=nonZeroTriggerValues(ndx);
                triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
                %             seqEndNdx=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
                triggerNdx=sort([triggerNdx1;triggerNdx2]);
                %   triggerNdx=sort([triggerNdx1;triggerNdx2;seqEndNdx]);
                
                artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
                artifactRemovalDataInput.data=dataMat;
                
                [cleanEEG,artifactInfoMat,eyeBlinkFlag] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
                
                
                cleanDataBufferObject.addData(cleanEEG);
                artifactInfoBufferObject.addData(artifactInfoMat);
                featureExtractionStruct.rejectSequenceFlag=0;
                if rejectSequence
                    if eyeBlinkFlag==1 || (nnz(artifactInfoMat)/numel(artifactInfoMat))>=artifactFilteringParams.contaminatedSamplesPercentage
                        featureExtractionStruct.rejectSequenceFlag=1;
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 1];
                        
                    else
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 0];
                    end
                end
                %     subplot(2,1,1);plot(dataMat);title('rawdata');
                %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
%                  disp('c')
            end
        else
            if length(triggerMatSequenceOns)<1
                cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,dataBufferObject.lastSampleTimeIndex));
                artifactInfoBufferObject.addData(zeros(dataBufferObject.lastSampleTimeIndex-lastProcessedSampleTimeIndex,numberOfChannels));
                lastProcessedSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
%                  disp('a1')
                
            elseif (length(triggerMatSequenceOns)==1 && isempty(triggerMatSeqEnds))
                %             Spell,copyPhrase,masterytask
                triggerMatSequenceOns1=find([0; diff(triggerBufferObject.getOrderedData(1,dataBufferObject.lastSampleTimeIndex))]==triggerPartitionerStruct.fixationID);
                if (triggerMatSequenceOns1(end)-1)~=lastProcessedSampleTimeIndex
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,triggerMatSequenceOns1(end)-1));
                    artifactInfoBufferObject.addData(zeros((triggerMatSequenceOns1(end)-1)-lastProcessedSampleTimeIndex,numberOfChannels));
                end
                lastProcessedSampleTimeIndex=triggerMatSequenceOns1(end)-1;
%                  disp('b1')
                
            elseif length(triggerMatSequenceOns)>=1 && length(triggerMatSeqEnds)<=length(triggerMatSequenceOns)
                %           Spell,copyPhrase,masterytask
                triggerMat1=[0; diff(triggerBufferObject.getOrderedData(1,dataBufferObject.lastSampleTimeIndex))];
                triggerMatSequenceOns1=find(triggerMat1==triggerPartitionerStruct.fixationID);
                seqOnTime =triggerMatSequenceOns1(triggerMatSequenceOns1>=lastProcessedSampleTimeIndex,1);
                triggerMatSeqEnds1=find(triggerMat1==triggerPartitionerStruct.sequenceEndID);                
                trialOns1 = find(0<triggerMat1 & triggerMat1<=triggerPartitionerStruct.sequenceEndID-1);                

                conventionalTriggerMatSeqEnds1=trialOns1(find(trialOns1<triggerMatSeqEnds1(end),1,'last'))+triggerPartitionerStruct.windowLengthinSamples;
                triggerMatSeqEnds1=conventionalTriggerMatSeqEnds1;               
                if (seqOnTime-1)~=lastProcessedSampleTimeIndex
                    cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,seqOnTime-1));
                    artifactInfoBufferObject.addData(zeros((seqOnTime-1)-lastProcessedSampleTimeIndex,numberOfChannels));
                end
                lastProcessedSampleTimeIndex = triggerMatSeqEnds1(end);
                dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
                %                 triggerMat =[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
                triggerMat =diff(triggerBufferObject.getOrderedData(seqOnTime-1,lastProcessedSampleTimeIndex));
                %           Spell,copyPhrase,masterytask
                seqOnNdx=find(triggerMat==triggerPartitionerStruct.fixationID);
                
                nonZeroTriggerValues=find((triggerMat)~=0);
                [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
                ndx=ndx+1;
                triggerNdx1=nonZeroTriggerValues(ndx);
                triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1);
                seqEndNdx=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
                triggerNdx=sort([triggerNdx1;triggerNdx2]);
                %   triggerNdx=sort([triggerNdx1;triggerNdx2;seqEndNdx]);
                
                artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
                artifactRemovalDataInput.data=dataMat;
                
                [cleanEEG,artifactInfoMat,eyeBlinkFlag] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
                
                cleanDataBufferObject.addData(cleanEEG);
                artifactInfoBufferObject.addData(artifactInfoMat);
                featureExtractionStruct.rejectSequenceFlag=0;
                if rejectSequence
                    if eyeBlinkFlag==1 || (nnz(artifactInfoMat)/numel(artifactInfoMat))>=artifactFilteringParams.contaminatedSamplesPercentage
                        featureExtractionStruct.rejectSequenceFlag=1;
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 1];
                    else
                        featureExtractionStruct.rejectSequenceInfo=[featureExtractionStruct.rejectSequenceInfo 0];
                    end
                end
                %     subplot(2,1,1);plot(dataMat);title('rawdata');
                %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
%                  disp('c1')
            end
        end
    end
end
end
%%
% %%%%%%%%%%%%%%%%%
% % RSVP iconCHAT %
% %%%%%%%%%%%%%%%%%
%
% persistent lastProcessedSampleTimeIndex;
% % for the first run of artifactFiltering function
% if isempty(lastProcessedSampleTimeIndex),
% %     if triggerBufferObject.filledStatus
%           lastProcessedSampleTimeIndex=0;
% %         lastProcessedSampleTimeIndex=dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize+1;
% %     else
% %         lastProcessedSampleTimeIndex=dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize+1;
% %     end
% end
%
% triggerMat =[0; diff(triggerBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,triggerBufferObject.lastSampleTimeIndex))];
% % triggerMatSequenceOns=find(triggerMat==triggerValue.sequenceOn);% iconCHAT
% triggerMatSequenceOns=find(triggerMat<=-80);  % RSVP keyboard
%
% if length(triggerMatSequenceOns)<1    % checked
%     cleanDataBufferObject.addData(dataBufferObject.getOrderedData);
%     lastProcessedSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
%     artifactInfoBufferObject.addData(zeros(dataBufferObject.bufferSize,numberOfChannels));
%
% elseif length(triggerMatSequenceOns)==1 &&...
%         (dataBufferObject.bufferSize-triggerMatSequenceOns(end))<=2*seQuenceLength ,  %% checked
%     lastProcessedSampleTimeIndex=(dataBufferObject.lastSampleTimeIndex-(dataBufferObject.bufferSize-(triggerMatSequenceOns(end))))-1;
%     cleanDataBufferObject.addData(dataBufferObject.getOrderedData(dataBufferObject.getOrderedData(dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize+1,lastProcessedSampleTimeIndex)));
%     artifactInfoBufferObject.addData(zeros(lastProcessedSampleTimeIndex-(dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize)+1,numberOfChannels));
%
% elseif length(triggerMatSequenceOns)>=1 && (dataBufferObject.bufferSize-triggerMatSequenceOns(end))>2*seQuenceLength ,
%     seqOnTime = dataBufferObject.lastSampleTimeIndex-(dataBufferObject.bufferSize-triggerMatSequenceOns(1));
%     lastProcessedSampleTimeIndex = dataBufferObject.lastSampleTimeIndex-(dataBufferObject.bufferSize-(triggerMatSequenceOns(end)+2*seQuenceLength));
%
%     dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
%     triggerMat =[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
%
% %     triggerNdx = find((triggerMat>=range1 & triggerMat<=range2) |
% %     triggerMat>=128 ); %iconCHAT
%     seqOnNdx=find(triggerMat<=-80);
%     nonZeroTriggerValues=find((triggerMat)~=0);
%     [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
%     ndx=ndx+1;
%     triggerNdx1=nonZeroTriggerValues(ndx);
%     triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1); %rsvpKEYBOARD
%     triggerNdx=sort([triggerNdx1;triggerNdx2]);
%     seqEndNdx=find(triggerMat==triggerPartitionerStruct.sequenceEndID);
%
%
%
%     artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
%     artifactRemovalDataInput.data=dataMat;
%
%     [cleanEEG,artifactInfoMat] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
%
%
%     cleanDataBufferObject.addData(dataBufferObject.getOrderedData(dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize+1,seqOnTime));
%     cleanDataBufferObject.addData(cleanEEG);
%     cleanDataBufferObject.addData(dataBufferObject.getOrderedData(lastProcessedSampleTimeIndex+1,dataBufferObject.lastSampleTimeIndex));
%     artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns(1),numberOfChannels));
%     artifactInfoBufferObject.addData(artifactInfoMat);
%     %     subplot(2,1,1);plot(dataMat);title('rawdata');
%     %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
%
% elseif length(triggerMatSequenceOns)>=1 && (dataBufferObject.bufferSize-triggerMatSequenceOns(end))<=2*seQuenceLength , %%checked
%
%     seqOnTime = dataBufferObject.lastSampleTimeIndex-(dataBufferObject.bufferSize-triggerMatSequenceOns(1));
%     lastProcessedSampleTimeIndex =(dataBufferObject.lastSampleTimeIndex-(dataBufferObject.bufferSize-(triggerMatSequenceOns(end))))-1;
%
%     dataMat   = dataBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex);
%     triggerMat=[0; diff(triggerBufferObject.getOrderedData(seqOnTime,lastProcessedSampleTimeIndex))];
%
% %     triggerNdx = find((triggerMat>=range1 & triggerMat<=range2) | triggerMat>=128 ); %iconCHAT
%
%     % rsvp keyboard
%     seqOnNdx=find(triggerMat<=-80);
%     nonZeroTriggerValues=find((triggerMat)~=0);
%     [m,ndx]=ismember(seqOnNdx,nonZeroTriggerValues);
%     ndx=ndx+1;
%     triggerNdx1=nonZeroTriggerValues(ndx);
%     triggerNdx2 = find(0<triggerMat & triggerMat<=triggerPartitionerStruct.sequenceEndID-1); %rsvpKEYBOARD
%     triggerNdx=sort([triggerNdx1;triggerNdx2]);
%
%     artifactRemovalDataInput.electrodes=amplifierStruct.channelNames;
%     artifactRemovalDataInput.data=dataMat;
%     [cleanEEG,artifactInfoMat] = artifactRemoval(artifactRemovalDataInput,fs,triggerNdx,artifactFilteringParams,numberOfChannels);
%     cleanDataBufferObject.addData(dataBufferObject.getOrderedData(dataBufferObject.lastSampleTimeIndex-dataBufferObject.bufferSize+1,seqOnTime));
%     cleanDataBufferObject.addData(cleanEEG);
%     artifactInfoBufferObject.addData(zeros(triggerMatSequenceOns(1),numberOfChannels));
%     artifactInfoBufferObject.addData(artifactInfoMat);
%     %     subplot(2,1,1);plot(dataMat);title('rawdata');
%     %     subplot(2,1,2);plot(cleanEEG);title('cleanEEG');
% end
% end
