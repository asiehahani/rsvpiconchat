%% [CleanEEG,artifactInfoMat,eyeBlinkFlag]= artifactRemoval(eeg,srate,triggerNdx,artifactFilteringParams,numberOfChannels)
% artifactRemoval(eeg,srate,triggerNdx,artifactFilteringParams,numberOfChannels)
% consists of 4 steps for removing Artifacts. In each step Parameters were
% estimated for various aspects of data in both the EEG time series and in
% the independent components of the EEG. Outliers form certain threshold
% (defined in artifactFilteringParams) are detected and removed.
%
% 1. Channel rejection & interpolation section:
%  In this step we calculate channel related parameters for each channel.
%  There are two options; one is rejecting outlier channel and leave rest of
%  process working with reduced number of channels. Second is interpolating
%  outlier channels with non-contaminated channels using spherical splines
%  in 10-20 system.
%
% 2. Epoch rejection section:
%  In this step contaminated all channels epochs are detected. Note that we
%  do not have any rejection in this step. We just update artifactInfoMat
%  matrix which shows that which epochs in all channels are contaminated.
%
% 3. Component rejection section:
%  In this step we first apply pca ...........(?!?!?!)
%  Then we just eliminate ICs recognized as sources of artifacts and mix
%  ICs to recover signals again. Note that in this step we do not reject
%  any part of EEG signals. So, we just have artifact reduced signal and
%  artifactInfoMat matrix WILL NOT update in this step.
%
% 4. Channels in epochs rejection & interpolation section:
%  This step is like the first step but just applied on an epoch instead of
%  whole eeg data input. So, we have interpolation using spherical splines
%  and updating artifactInfoMat.
%
%   This code is based on H. Nolan work and full decription of approach can
%   be find in:
%   http://www.sciencedirect.com/science/article/pii/S0165027010003894#
%
%
%   The inputs of the function:
%           eeg - a structure for stroing raw data and electrode locations
%           eeg.data-EEG channels(numberOfSamples x numberOfChannels) matrix
%           eeg.electrodes - same as ampChannelList.electrodeLocations
%           eog_chans:   EOG channels (we dont have it yet,so we construct it by ourselves)
%           srate  : sampling rate
%           trigerNdx: Ndx of stimulus
%
%   The outputs of the function:
%           cleanEEG - cleaned EEG channels(numberOfSamples x numberOfChannels) matrix
%           artifactInfoMat - binary matrix with the same size as input eeg
%                           -for each sample 0 if good, 1 if contaminated
%   See also artifactFiltering, hurst_exponent, h_eeg_interp_spl_m, runica
%   http://www.sciencedirect.com/science/article/pii/S0165027010003894#
%%
function [CleanEEG,artifactInfoMat,eyeBlinkFlag]= artifactRemoval(eeg,srate,triggerNdx,artifactFilteringParams,numberOfChannels)
% N ==> Number of channels (n indicates a specific channel)(numberOfChannels)
% E ==> Number of epochs   (e indicates a specific epoch)
% C ==> Number of ICs      (c indicates a specific IC)

initialData=eeg.data;
artifactInfoMat=zeros(size(initialData,1),numberOfChannels);
eyeBlinkFlag=0;
% build EOG channels:
Fp1Index=find(strcmpi(eeg.electrodes,'Fp1'));
Fp2Index=find(strcmpi(eeg.electrodes,'Fp2'));
Fc1Index=find(strcmpi(eeg.electrodes,'Fc1'));
Fc2Index=find(strcmpi(eeg.electrodes,'Fc2'));
% EOGh1:  Fp1-Fp2
eog_chans(:,1)=initialData(:,Fp1Index)-initialData(:,Fp2Index);
% EOGh2:  Fc1-Fc2
eog_chans(:,2)=initialData(:,Fc1Index)-initialData(:,Fc2Index);
% EOGv1:  Fp1-Fc1
eog_chans(:,3)=initialData(:,Fp1Index)-initialData(:,Fc1Index);
% EOGv2:  Fp2-Fc2
eog_chans(:,4)=initialData(:,Fp2Index)-initialData(:,Fc2Index);

%% -------------------------------------------------------------------------
%%%%%%%%%%%%%
% Filtering %
%%%%%%%%%%%%%
% filter data using equiripple filters btw 1Hz and 95Hz with a notch
% filter at 50 Hz(BW=6 hz)

%% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1)Channel rejection & interpolation section %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   artifactFilteringParams.trh1=3;
%   Parameter 1: the mean correlation coefficient of channel n, where rxn,xm is the Pearson correlation coefficient between channels n and m
X1=initialData;
R=corrcoef(X1);
par1=mean(R);
Z1 = zscore(par1);
%   Parameter 2: variance
par2 = var(X1);
Z2 = zscore(par2);
%   Parameter 3: the hurst exponent of channel n
par3=zeros(1,numberOfChannels);
for n=1:numberOfChannels
    par3(n)= hurst_exponent((X1(:,n))');
end
Z3 = zscore(par3);
Out1=find(abs(Z1)>=artifactFilteringParams.trh1);Out2=find(abs(Z2)>=artifactFilteringParams.trh1);Out3=find(abs(Z3)>=artifactFilteringParams.trh1);
Outlier_Channels=union(union(Out1,Out2),Out3);
%%%% channel dropped detection %%%%
Out1=find(abs(Z1)>=artifactFilteringParams.trh1d);Out2=find(abs(Z2)>=artifactFilteringParams.trh1d);Out3=find(abs(Z3)>=artifactFilteringParams.trh1d);
Outlier_Channels_dropped=union(union(Out1,Out2),Out3);
if (Outlier_Channels_dropped)
%     msgbox(['electrode ' num2str(Outlier_Channels_dropped) ' dropped'],'Warning')
end
%%%%
eegn.data=X1';
eegn.electrodes=eeg.electrodes;
eeg1 = h_eeg_interp_spl_m(eegn,Outlier_Channels);
X1_interpolated=eeg1.data';
artifactInfoMat(:,Outlier_Channels)=1;
% fprintf('Number of rejected channels: %d\n', length(Outlier_Channels));
%  %% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2)Epoch rejection section %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     artifactFilteringParams.trh2=3;
X1= X1_interpolated;
n=0;
% epochLengthAlmost=(triggerNdx(2)-triggerNdx(1));
epochLengthAlmost=70;
triggerNdxLength=length(triggerNdx);
% triggerNdx
par4=zeros(1,triggerNdxLength);par5=zeros(1,triggerNdxLength);par6=zeros(1,triggerNdxLength);
% for i=1:(triggerNdxLength)
%     if  i==triggerNdxLength || (triggerNdx(i+1)-triggerNdx(i))>3*epochLengthAlmost
%         endofEpoch=triggerNdx(i)+2*epochLengthAlmost;
%     else
%         endofEpoch=triggerNdx(i+1)-1;
%     end
for i=1:(triggerNdxLength)
    if i==triggerNdxLength
        endofEpoch=size(initialData,1);   %change later
    elseif (triggerNdx(i+1)-triggerNdx(i))>2*epochLengthAlmost
        endofEpoch=triggerNdx(i)+2*epochLengthAlmost;
    else
        endofEpoch=triggerNdx(i+1)-1;
    end
    e=X1(triggerNdx(i):endofEpoch,:);
    %   baseline correction:
    %   When you epoch the data, you will be given the option to perform baseline correction.
    %   The baseline correction process subtracts the mean prestimulus voltage
    %   (or the voltage over some other range that you specify) from the entire
    %   waveform for each channel in each epoch. This eliminates any overall
    %   voltage offset from the waveform in each epoch.
    e1=e-(mean(e)'*ones(1,size(e,1)))';
    maxe=max(e1,[],1);
    mine=min(e1,[],1);
    n=n+1;
    %   Parameter 4: the amplitude range in epoch e
    par4(n)=mean(maxe-mine);
    %   Parameter 5:The deviation from the channel average in epoch e
    par5(n)=mean(mean(e1)-mean(X1));
    %   Parameter 6: the variance in epoch e
    par6(n)=mean(var(e1));
end
Z4= zscore(par4);
Z5= zscore(par5);
Z6= zscore(par6);
Out4=find(abs(Z4)>=artifactFilteringParams.trh2);Out5=find(abs(Z5)>=artifactFilteringParams.trh2);Out6=find(abs(Z6)>=artifactFilteringParams.trh2);
Outlier_Epochs=union(union(Out4,Out5),Out6);
for j=Outlier_Epochs;
    %     if j==triggerNdxLength || (triggerNdx(j+1)-triggerNdx(j))>2*epochLengthAlmost
    if j==triggerNdxLength
        endofEpoch=size(initialData,1);   %change later
    elseif j==(triggerNdx(j+1)-triggerNdx(j))>2*epochLengthAlmost
        endofEpoch=triggerNdx(j)+2*epochLengthAlmost;
    else
        endofEpoch=triggerNdx(j+1)-1;
    end
    %        X3_modified((k-1)*epoch_length+1:k*epoch_length,:)=epoch1.data';
    %        %warning: change this ,we dont wanna reject anything ,we are gonna
    %        mark them in artifactbufferinfo
    %     X1(triggerNdx(j):endofEpoch,:)=[];     %%%% COMMENT or not?
    artifactInfoMat(triggerNdx(j):endofEpoch,:)=1;
end
%     E_updated=E-length(Outlier_Epochs);
X2_modified=X1;
% if isempty(Outlier_Epochs)
%     CleanEEG.state=0;  % should be rejected sequences has the state 1
% else
%     CleanEEG.state=1;
% end
% fprintf('Number of rejected epochs: %d\n', length(Outlier_Epochs));
%% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%
% (3.1)Do ICA %
%%%%%%%%%%%%%%%
% Ninterpolated=length(Outlier_Channels);
% k=25; L=size(X2_modified,1);
% Cpca=min(floor(sqrt(L/k)),numberOfChannels-Ninterpolated);
%     %   DO PCA
% %   [M,N] = size(data);
% %   mn = mean(data,2);
%     mn = mean(X2_modified',2);
%     data3=data2-(mean(data2)'*ones(1,size(data2,1)))';
X2_modified=X2_modified-(mean(X2_modified)'*ones(1,size(X2_modified,1)))';
%     X2_modified = (X2_modified' - repmat(mn,1,size(X1',2)))';
%     Y = X2_modified / sqrt(size(X2_modified',2)-1);
%     [u,S,PC] = svd(Y);
% %   S = diag(S);
% %   V = S .* S;
%     X3_PCA = PC' * X2_modified';
%     X30=X3_PCA(1:Cpca,:)';
%     X3=[X30 eog_chans(1:size(X30,1),:)];
% ------------------------------------Whitening-----------------------------------------------------------------------------------
X3=X2_modified;
%     [dim, sample]=size(X3');
%     X31 = X3' - mean(X3)' * ones( 1, sample );
%     C=cov(X31');
% %     [E1, D] = eig (C);
%     [E1, D,~] = svd (C);
%     V =(inv (sqrt (D))) * E1';    % has conflict in some sources and book
%     Z = V * X31;
% %   figure, plot(Z(1,:),Z(2,:),'.'); title('first two whitened mixed signal');axis equal;
%     % ------------------------------------------------------------------------------
% SFA
% Y = X3(1:end-1,:)-X3(2 : end,:);
% [U,V,W,C1,S] = gsvd(X3,Y,0);
% extractedSources=U;
% S=extractedSources;
% % ICA
[weights,sphere] = runica(X3','verbose','off');
unmixing_matrix = weights*sphere;
S=unmixing_matrix*X3';
S=S';
%
%% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (3.2)Component rejection section %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     artifactFilteringParams.trh3=3;
%   Parameter 8:  kurtosis of the spatial information in component c
par8= kurtosis(S);
C=size(S,2);
par7=zeros(1,C); % par9=zeros(1,C);
par10=zeros(1,C);par11=zeros(1,C);
for c=1:C
    %   Parameter 7:  the maximum of the absolute correlation coefficient between component c time-course and EOG channels
    f=zeros(1,size(eog_chans,2));
    for v=1:size(eog_chans,2)
        cc= corrcoef(eog_chans(1:size(S,1),v),S(:,c));
        f(v)=cc(1,2);
    end
    par7(c)=max(abs(f(v)));
    %   Parameter 9: the mean slope of the power spectrum of the component c time-course, between the band edges of the low-pass filter band
    %   [Pxx,f] = pwelch(x,window,noverlap,nfft,fs); If you specify window as
    %   the empty vector [], then the signal data is divided into eight segments, and a Hamming window is used on each one.;
    %   If you specify noverlap as the empty vector [], then pwelch determines the segments of x so that there is 50% overlap (default)
%     [Pxx(:,c),freqs] = pwelch((S(:,c))',[],[],srate,srate);
%      [Pxx,freqs] = pwelch((S(:,c))',[],[],srate,srate);
%     par9(c) = mean(diff(10*log10(Pxx(find(freqs>=artifactFilteringParams.lpf1,1):find(freqs<=artifactFilteringParams.lpf2,1,'last'),c))));
%     par9(c) = mean(diff(10*log10(Pxx(find(freqs>=artifactFilteringParams.lpf1,1):find(freqs<=artifactFilteringParams.lpf2)))));
    %   srate => sampling rate
    %   Parameter 10: Hxct  , the Hurst exponent of component c time course
    par10(c)= hurst_exponent((S(:,c))');
    %   Parameter 11: the median slope of the component c time-course
    par11(c)= median(diff((S(:,c))'));
end
Z7 = zscore(par7);
Z8 = zscore(par8);
% Z9 = zscore(par9);
Z10 = zscore(par10);
Z11 = zscore(par11);
Out7=find(abs(Z7)>=artifactFilteringParams.trh3);Out7eyeBlink=find(abs(Z7)>=artifactFilteringParams.trh3eyeBlinkDetector);
Out8=find(abs(Z8)>=artifactFilteringParams.trh3);
Out9=[];% Out9=find(abs(Z9)>=artifactFilteringParams.trh3);
Out10=find(abs(Z10)>=artifactFilteringParams.trh3);Out11=find(abs(Z11)>=artifactFilteringParams.trh3);
Outlier_ICs=union(union(union(Out7,Out8),union(Out9,Out10)),Out11);
if Out7eyeBlink
    eyeBlinkFlag=1;
end
S(:,Out7)=0; S(:,Out8)=0; %S(:,out9)=0; 
S(:,Out10)=0; S(:,Out11)=0;
% fprintf('Number of rejected ICs: %d\n', length(Outlier_ICs));

% SFA
% X3_modified=S*C1*W';
X3_modified=(unmixing_matrix\S')';
%   X3_modified=X3_modified./(ones(size(X3_modified,1),size(X3_modified,2))*(mean(max(X3_modified))./mean(max(X2_modified))));
%% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (4)Channels in epochs rejection & interpolation section %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for i=1:(triggerNdxLength)
%     if i==triggerNdxLength || (triggerNdx(i+1)-triggerNdx(i))>3*epochLengthAlmost
%         endofEpoch=triggerNdx(i)+2*epochLengthAlmost;
%     else
%         endofEpoch=triggerNdx(i+1)-1;
%     end
for i=1:(triggerNdxLength-1)
    if  (triggerNdx(i+1)-triggerNdx(i))>2*epochLengthAlmost
        endofEpoch=triggerNdx(i)+2*epochLengthAlmost;
    else
        endofEpoch=triggerNdx(i+1)-1;
    end
    
    e=X3_modified(triggerNdx(i):endofEpoch,:);
    %        for i=0:epoch_length:(E_updated-1)*epoch_length  %note to change E_updated
    %            e=Y4(i+1:i+epoch_length);
    maxe=max(e);
    mine=min(e);
    %         j=j+1;
    %   Parameter 12:  the variance of channel n in epoch e
    par12=var(e);
    %   Parameter 13: the median slope of the channel n in epoch e
    par13=median(diff(e));
    %   Parameter 14: the amplitude range of channel n in epoch e
    par14=maxe-mine;
    %   Parameter 15: the deviation from the channel average of channel n in epoch e
    par15=(mean(e)-mean(X3_modified));   %%CHECK AGAIN
    Z12 = zscore(par12);Z13 = zscore(par13);Z14 = zscore(par14);Z15 = zscore(par15);
    Out12=find(abs(Z12)>=artifactFilteringParams.trh4);Out13=find(abs(Z13)>=artifactFilteringParams.trh4);Out14=find(abs(Z14)>=artifactFilteringParams.trh4);Out15=find(abs(Z15)>=artifactFilteringParams.trh4);
    Outlier_ChannelsWithinEpochs=union(union(Out12,Out13),union(Out14,Out15));
    artifactInfoMat(triggerNdx(i):endofEpoch,Outlier_ChannelsWithinEpochs)=1;
    epoch.data=e';
    epoch.electrodes=eeg.electrodes;
    epoch1 = h_eeg_interp_spl_m(epoch,Outlier_ChannelsWithinEpochs);
    X3_modified(triggerNdx(i):endofEpoch,:)=epoch1.data';
end
CleanEEG=real(X3_modified);
