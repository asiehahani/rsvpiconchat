function varargout = SignalMonitorGui(varargin)
% SIGNALMONITORGUI MATLAB code for SignalMonitorGui.fig
%      SIGNALMONITORGUI, by itself, creates a new SIGNALMONITORGUI or raises the existing
%      singleton*.
%
%      H = SIGNALMONITORGUI returns the handle to a new SIGNALMONITORGUI or the handle to
%      the existing singleton*.
%
%      SIGNALMONITORGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIGNALMONITORGUI.M with the given input arguments.
%
%      SIGNALMONITORGUI('Property','Value',...) creates a new SIGNALMONITORGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SignalMonitorGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SignalMonitorGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SignalMonitorGui

% Last Modified by GUIDE v2.5 13-Sep-2012 15:45:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SignalMonitorGui_OpeningFcn, ...
    'gui_OutputFcn',  @SignalMonitorGui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SignalMonitorGui is made visible.
function SignalMonitorGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SignalMonitorGui (see VARARGIN)

% Choose default command line output for SignalMonitorGui
SignalMonitorGuiLibDir = '.';
addpath(genpath(SignalMonitorGuiLibDir));
handles.output = hObject;
nargin = length(varargin);
handles.MAX_EEG_CHANNELS_PER_AMP = 16;
if(nargin)
    handles.amplifierStruct = varargin{1};
    handles.recordingFilename = varargin{2};
end

if(nargin)
    update1020locations(handles);
    enableButtons(handles);
    updateAmplifierSerial(handles.amplifierSelector,handles.amplifierStruct.ai.DeviceSerial);
end

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = SignalMonitorGui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resizeGUI(hObject);
% Get default command line output from handles structure
% varargout{1} = handles.output;


% --- Executes on selection change in modeSelector.
function modeSelector_Callback(hObject, eventdata, handles)
% hObject    handle to modeSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modeSelector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modeSelector

% Need to give screen refresh time
switch get(hObject,'Value')
   
    case 2
        updateStateMessage(handles.state_msg,'Push EEG Display to START');
        set(handles.EEGDisplay,'Visible','on');
        set(handles.Quit,'Enable','off');
        set(handles.Continue,'Enable','off');
end
% Update handles structure
guidata(hObject, handles);

% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes during object creation, after setting all properties.
function LocationChannel1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LocationChannel5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LocationChannel11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LocationChannel12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function LocationChannel13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LocationChannel14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LocationChannel15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LocationChannel16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LocationChannel16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function modeSelector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modeSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function amplifierSelector_CreateFcn(hObject, eventdata, ~)
% hObject    handle to amplifierSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function scaleSelector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scaleSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function state_msg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to state_msg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in channel1_button.
function channel1_button_Callback(hObject , eventdata, handles)
% hObject    handle to channel1_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 1;
channelControl(handles,channelIndex);

% --- Executes on button press in channel2_button.
function channel2_button_Callback(hObject , eventdata, handles)
% hObject    handle to channel2_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)val=get(hObject ,'Value');
channelIndex =2;
channelControl(handles,channelIndex); 

% --- Executes on button press in channel3_button.
function channel3_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel3_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 3;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel4_button.
function channel4_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel4_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)val=get(hObject ,'Value');
channelIndex = 4;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel5_button.
function channel5_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel5_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 5;
channelControl(handles,channelIndex); 

% --- Executes on button press in channel6_button.
function channel6_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel6_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 6;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel7_button.
function channel7_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel7_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 7;
channelControl(handles,channelIndex); 

% --- Executes on button press in channel8_button.
function channel8_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel8_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 8;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel9_button.
function channel9_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel9_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 9;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel10_button.
function channel10_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel10_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 10;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel11_button.
function channel11_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel11_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 11;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel12_button.
function channel12_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel12_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 12;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel13_button.
function channel13_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel13_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 13;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel14_button.
function channel14_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel14_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 14;
channelControl(handles,channelIndex); 


% --- Executes on button press in channel15_button.
function channel15_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel15_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
channelIndex = 15;
channelControl(handles,channelIndex); 

% --- Executes on button press in channel16_button.
function channel16_button_Callback(hObject, eventdata, handles)
% hObject    handle to channel16_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)val=get(hObject ,'Value');
channelIndex = 16;
channelControl(handles,channelIndex); 

% --- Executes on button press in channel_all_switch.
function channel_all_switch_Callback(hObject, eventdata, handles)
% hObject    handle to channel_all_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Select all/none channels
currentAmplifier2display = get(handles.amplifierSelector,'Value');
channelNdx = handles.amplifierStruct.channelBeginIndices(currentAmplifier2display):handles.amplifierStruct.channelEndIndices(currentAmplifier2display);
% GUImode = get(handles.modeSelector,'Value');
channelNdx = 1:handles.MAX_EEG_CHANNELS_PER_AMP;
ONstate=get(handles.channel_all_switch,'Value');
if(ONstate)
    set(handles.channel_all_switch,'String','Select None')
    enableButtons(handles,1);
    channelControl(handles,channelNdx);
    for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP
        changeBackgroundColor(eval(['handles.LocationChannel',num2str(ii)]),[1 1 1]);
    end
else
    set(handles.channel_all_switch,'String','Select All')
    enableButtons(handles,0);
    for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP
        changeBackgroundColor(eval(['handles.LocationChannel',num2str(ii)]),[0.5 0.5 0.5]);
        
    end
end

% --- Executes on button press in EEGDisplay.
function EEGDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to EEGDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% updateSateMessage(handles.state_msg,'Push EEG Display to START'); 
DisplayMode(handles);


% --- Executes on button press in Quit.
function Quit_Callback(hObject, eventdata, handles)
% hObject    handle to Quit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

assignin('base','GUIrunning',false);
close all;
clear;


% --- Executes on selection change in scaleSelector.
function scaleSelector_Callback(hObject, eventdata, handles)
% hObject    handle to scaleSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns scaleSelector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from scaleSelector


% --- Executes on selection change in amplifierSelector.
function amplifierSelector_Callback(hObject, eventdata, handles)
% hObject    handle to amplifierSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
NUMBER_REFERENCE_CHANNEL = 4;
amplifier2display = get(hObject,'Value');
enableButtons(handles);
update1020locations(handles);
% update impedances if it has been fetched before
if(~sum(cellfun(@isempty,handles.impedances)))
    for ii = 1:handles.amplifierStruct.numberOfChannels(amplifier2display)
        eval(['handlesImpedance(',num2str(ii),') = handles.ImpedanceChannel',num2str(ii),';']);
        eval(['handlesLocationChannel(',num2str(ii),') = handles.LocationChannel',num2str(ii),';']);
    end
    for ii = 1:NUMBER_REFERENCE_CHANNEL
        eval(['handlesReferenceChannel(',num2str(ii),') = handles.ref',num2str(ii),';']);
    end
    
    color = [0.5 0.5 0.5];
    for ii = handles.amplifierStruct.numberOfChannels(amplifier2display)+1:handles.MAX_EEG_CHANNELS_PER_AMP;
        
        eval(['set(handles.LocationChannel',num2str(ii),',''backgroundcolor'',color);']);
    end
end


% --- Executes on button press in Continue.
function Continue_Callback(hObject, eventdata, handles)
% hObject    handle to Continue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base','continueMainBCIFlag',true);
assignin('base','GUIrunning',false);
close all;

