%% resizeGUI(hObject);
%Resize GUI to fill the most of the screen area
%   Inputs to the function handle to GUI.
%   NOTE: This function will only resize main GUI. Every item in the GUI
%   retain its size and location unless under the proptery 'Units' is set to
%   be 'Normalized'.
%%

function resizeGUI(hObject)
GUIverticalStartLocation = 30; % in pixels. 1 corresponds to pixels at the bottom on the screen
                               % Needed to ensure no part GUI is hidden
                               % under task bar
reduceHeightofGUI = 50;        % in pixels. Reduces heigt of gui from verical resolution to make sure GUI 
                               % fits properly.


screenResolution = get(0,'screenSize');
set(hObject,'Units','Pixels');
set(hObject,'position',[screenResolution(1),screenResolution(2)+GUIverticalStartLocation,...
                        screenResolution(3),screenResolution(4)-reduceHeightofGUI]);