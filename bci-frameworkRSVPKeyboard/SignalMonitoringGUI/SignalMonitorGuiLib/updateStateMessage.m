%% updateStateMessge
%Upates the state of GUI in the text box.
%   Input : handlesStateMsg,message
%%
function updateStateMessage(handleStateMsg,message)
    set(handleStateMsg,'String',message);
end