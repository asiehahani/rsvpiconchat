%% update1020locations(handles)
%Updates 10-20 locations on GUI for connected channels and display NC for
%   not connected channels.
%   Input : handles structure to GUI object
%%
function update1020locations(handles)
amplifier2display = get(handles.amplifierSelector,'Value');
for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP % check with two amps
    if(ii<=handles.amplifierStruct.numberOfChannels(amplifier2display))
        eval(['set(handles.LocationChannel',num2str(ii),',''String'',handles.amplifierStruct.ai(',num2str(amplifier2display),').Channel(',num2str(ii),').ChannelName);']);
    else
        eval(['set(handles.LocationChannel',num2str(ii),',''String'',''NC'');']);
    end
end