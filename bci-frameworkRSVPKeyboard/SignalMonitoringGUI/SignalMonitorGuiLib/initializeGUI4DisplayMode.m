%% initializeGUI4DisplayMode(handles)
%Intializes GUI panel, makes required buttons visible and start amplifiers
%   Input : handles to object
%   Output : data2display,pointer2lastStoredSample
%%
function [data2display,pointer2lastStoredSample] = initializeGUI4DisplayMode(handles)
global plotHandle,
global lineHandle,
global newDataNdx,

seconds2Display = 10;

updateStateMessage(handles.state_msg,'Displaying EEG Signals');
set(handles.EEGDisplay,'String','Stop EEG Display');

currentAmplifier2display = get(handles.amplifierSelector,'Value');

 % put an edit button for taking input
sampleRate = handles.amplifierStruct.ai(currentAmplifier2display).SampleRate;
numberOfDisplaySamples = sampleRate*seconds2Display;

data2display = zeros(numberOfDisplaySamples,handles.amplifierStruct.totalNumberOfChannels);
plotHandle = [];
lineHandle = [];
newDataNdx = 1;
[success]=startAmps(handles.amplifierStruct,[handles.recordingFilename,'_guiData'],'Index');

end