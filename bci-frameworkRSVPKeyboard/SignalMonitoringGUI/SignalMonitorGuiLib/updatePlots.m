%% updatePlots(handles,data2display,YScalePerDivision)
%update plots of signals,showing signals of selected amplifier and channels
%   update channel locations of selected channels. 
%   Input : handles structure to GUI object,data2display,YscalePerDivision
%%

function updatePlots(handles,data2display,YScalePerDivision)
global plotHandle,
global lineHandle,
persistent currentAmplifier2display;
global newDataNdx;


numberOfDisplaySamples = length(data2display);
onChannelCounter = 0;

%Flip channel names for ytickLabels % YLables = getYLables(handles.amplifeirStruct.channelNames)
if(~isempty(currentAmplifier2display))
    previousAmplifier2display = currentAmplifier2display;
end
currentAmplifier2display = get(handles.amplifierSelector,'Value');
channelNdx = handles.amplifierStruct.channelBeginIndices(currentAmplifier2display):handles.amplifierStruct.channelEndIndices(currentAmplifier2display);
channelNames = handles.amplifierStruct.channelNames(channelNdx);

for ii = 1:length(channelNdx)
    eval(['ONstate(1,',num2str(ii),')=get(handles.channel',num2str(ii),'_button,''Value'');']);
end
Cndx = sum(ONstate):-1:1;
ONchannels = fliplr(find(ONstate));

if(~isempty(ONchannels))
    YLabels = cell(1,length(ONchannels)+2);
    for ii = 1:length(ONchannels)
        YLabels{1,ii+1} = channelNames{1,ONchannels(ii)};
    end
else
    YLabels = {' ',' '};
end
% First Iteration
if(isempty(plotHandle))
    

    numberOfChannels = handles.amplifierStruct.numberOfChannels(currentAmplifier2display);
    traceLineLoc = newDataNdx;
    if(sum(ONstate))
        hold off, plotHandle = plot(data2display(:,channelNdx)+repmat(length(ONstate):-1:1,numberOfDisplaySamples,1)); hold on,
    
    lineHandle=line([traceLineLoc traceLineLoc],[0 sum(ONstate)+1],'Color','r');
    for ii = 1:numberOfChannels;
        line([0 numberOfDisplaySamples],[ii ii],'Color',[0.75 0.75 0.75]);
    end
    for ii = 1:numberOfChannels
        set(plotHandle(ii),'YDataSource','data2display(:,ii)'); % make it for the slave amplifier too - shalini
    end
    ylim([0 sum(ONstate)+1]);
    set(gca,'YTick',0:sum(ONstate)+1);
    set(gca,'YTickLabel',sum(ONstate)+1:-1:0);
    xlabel('Seconds');
    ylabel('Display channels');
    xlim([0,numberOfDisplaySamples]);
    box on,
    end
else
    currentAmplifier2display = get(handles.amplifierSelector,'Value');
    if (previousAmplifier2display == currentAmplifier2display)
        traceLineLoc = newDataNdx;
        delete(lineHandle),
        lineHandle=line([traceLineLoc traceLineLoc],[0 sum(ONstate)+1],'Color','r');
        for ii=1: length(handles.amplifierStruct.channelBeginIndices(currentAmplifier2display):handles.amplifierStruct.channelEndIndices(currentAmplifier2display))
            if(ONstate(1,ii))
                onChannelCounter = onChannelCounter+1;
                set(plotHandle(ii),'XDataSource','[1:numberOfDisplaySamples]');
                set(plotHandle(ii),'YDataSource','data2display(:,handles.amplifierStruct.channelBeginIndices(currentAmplifier2display)+ii-1)+repmat(Cndx(onChannelCounter),numberOfDisplaySamples,1)');
                refreshdata(plotHandle(ii),'caller');
            else
                set(plotHandle(ii),'XDataSource','0');
                set(plotHandle(ii),'YDatasource','0');
                refreshdata(plotHandle(ii),'caller');
            end
        end
        axis([0 numberOfDisplaySamples 0 sum(ONstate)+1]);
        if(~isempty(ONchannels))
            set(gca,'YTickLabel',[ONchannels(1)-1 ONchannels ONchannels(end)+1]);
            set(gca,'YTickLabel',YLabels);
        end
        hold on,
    else
        delete(plotHandle);
        plotHandle=[];
    end
end