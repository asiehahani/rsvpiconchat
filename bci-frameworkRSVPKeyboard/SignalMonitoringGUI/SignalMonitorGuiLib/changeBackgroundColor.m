%% changeBackgroundColor(handle2object,color)
%Changes backgound color of an object to the specified color
%   Inputs : handle2object
%   color ([R G B] format)
%%
function changeBackgroundColor(handle2object,color)
    set(handle2object,'BackgroundColor',color);
end