%% DisplayMode(handles)
%DisplayMode function checks the value of 'Show EEG' button on the GUI.
%   If it is ON: This function starts EEG amplifier(s) and display signals
%   If it is OFF: this function stops EEG amplifer(s).
%   Input : handles to GUI objects.
%%

function DisplayMode(handles)

set(handles.scaleSelector,'Visible','on');
set(handles.scaleSelectorLabel,'Visible','on');

if(get(handles.EEGDisplay,'Value'))
    [data2display] = initializeGUI4DisplayMode(handles);
    while(get(handles.EEGDisplay,'Value'))
        if(strcmpi(handles.amplifierStruct.ai.Running,'off')),
            break,
        end
        [~,rawData,~]=getAmpsData(handles.amplifierStruct);
        if(~isempty(rawData))
            [YScalePerDivision] = getSignalScalingFactor(handles.scaleSelector);
            [data2display] = bufferData(data2display,rawData,YScalePerDivision);
            pause(0.05), % It is needed for screen refresh time - shalini
            updatePlots(handles,data2display,YScalePerDivision);
        else
            pause(0.1),
        end
    end
else
    [success]=stopAmps(handles.amplifierStruct);
    updateStateMessage(handles.state_msg,'Display Stopped');
    set(handles.EEGDisplay,'String','Start EEG Display');
    set(handles.EEGDisplay,'Visible','off');
    set(handles.scaleSelector,'Visible','off');
    set(handles.scaleSelectorLabel,'Visible','off');
    set(handles.Quit,'Enable','on');
    set(handles.Continue,'Enable','on');
    hold off, clearvars plotHandle lineHandle
end