%%updateAmplifierInfo
% Update Serial Number of the amplifier on GUI panel
% Input: Device serial
function updateAmplifierSerial(handleAmplifierSelector,deviceSerial)
set(handleAmplifierSelector,'String',deviceSerial);