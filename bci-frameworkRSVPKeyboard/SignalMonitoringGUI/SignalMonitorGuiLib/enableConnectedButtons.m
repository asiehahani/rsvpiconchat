%%enableButtons(handles)
%Enable/Disable channel control buttons
% Input : handles structure to GUI object
function enableButtons(handles)
amplifier2display = get(handles.amplifierSelector,'Value');
for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP % check with two amps
    if(ii<=handles.amplifierStruct.numberOfChannels(amplifier2display))
        eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''on'');']);
    else
        eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''off'');']);
    end
end