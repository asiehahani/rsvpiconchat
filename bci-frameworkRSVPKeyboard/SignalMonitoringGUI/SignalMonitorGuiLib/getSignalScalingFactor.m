%% getSignalScalingFactor(handlesScaleSelector)
%Scales options of ytick division
%   Inputs : handlesScaleSelector
%%

function [YScalePerDivision] = getSignalScalingFactor(handlesScaleSelector)
scaleNdx = get(handlesScaleSelector,'Value');
switch(scaleNdx)
    case 1
        YScalePerDivision = 100e-6;
    case 2
        YScalePerDivision = 5e-6;
    case 3
        YScalePerDivision = 10e-6;
    case 4
        YScalePerDivision = 20e-6;
    case 5
        YScalePerDivision = 50e-6;
    case 6
        YScalePerDivision = 100e-6;
    case 7
        YScalePerDivision = 200e-6;
    case 8
        YScalePerDivision = 500e-6;
    case 9
        YScalePerDivision = 1e-3;
    case 10
        YScalePerDivision = 2e-3;
    case 11
        YScalePerDivision = 5e-3;
end