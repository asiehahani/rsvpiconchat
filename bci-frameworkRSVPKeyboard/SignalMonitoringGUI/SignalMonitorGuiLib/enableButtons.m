%% enableButtons(handles)
%Enable/Disable channel control buttons
%   Input : handles structure to GUI object,Onstate
%%
function enableButtons(handles,ONstate)
amplifier2display = get(handles.amplifierSelector,'Value');
channelNdx = handles.amplifierStruct.channelBeginIndices(amplifier2display):handles.amplifierStruct.channelEndIndices(amplifier2display);
switch nargin
    case 1
        for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP % check with two amps
            if(ii<=handles.amplifierStruct.numberOfChannels(amplifier2display))
                eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''on'');']);
                eval(['set(handles.channel',num2str(ii),'_button,''Value'',1);']);
            else
                eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''off'');']);
                eval(['set(handles.channel',num2str(ii),'_button,''Value'',0);']);
            end
        end
        set(handles.channel_all_switch,'Value',1);
        set(handles.channel_all_switch,'String','Select None'); 
    case 2
        if(~ONstate)
            for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP
                eval(['set(handles.channel',num2str(ii),'_button,''Value'',0);']);
            end
        else
            for ii = 1:length(channelNdx)
                eval(['set(handles.channel',num2str(ii),'_button,''Value'',1);']);
            end
        end
end