%% [data2display] = bufferData(data2display,rawData,YScalePerDivision)
%Buffers data fetched from ampliifers in circular fashion
%   Input : data2display , rawData , YScalePerDivision
%   Output : data2display (Data buffer)
%%
function [data2display] = bufferData(data2display,rawData,YScalePerDivision)
global newDataNdx;
if(isempty(newDataNdx))
    newDataNdx = 1;
end
numberFetchedSamples = size(rawData,1);
numberOfDisplaySamples = size(data2display,1);
if(numberFetchedSamples<numberOfDisplaySamples)
    L = length(data2display)-newDataNdx+1;
    if(L>=  numberFetchedSamples)
        data2display(newDataNdx:newDataNdx+numberFetchedSamples-1,:)=rawData/YScalePerDivision;
        newDataNdx = newDataNdx+numberFetchedSamples;
    else
        if(L)
            data2display(newDataNdx:end,:)=rawData(1:L,:)/YScalePerDivision;
            data2display(1:numberFetchedSamples-L,:)=rawData(L+1:end,:)/YScalePerDivision;
            newDataNdx = numberFetchedSamples-L+1;
        else
            data2display(1:numberFetchedSamples,:)=rawData/YScalePerDivision;
            newDataNdx = numberFetchedSamples+1;
        end
    end
else
   data2display(:,:) =rawData(end-(numberOfDisplaySamples)+1:end,:)/YScalePerDivision;
   newDataNdx = 1;
end