%%enableConnectedButtos(handles)
%Enables connected buttons
% Input : handles structure to GUI object
function enableConnectedButtons(handles)
amplifier2display = get(handles.amplifierSelector,'Value');
for ii = 1:length(handles.amplifierStruct.numberOfChannels(amplifier2display))
    eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''on'');']);
end