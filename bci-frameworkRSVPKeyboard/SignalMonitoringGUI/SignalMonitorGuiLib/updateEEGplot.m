%% updateEEGplot
%updateEEGplot function checks the value of 'Show EEG' button on the GUI.
% If it is ON: This function starts EEG amplifier(s) and display signals
% If it is OFF: this function stops EEG amplifer(s).
% Input : handles to GUI objects.

function updateEEGDisplay(handles)
set(handles.EEGDisplay,'Visible','on');
set(handles.scaleSelector,'Visible','on');
set(handles.scaleSelectorLabel,'Visible','on');
amplifier2display = get(handles.amplifierSelector,'Value');

if(get(handles.EEGDisplay,'Value'))
    updateStateMessage(handles.state_msg,'Displaying EEG Signals');    
    set(handles.EEGDisplay,'String','Stop EEG Display');
    
    seconds2Display = 10; % put an edit button for taking input
    sampleRate = handles.amplifierStruct.ai(amplifier2display).SampleRate;
    numberOfDisplaySamples = sampleRate*seconds2Display;
    
    data2display = zeros(numberOfDisplaySamples,handles.amplifierStruct.totalNumberOfChannels);
    plotHandle = [];
    status = [amplifier2display];
    pointer2lastStoredSample = 1; 
    
    [success]=startAmps(handles.amplifierStruct,[handles.recordingFilename,'_guiData'],'Index');
    
    pause(1);
    
    while(get(handles.EEGDisplay,'Value'))
        amplifier2display = get(handles.amplifierSelector,'Value');
        status = [status amplifier2display];
        numberOfChannels = handles.amplifierStruct.numberOfChannels(amplifier2display);
        if(amplifier2display-1)
            % numberOfChannelsPrecedingAmpifier = length(handles.amplifierStruct.ai(amplifier2display-1).Channel);
            % channelNdx = (amplifier2display-1)*numberOfChannelsPrecedingAmpifier+1:amplifier2display*numberOfChannels;
            channelNdx = (amplifier2display-1)*handles.numberOfChannelsPerAmp((amplifier2display-1)):handles.numberOfChannelsPerAmp(amplifier2display);
        else
            channelNdx = 1:numberOfChannels;
        end
        tic,
        scaleNdx = get(handles.scaleSelector,'Value');
        switch(scaleNdx)
            case 1
                YscalePerDivision = 100e-6;
            case 2
                YscalePerDivision = 50e-6;
            case 3
                YscalePerDivision = 10e-6;
            case 4
                YscalePerDivision = 20e-6;
            case 5
                YscalePerDivision = 50e-6;
            case 6
                YscalePerDivision = 100e-6;
            case 7
                YscalePerDivision = 200e-6;
            case 8
                YscalePerDivision = 500e-6;
            case 9
                YscalePerDivision = 1e-3;
            case 10
                YscalePerDivision = 2e-3;
            case 11
                YscalePerDivision = 5e-3;
        end
        if(strcmpi(handles.amplifierStruct.ai.Running,'off')),
            break,
        end
        [success,rawData,triggerData]=getAmpsData(handles.amplifierStruct);
        
        numberFetchedSamples = size(rawData,1);
        for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP
            eval(['val(1,',num2str(ii),')=get(handles.channel',num2str(ii),'_button,''Value'');']);
        end
        if(exist('channelNdx'))
            clear channelNdx,
        end
            channelNdx = handles.amplifierStruct.channelBeginIndices(amplifier2display):handles.amplifierStruct.channelEndIndices(amplifier2display);
            channelNdx = channelNdx(val==1);
        if(~isempty(rawData))   
            if(numberFetchedSamples<numberOfDisplaySamples)
            L = length(data2display)-pointer2lastStoredSample+1;
            if(L>=  numberFetchedSamples)
                data2display(pointer2lastStoredSample:pointer2lastStoredSample+numberFetchedSamples-1,:)=rawData/YscalePerDivision;
                pointer2lastStoredSample = pointer2lastStoredSample+numberFetchedSamples;
            else
                if(L)
                    data2display(pointer2lastStoredSample:end,:)=rawData(1:L,:)/YscalePerDivision;
                    data2display(1:numberFetchedSamples-L,:)=rawData(L+1:end,:)/YscalePerDivision;
                    pointer2lastStoredSample = numberFetchedSamples-L+1;
                else
                    data2display(1:numberFetchedSamples,:)=rawData/YscalePerDivision;
                    pointer2lastStoredSample = numberFetchedSamples+1;
                end
            end
            else
%                 data2display(:,:) =rawData(end-(numberFetchedSamples-numberOfDisplaySamples)+1:end,:)/YscalePerDivision;
            end
            
            
                        if(isempty(plotHandle))
                hold off,
                traceLineLoc = numberFetchedSamples;
                plotHandle = plot(data2display(:,channelNdx)+repmat(sum(val):-1:1,numberOfDisplaySamples,1)); 
                hold on,
                lineHandle=line([traceLineLoc traceLineLoc],[0 17],'Color','r');
                for ii = 1:numberOfChannels;
                    line([0 numberOfDisplaySamples],[ii ii],'Color',[0.75 0.75 0.75]);
                end
                for ii = 1:numberOfChannels
                    set(plotHandle(ii),'YDataSource','data2display(:,ii)');
                end
                ylim([0 sum(val)+1]);
                set(gca,'YTick',0:sum(val)+1);
                set(gca,'YTickLabel',sum(val)+1:-1:0);
%                 axis([1 numberOfDisplaySamples 0 17]);
                xlabel('Seconds');
                ylabel(num2str(YscalePerDivision));
                xlim([0,numberOfDisplaySamples]);
                box on,
            else
                 if (status(end) == (status(end-1)))
                traceLineLoc = pointer2lastStoredSample;
                delete(lineHandle);
                lineHandle=line([traceLineLoc traceLineLoc],[0 17],'Color','r');
                onChannelCounter = 0;
                Cndx = sum(val):-1:1;
                %                 channelNdx
                for ii = 1:handles.MAX_EEG_CHANNELS_PER_AMP
                    eval(['val(1,',num2str(ii),')=get(handles.channel',num2str(ii),'_button,''Value'');']);
                end
                for ii=1: length(handles.amplifierStruct.channelBeginIndices(amplifier2display):handles.amplifierStruct.channelEndIndices(amplifier2display))
                    if(val(1,ii))
                        onChannelCounter = onChannelCounter+1;
                        set(plotHandle(ii),'XDataSource','[1:numberOfDisplaySamples]');
                        set(plotHandle(ii),'YDataSource','data2display(:,handles.amplifierStruct.channelBeginIndices(amplifier2display)+ii-1)+repmat(Cndx(onChannelCounter),numberOfDisplaySamples,1)');
                        refreshdata(plotHandle(ii),'caller');
                    else
                        set(plotHandle(ii),'XDataSource','0');
                        set(plotHandle(ii),'YDatasource','0');
                        refreshdata(plotHandle(ii),'caller');
                    end
                end
                axis([0 numberOfDisplaySamples 0 sum(val)+1]);
                set(gca,'YTickLabel',sum(val)+1:-1:0);
                set(gca,'YTickLabel',sum(val)+1:-1:0);
                hold on,
                else
                    delete(plotHandle);
                    plotHandle=[];
                end
            end

        else
            pause(0.1),
            
        end
    end
else
    [success]=stopAmps(handles.amplifierStruct);
    updateStateMessage(handles.state_msg,'Display Stopped');
    set(handles.EEGDisplay,'String','Start EEG Display');
    set(handles.EEGDisplay,'Visible','off');
    set(handles.scaleSelector,'Visible','off');
    set(handles.scaleSelectorLabel,'Visible','off');
    hold off, clearvars plotHandle lineHandle
end