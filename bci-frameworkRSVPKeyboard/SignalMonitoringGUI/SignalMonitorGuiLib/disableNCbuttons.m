%% disableNCbuttons(handles)
%Disables Not connected buttons
%   Input : handles structure to GUI object
%%
function disableNCbuttons(handles)
amplifier2display = get(handles.amplifierSelector,'Value');
for ii = handles.amplifierStruct.numberOfChannels(amplifier2display)+1:handles.MAX_EEG_CHANNELS_PER_AMP % check with two amps
    eval(['set(handles.channel',num2str(ii),'_button,''Enable'',''off'');']);
end