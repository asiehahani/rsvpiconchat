%% channelControl(handlesButton,handlesModeSelector,channelIndex)
%Changes background color of the location button to indicate whether
%channel is ON or OFF.
%   Inputs : handles to button and modeSelector, channelIndex
%%

function channelControl(handles,channelIndex)

for ii = 1:length(channelIndex)
    ONstate = eval(['get(handles.channel',num2str(channelIndex(ii)),'_button,''Value'');']);
    if(ONstate)
        eval(['set(handles.LocationChannel',num2str(channelIndex(ii)),',''BackgroundColor'',[1 1 1]);']);
    else
        eval(['set(handles.LocationChannel',num2str(channelIndex(ii)),',''BackgroundColor'',[0.5 0.5 0.5]);']);
    end
end