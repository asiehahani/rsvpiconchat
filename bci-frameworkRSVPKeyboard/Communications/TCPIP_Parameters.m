%% TCP/IP Parameters
%Contains user adjustable parameter file corresponding to signal
% processing.
%    
%    It creates the following structure,
%    BCIpacket : Specifies header to be used for different packet types and
%                and terminator that MATLAB appends to the data.
%    TCPIPparams : TCP/IP parameters Input/Outpur buffer Size
%

% Packet types used 
if(0)
BCIpacket.HDR.START = 1;     % STARTS PRESENTATION
BCIpacket.HDR.PAUSE = 2;     % PAUSES PRESENTATION
BCIpacket.HDR.STOP = 3;      % STOPS PRESENTATION
BCIpacket.HDR.DECISION = 4;  % SENDS DECISION
BCIpacket.HDR.PARAMETERS = 5;% Presentation parameters
BCIpacket.HDR.TARGET = 6;     % Target ID
BCIpacket.HDR.PROBABILITIES = 7; % Language model probabilites
BCIpacket.HDR.STATE = 8;     % 
BCIpacket.HDR.MESSAGE = 9;   % 
BCIpacket.HDR.SIGNAL = 10;    % Send EEG signals
BCIpacket.HDR.ROBOT = 11;     % Controls Robot
else
BCIpacket.HDR.START = '01';     % STARTS PRESENTATION
BCIpacket.HDR.PAUSE = '02';     % PAUSES PRESENTATION
BCIpacket.HDR.STOP = '03';      % STOPS PRESENTATION
BCIpacket.HDR.DECISION = '04';  % SENDS DECISION
BCIpacket.HDR.PARAMETERS = '05';% Presentation parameters
BCIpacket.HDR.TARGET = '06';     % Target ID
BCIpacket.HDR.PROBABILITIES = '07'; % Language model probabilites
BCIpacket.HDR.STATE = '08';     % 
BCIpacket.HDR.MESSAGE = '09';   % 
BCIpacket.HDR.SIGNAL = '10';    % Send EEG signals
BCIpacket.HDR.ROBOT = '11';     % Controls Robot
 end
BCIpacket.Terminator = 'CR/LF';% Terminator for TCP/IP data
BCIpacket.HDRLength = 2;
% TCP/IP parameters
TCPIP.OutputBufferSize = 1024; % in Bytes
TCPIP.InputBufferSize = 1024;  % in Bytes