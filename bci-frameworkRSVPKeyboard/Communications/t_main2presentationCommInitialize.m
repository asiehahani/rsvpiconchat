%% [success,main2presentationCommObject]=t_main2presentationCommInitialize(presentationIP,presentationPort)
%t_main2presentationCommInitialize(presentationIP,presentationPort) is template 
% function to create TCP/IP object for communication from main function to presentation
% side. Main function side is assumed to be the server side. The output and
% input buffer sizes are also set. A simple call back function that
% displays the received information on the screen is used for the tcp/ip object.
% See also receivePresentationPacket. Terminator mode is selected for the
% tcp/ip object, once a line feed (LF) type is encountered the received data is
% used in the call back function.
%
%   The inputs of the function
%      presentationIP - IP address of the computer that runs the
%      presentation. If the presentation runs on the same computer as the main
%      function, leave the input blank, the default value 'localhost' is used to
%      create the tcp/ip object.
%
%      presentationPort - Port number used for the communication between
%      the presentation and the main functions. If no value is mentioned
%      default value is used in the tcp/ip object.
%
%
%   The outputs of the function
%
%      success (0/1) - A flag that shows the success of the process.
%
%      main2presentationCommObject - TCP/IP object created for the
%      communication between main and presentation functions.
%
%      BCIpacketStruct - A structure containing header, header length and terminator
%      information. Having this function as an output variable help passing
%      them to receiveMainPacket function ( a callback funtction that
%      executes from the base workspace.
%
%
%  See also receivePresentationPacket, tcpipParameters.

function [success,main2presentationCommObject,BCIpacketStruct]=t_main2presentationCommInitialize(presentationIP,presentationPort)
global errorFilename,
if(isempty(errorFilename))
    errorFilename = 'main2presentationCommInitializeErrorFile';
end
% if(exist('logError','file')~=2)
%     addpath('..\GeneralFramework');
% end
 

%% Read TCP/IP connection parameters

tcpipParameters;

%% Checking for the input variables

if(nargin==1)
    presentationPort=presentationIP;
    presentationIP='localhost';
elseif(nargin==0)
    presentationIP=input('Please enter the presentation IP address [localhost]:','s');
    if(isempty(presentationIP))
        presentationIP='localhost'; % If no IP number is entered, local host option is assumed for the tcp/ip object
    end
    presentationPort=input('Please enter the main port number [52957]:','s');
    if(isempty(presentationPort))
        presentationPort=52957; % Default port number
    else
        presentationPort=str2int(presentationPort);
    end
end

%% Creating the TCP/IP object

try
    main2presentationCommObject = tcpip(presentationIP,presentationPort,...
        'NetworkRole', 'server',...
        'Terminator',BCIpacketStruct.terminator ,...
        'OutputBufferSize', tcpipBufferSize.output,...
        'InputBufferSize',tcpipBufferSize.input,...
        'BytesAvailableFcn','',...%(main2presentationCommObject,BCIpacketStruct)',...
        'BytesAvailableFcnMode','terminator');
    
    fopen(main2presentationCommObject);
    disp('Presentation TCP/IP server started');
    success=1;
catch ME
    logError(ME);
    success=0;
end