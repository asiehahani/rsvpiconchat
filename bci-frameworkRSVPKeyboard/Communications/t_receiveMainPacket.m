%% t_receiveMainPacket(presentation2mainCommObject,BCIpacketStruct)
%This template functions reads a packet separated by terminator from the buffer.
% This function parses BCI packet header and create packetReceived
% structure.
%
%   The inputs of the function
%      main2presentationCommObject - object obtained for tcp/ip.
%
%      BCIpacketStruct - A structure containing header, header length and terminator
%      information.
%
% See also main2presentationCommInitialize.

function receivedPacket=t_receiveMainPacket(presentation2mainCommObject,BCIpacketStruct)

try
    if(presentation2mainCommObject.BytesAvailable)
        receivedStream =  fscanf(presentation2mainCommObject);
        receivedPacket.header = str2double(receivedStream(1:BCIpacketStruct.HDRLength));
        
        switch(receivedPacket.header)
            case 1
                disp('received Start Packet');
            case 2
                disp('received Pause Packet');
                disp(receivedPacket)
            case 3
                disp('received Stop Packet');
            case 4
                disp('received Decision Packet');
                receivedPacket.data = hex2num(reshape(receivedStream(BCIpacketStruct.HDRLength+1:end-2),length(receivedStream(BCIpacketStruct.HDRLength+1:end-2))/16,16));
                disp(receivedPacket)
            case 5
                disp('received Parameters Packet');
            case 6
                disp('received Target Packet');
            case 7
                disp('received Probabilities Packet');
                labels = receivedStream(strfind(receivedStream,'<')+1 : strfind(receivedStream,'>')-1);
                if(~isempty(labels))
                    labelSeparatorNdx = strfind(labels,',');
                    numberOfLabels = length(strfind(labels,','))+1;
                    receivedPacket.labels = cell(numberOfLabels,1);
                    for ii = 1:numberOfLabels
                        if ii == 1
                            receivedPacket.labels(ii) = {labels(1:labelSeparatorNdx(ii)-1)};
                        elseif ii == numberOfLabels
                            receivedPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:end)};
                        else
                            receivedPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:labelSeparatorNdx(ii)-1)};
                        end
                    end
                else
                    receivedPacket.labels = [];
                end
                dataNdx = strfind(receivedStream,'>')+1;
                receivedPacket.data = hex2num(reshape(receivedStream(dataNdx:end-2),length(receivedStream(dataNdx:end-2))/16,16));
                disp(receivedPacket)
            case 8
                disp('received State Packet');
            case 9
                %disp('received Message Packet');
                receivedPacket.data = receivedStream(BCIpacketStruct.HDRLength+1:end-1);
                %disp(receivedPacket)
            case 10
                disp('received Signals Packet');
            case  11
                disp('received Robot Packet');
        end
        
    else
        receivedPacket=[];
    end
catch ME
    logError(ME);
end