%% tcpipParameters
%Contains TCP/IP communication parameters.
%   This files create following structures
%   TCPIPBufferSize -
%       *TCPIPBufferSize.input - input buffer size in bytes
%       *TCPIPBufferSize.output - output buffer size in bytes
%   BCIpacketStruct with following fields,
%       *BCIpacketStruct.HDR - defines header codes for different packet types
%       used in BCI projects. HDR structure has a specific code for all
%       possible packet type, with convention given below.
%           Header definition for different packet types:
%               START = '01';     % STARTS PRESENTATION
%               PAUSE = '02';     % PAUSES PRESENTATION
%               STOP = '03';      % STOPS PRESENTATION
%               DECISION = '04';  % SENDS DECISION
%               PARAMETERS = '05';% Presentation parameters
%               TARGET = '06';     % Target ID
%               PROBABILITIES = '07'; % Language model probabilites
%               STATE = '08';     % 
%               MESSAGE = '09';   % 
%               SIGNAL = '10';    % Send EEG signals
%               ROBOT = '11';     % Controls Robot
%    *BCIpacketStruct.terminator - this field is appended in order to
%                                   receive callback while data is received.
%    *BCIpacketStruct.HDRLength - specifies number of characters to used in the
%    header.
%%
% TCP/IP parameters
tcpipBufferSize.input = 4096; % in Bytes
tcpipBufferSize.output = 4096;  % in Bytes

% Packet types used 
BCIpacketStruct.HDR.START = '01';     % STARTS PRESENTATION
BCIpacketStruct.HDR.PAUSE = '02';     % PAUSES PRESENTATION
BCIpacketStruct.HDR.STOP = '03';      % STOPS PRESENTATION
BCIpacketStruct.HDR.DECISION = '04';  % SENDS DECISION
BCIpacketStruct.HDR.PARAMETERS = '05';% Presentation parameters
BCIpacketStruct.HDR.TARGET = '06';     % Target ID
BCIpacketStruct.HDR.PROBABILITIES = '07'; % Language model probabilites
BCIpacketStruct.HDR.STATE = '08';     % 
BCIpacketStruct.HDR.MESSAGE = '09';   % 
BCIpacketStruct.HDR.SIGNAL = '10';    % Send EEG signals
BCIpacketStruct.HDR.ROBOT = '11';     % Controls Robot

BCIpacketStruct.terminator = '$';% Terminator for TCP/IP data
BCIpacketStruct.HDRLength = 2;

