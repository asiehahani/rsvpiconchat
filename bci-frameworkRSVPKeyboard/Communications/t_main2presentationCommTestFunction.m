%%Template code to send packet using TCP/IP interface object from mainBCI to
% presentation. Use this script after establishing TCPIP connection between
% server and the client.
% Sends four sample packets - pause(this packet contains header with no
%                             data,
%                             decision(this packet contains header and a vector of doubles as data)
%                             probabilities(this packet contains header, a vector of probabilities in doubles as data and array if cell for labels),
%                             message (string)
% See also t_sendBCIpacket.
clc,
tcpipParameters;

% Sends Pause Packet
packet2send.header =  BCIpacketStruct.HDR.PAUSE;
tic,
[success] = t_sendBCIpacket(main2presentationCommObject,packet2send);
toc,
disp(packet2send)

% Send Decision
packet2send.header =  BCIpacketStruct.HDR.DECISION;
packet2send.data = [1.2323e5 233289e-10];
tic,
[success] = t_sendBCIpacket(main2presentationCommObject,packet2send);
toc,
disp(packet2send)

% Sends Message Packet
msg = 'Testing message sending';
packet2send.header = BCIpacketStruct.HDR.MESSAGE;
packet2send.data = 'Testing message sending';
tic,
[success] = t_sendBCIpacket(main2presentationCommObject,packet2send);
toc,
disp(packet2send)

% Sends Probabilities Packet
load probabilities.mat
packet2send.header = BCIpacketStruct.HDR.PROBABILITIES;
sendProbabilitiesLabels = true;
if(sendProbabilitiesLabels)
    packet2send.data = prob(1:3);
    packet2send.labels = {'icon1','icon2','icon3'};
else
    packet2send.data = prob;
    packet2send.labels = [];
end
tic,
[success] = t_sendBCIpacket(main2presentationCommObject,packet2send);
toc,
disp(packet2send)

clear packet2send,