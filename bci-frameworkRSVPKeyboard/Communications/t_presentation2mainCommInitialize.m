%% [success,presentation2mainCommObject,BCIpacket]=t_presentation2mainCommInitialize(mainIP,mainPort)
%t_presentation2mainCommInitialize(mainIP,mainPort) is template function to
% create TCP/IP object for communication from main function to presentation
% side. Main function side is assumed to be the server side. The output and
% input buffer sizes are also set. A simple call back function that
% displays the received information on the screen is used for the tcp/ip object.
% See also receivePresentationPacket. Terminator mode is selected for the
% tcp/ip object, once a line feed (LF) type is encountered the received data is
% used in the call back function.
%
%   The inputs of the function
%      presentationIP - IP address of the computer that runs the
%      presentation. If the presentation runs on the same computer as the main
%      function, leave the input blank, the default value 'localhost' is used to
%      create the tcp/ip object.
%
%      presentationPort - Port number used for the communication between
%      the presentation and the main functions. If no value is mentioned
%      default value is used in the tcp/ip object.
%
%
%   The outputs of the function
%
%      success (0/1) - A flag that shows the success of the process.
%
%      main2presentationCommObject - TCP/IP object created for the
%      communication between main and presentation functions.
%
%      BCIpacketStruct - A structure containing header, header length and terminator
%      information. Having this function as an output variable help passing
%      them to receiveMainPacket function ( a callback funtction that
%      executes from the base workspace.
%
%
%  See also receiveMainPacket, tcpipParameters.

function [success,presentation2mainCommObject,BCIpacketStruct]=t_presentation2mainCommInitialize(mainIP,mainPort)
global errorFilename,
if(isempty(errorFilename))
    errorFilename = 'main2presentationCommInitializeErrorFile';
end
if(exist('logError','file')~=2)
    addpath('..\GeneralFramework');
    addpath('..\Parameters');
end

%% Read TCP/IP connection parameters
tcpipParameters;
MAX_CONNECTION_TRIAL_COUNT=11;


%% Checking for the input variables

if(nargin==1)
    mainPort=mainIP;
    mainIP='localhost';
elseif(nargin==0)
    mainIP=input('Please enter the main IP address [localhost]:','s');
    if(isempty(mainIP))
        mainIP='localhost';
    end
    mainPort=input('Please enter the main port number [52957]:','s');
    if(isempty(mainPort))
        mainPort=52957;
    else
        mainPort=str2int(mainPort);
    end
end

%% Creating the tcp/ip object
try
    presentation2mainCommObject = tcpip(mainIP,mainPort,...
        'NetworkRole', 'client',...
        'Terminator',BCIpacketStruct.terminator ,...
        'OutputBufferSize', tcpipBufferSize.output,...
        'InputBufferSize',tcpipBufferSize.input,...
        'BytesAvailableFcn','x=3;',...%t_receiveMainPacket(presentation2mainCommObject,BCIpacketStruct)',...
        'BytesAvailableFcnMode','terminator');
    
catch ME
    
    success=0;
    rethrow(ME);
end
%% Trying to connect to the server side.

success=0;
connectionTryCount=0;
while(~success)
    try
        fopen(presentation2mainCommObject);
        disp('Connection between main and presentation is established.');
        success=1;
    catch ME
        if(strcmp(ME.identifier,'instrument:fopen:opfailed'))
            connectionTryCount=connectionTryCount+1;
            if(connectionTryCount<MAX_CONNECTION_TRIAL_COUNT)
                disp(['Server is unreachable. Reconnecting (' num2str(connectionTryCount) ')...']);
            else
                disp('Server is unreachable.');
                tryAgain=input('Continue trying? ([y]/n):','s');
                if(tryAgain=='n')
                    disp('Connection aborted by user.')
                    rethrow(ME);
                else
                    connectionTryCount=0;
                end
            end
        end
    end
    
end