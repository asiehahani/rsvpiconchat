%% t_receivePresentaionPacket(main2presentationCommObject,BCIpacketStruct)
%This template functions reads a packet separated by terminator from the buffer.
% This function parses BCI packet header and create packetReceived
% structure.
%
%   The inputs of the function
%      main2presentationCommObject - object obtained for tcp/ip.
%
%      BCIpacketStruct - A structure containing header, header length and terminator
%      information.
%
% See also main2presentationCommInitialize.

%function t_receivePresentationPacket(main2presentationCommObject,BCIpacketStruct)
global main2presentationCommObject

try
    if(main2presentationCommObject.BytesAvailable)
        recievedStream =  fscanf(main2presentationCommObject);
        recievedPacket.header = str2double(recievedStream(1:BCIpacketStruct.HDRLength));
        
        switch(recievedPacket.header)
            case 1
                disp('Recieved Start Packet');
            case 2
                disp('Recieved Pause Packet');
                disp(recievedPacket)
            case 3
                disp('Recieved Stop Packet');
                continueMainLoop=false;
                
                success=quitDAQ(amplifierStruct);
                %sessionControlStruct.externalStopFlag=true;
%                 amplifierStruct=rmfield(amplifierStruct,'ai');
            case 4
                disp('Recieved Decision Packet');
                recievedPacket.data = hex2num(reshape(recievedStream(BCIpacketStruct.HDRLength+1:end-2),length(recievedStream(BCIpacketStruct.HDRLength+1:end-2))/16,16));
                disp(recievedPacket)
            case 5
                disp('Recieved Parameters Packet');
            case 6
                disp('Recieved Target Packet');
            case 7
                disp('Recieved Probabilities Packet');
                disp('Recieved Probabilities Packet');
                labels = recievedStream(strfind(recievedStream,'<')+1 : strfind(recievedStream,'>')-1);
                if(~isempty(labels))
                    labelSeparatorNdx = strfind(labels,',');
                    numberOfLabels = length(strfind(labels,','))+1;
                    recievedPacket.labels = cell(numberOfLabels,1);
                    for ii = 1:numberOfLabels
                        if ii == 1
                            recievedPacket.labels(ii) = {labels(1:labelSeparatorNdx(ii)-1)};
                        elseif ii == numberOfLabels
                            recievedPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:end)};
                        else
                            recievedPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:labelSeparatorNdx(ii)-1)};
                        end
                    end
                else
                    recievedPacket.labels = [];
                end
                dataNdx = strfind(recievedStream,'>')+1;
                recievedPacket.data = hex2num(reshape(recievedStream(dataNdx:end-2),length(recievedStream(dataNdx:end-2))/16,16));
                disp(recievedPacket)
            case 8
                disp('Recieved State Packet');
            case 9
                disp('Recieved Message Packet');
                recievedPacket.data = recievedStream(BCIpacketStruct.HDRLength+1:end-2);
                disp(recievedPacket)
            case 10
                disp('Recieved Signals Packet');
            case  11
                disp('Recieved Robot Packet');
        end
    end
catch ME
    logError(ME);
end