% Sample Probabilities read function
% This function look for labels and probaility values from receieved
% stream.
% InputStream format : HeaderCharacters <> probalitiesInHex
% HeaderCharacters ->  Contains number of charters specified by HDRLength 
% iconlabels -> each labels is comma separated and enclosed in <>

function [success] = readProbabilities(recievedStream,dataNdx)

labels = recievedStream(strfind(recievedStream,'<')+1 : strfind(recievedStream,'>')-1);
if(~isempty(labels))
    labelSeparatorNdx = strfind(labels,',');
    numberOfLabels = length(strfind(labels,','))+1;
    thisPacket.labels = cell(numberOfLabels,1);
    for ii = 1:numberOfLabels
        if ii == 1
            thisPacket.labels(ii) = {labels(1:labelSeparatorNdx(ii)-1)};
        elseif ii == numberOfLabels
            thisPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:end)};
        else
            thisPacket.labels(ii) = {labels(labelSeparatorNdx(ii-1)+1:labelSeparatorNdx(ii)-1)};
        end
    end
else
    thisPacket.labels = [];
end
dataNdx = strfind(recievedStream,'>')+1;
thisPacket.data = reshape(recievedStream(dataNdx:end),length(recievedStream(dataNdx:end))/16,16);
thisPacket.data = hex2num(reshape(recievedStream(dataNdx:end),length(recievedStream(dataNdx:end))/16,16));
disp(thisPacket);

success = true;
