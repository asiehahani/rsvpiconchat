%% [success]=startAmps(amplifierStruct,dataFilename,logToDiskMode)
%Sets amplifier(s) operating mode and starts data acquisition.
%   startAmps, sets the amplifier mode to "Normal" and sets the filename
%   for storing the data on the disk.
%
%   Note: When more than one amplifier is connected the Master amplifier 
%         should be started last.
%
%   [success]=startAmps(amplifierStruct,dataFilename,mode)
%   inputs:
%       amplifierStruct - a structure which contains the amplifier(s)
%                       objects
%       
%       dataFilename - a filename which is used to record data to Disk
%
%       logToDiskMode - selects to overwrite the files with the same name
%                       or to create a new one and index them (Index/Overwrite)
%
%   returns:
%       success (0/1) - a flag to show the success of the operation
%   
%%

function [success]=startAmps(amplifierStruct,dataFilename,logToDiskMode)

try
%% Test if amplifier(s) are running already    
    if strcmpi(amplifierStruct.ai(1).Running,'on')
        [success]=stopAmps(amplifierStruct);
        ME = (['Amplifier(s) were still running.',...
            ' The recording stopped and will start with the new filename(s).']);        
        warning(ME);
        logError(ME,0);
    end
%% Set mode and log filename    
    for ampIndex = amplifierStruct.numberOfAmplifiers:-1:1
        set(amplifierStruct.ai(ampIndex),'Mode','Normal',...
            'LogFileName',[dataFilename,'_Amp',num2str(ampIndex) '_Run'],...
            'LoggingMode','Disk&Memory',...
            'LogToDiskMode',logToDiskMode);
    end
%% Start amplifier(s)
    % Master amplifier should start at last.
    for ampIndex = amplifierStruct.numberOfAmplifiers:-1:1           
        start(amplifierStruct.ai(ampIndex));
    end
    disp('Recording started')
    success = 1;
catch ME
    logError(ME);
    success=0;
end