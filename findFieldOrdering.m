clear all
clc
global BCIframeworkDir

BCIframeworkDir='.';
addpath(genpath('.'))

imageStructs = xls2Structs('iconImageList.xls');
languageModelClient=iconlm(imageStructs,'\@iconlm\crowd.stats');

trialStructIndices=([imageStructs.IsTrial]==1);   
trialIDs=[imageStructs(trialStructIndices).ID];
trialNames={imageStructs(trialStructIndices).Name};


allField={imageStructs(trialStructIndices).Field};

verbID=find(~cellfun('isempty',strfind(allField,'verb')));
verbID=unique(verbID);


subjID=find(~cellfun('isempty',strfind(allField,'subject')));
subjID=unique(subjID);

objID=find(~cellfun('isempty',strfind(allField,'object')));
objID=unique(objID);

subjmodID=find(~cellfun('isempty',strfind(allField,'subjmod')));
subjmodID=unique(subjmodID);

objmodID=find(~cellfun('isempty',strfind(allField,'objmod')));
objmodID=unique(objmodID);

total=0;
matlabpool(4)
parfor i1=1:length(verbID)
for i2=1:length(subjID)
for i3=1:length(objID)
for i4=1:length(subjmodID)
for i5=1:length(objmodID)
combo = languageModelClient.getGram([trialNames(verbID(i1)) trialNames(subjID(i2)) trialNames(objID(i3)) trialNames(subjmodID(i4)) trialNames(objmodID(i5))]);
% combo = languageModelClient.getGram([trialNames(subjmodID(i4)) trialNames(objmodID(i5))]);
if isKey(languageModelClient.gramCounts, combo)
total=total+languageModelClient.gramCounts(combo);
end
end
end
end
end
end
matlabpool close
