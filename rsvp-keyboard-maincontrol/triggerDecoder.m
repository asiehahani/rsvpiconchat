%% function [firstUnprocessedTimeIndex,completedSequenceCount,trialSampleTimeIndices,trialTargetness,trialLabels]=triggerDecoder(triggerBufferObject,triggerPartitionerStruct)
% Trigger Control and Partitioning
%
%   Inputs : triggerBufferObject      - a dataBuffer class object containing the
%                                       buffer corresponding to trigger signal.
%
%           triggerPartitionerStruct  - a struct for tirgger partitioner
%
%   Outputs : firstUnprocessedTimeIndex  - the time index of first
%                                          unprocessed sequence
%             completedSequenceCount     - number of completed sequence
%             trialSampleTimeIndices     - time index for specific trial
%                                          sample
%             trialTargetness
%             trialLabels                - labels of trials
%%
function [firstUnprocessedTimeIndex,completedSequenceCount,trialSampleTimeIndices,trialTargetness,trialLabels]=triggerDecoder(triggerBufferObject,triggerPartitionerStruct)


try
    trialSampleTimeIndices=[];
    trialTargetness=[];
    trialLabels=[];
    completedSequenceCount=0;
    
    %Initialize first unprocessed time index
    firstUnprocessedTimeIndex=triggerPartitionerStruct.firstUnprocessedTimeIndex;
    if(isa(triggerBufferObject,'dataBuffer'))
        %check whether triggerBufferObject is of type dataBuffer
        %if so,set last sample time index from triggerBufferObject
        lastSampleTimeIndex=triggerBufferObject.lastSampleTimeIndex;
    else
        %else set last sample time index to the difference between the
        %length of triggerOfBufferObject and windowLengthinSample
        lastSampleTimeIndex=length(triggerBufferObject)-triggerPartitionerStruct.windowLengthinSamples;
    end
    if(lastSampleTimeIndex>firstUnprocessedTimeIndex)%whether the last sample index greater than first unprocessed time index
        if(isa(triggerBufferObject,'dataBuffer'))
            %check whether triggerBufferObject is of type dataBuffer
            %if so,get data from triggerBufferObject.getOrderedData
            %ranging from firstUnprocessedTimeIndex to lastSampleTimeIndex
            t=triggerBufferObject.getOrderedData(firstUnprocessedTimeIndex,lastSampleTimeIndex);
        else
            %else,get data from triggerBufferObject(1 to lastSampleTimeIndex)
            t=triggerBufferObject(1:lastSampleTimeIndex);
        end
        %index of element not equal to previous element
        tChangeLocAllRaw=find(diff(t)~=0)+1;
        %values of element not equal to previous element
        tChangeValues=t(tChangeLocAllRaw);
        %find the last zero that not equal to previous element
        lastChangetoZero=find(tChangeValues==0,1,'last');
        %update index of tChangeValues,select elements previous to lastChangetoZero
        tChangeLocAll=tChangeLocAllRaw(1:lastChangetoZero);
        %update values of tChangeValues
        tChangeValues=tChangeValues(1:lastChangetoZero);
        %find tChangeValues greater than zero
        tChangeValuesispositive=(tChangeValues>0);
        %index of non-zero tChangeValues
        tChangeValuesispositiveIndices=find(tChangeValuesispositive);
        
        tChangeLoc=tChangeLocAll(tChangeValuesispositive);
        %update tChangeValues
        tChangeValues=round(tChangeValues(tChangeValuesispositive));
        
        %index of sequence end flag
        sequenceEndIndices=find(tChangeValues==triggerPartitionerStruct.sequenceEndID);
        
        if(isempty(sequenceEndIndices))
            %check if sequenceEndIndices is empty
            %if so,set complete sequence count to zero
            completedSequenceCount=0;
            
            
        else
            
            if(tChangeLoc(sequenceEndIndices(end)-1)+triggerPartitionerStruct.windowLengthinSamples>length(t))
                %compare the length of sequence end indices (end -1) plus
                %length of sample with length of triggerData
                %update sequence end index from (1 to end-1)
                sequenceEndIndices=sequenceEndIndices(1:end-1);
            end
            if(isempty(sequenceEndIndices))
                %check if sequenceEndIndices is empty
                %if so,set complete sequence count to zero
                completedSequenceCount=0;
                
            else
                %else,set the completed sequence count to the length of
                %sequence end sequences
                completedSequenceCount=length(sequenceEndIndices);
                %update index of tChangeLoc(1 to sequenceEndIndices(end))
                tChangeLoc=tChangeLoc(1:sequenceEndIndices(end));
                %update values of tChangeValues(1 to sequenceEndIndices(end))
                tChangeValues=tChangeValues(1:sequenceEndIndices(end));
                %index of fixation flag
                fixationIndices=find(tChangeValues==triggerPartitionerStruct.fixationID);
                %number of total trial times
                trialCount=sum(sequenceEndIndices-fixationIndices-1);
                %initialize trial labels,trialSampleTimeIndices,trialTargetness,trial index
                trialLabels=zeros(1,trialCount);
                trialSampleTimeIndices=zeros(1,trialCount);
                trialTargetness=repmat(-1,1,trialCount);
                trialIndex=1;
                
                for(ii=1:completedSequenceCount)
                    sequenceEndIndex=sequenceEndIndices(ii);
                    % find the begin index of sequence
                    if(ii>1)
                        %update sequence begin index from
                        sequenceBeginIndex=sequenceEndIndices(ii-1)+1;
                    else
                        sequenceBeginIndex=1;
                    end
                    
                    
                    if(sequenceBeginIndex==fixationIndices(ii))
                        
                        
                        sequenceTrialCount=sequenceEndIndex-1-sequenceBeginIndex;
                        %check whether sequence begin index equal to
                        %fixation index,if so
                        %update trialLabels from tChaneValues(sequenceBeginIndex+1 to SequenceEndIndex-1)
                        trialLabels(trialIndex:trialIndex+sequenceTrialCount-1)=tChangeValues((sequenceBeginIndex+1):sequenceEndIndex-1);
                        %update trialSampleIndices to the sum of
                        %firstUnprocessedTimeIndex plus tChangeLoc(sequenceBeginIndex+1 to sequenceEndIndex-1)-1
                        trialSampleTimeIndices(trialIndex:trialIndex+sequenceTrialCount-1)=firstUnprocessedTimeIndex+tChangeLoc((sequenceBeginIndex+1):sequenceEndIndex-1)-1;
                        
                        %update trialIndex equal to the old value of trialIndex plus sequenceTrialCount
                        trialIndex=trialIndex+sequenceTrialCount;
                    else
                        %update
                        targetLabel=tChangeValues(sequenceBeginIndex)-triggerPartitionerStruct.TARGET_TRIGGER_OFFSET;
                        sequenceTrialCount=sequenceEndIndex-2-sequenceBeginIndex;
                        tempTrialLabels=tChangeValues((sequenceBeginIndex+2):sequenceEndIndex-1);
                        %update trialLabels from tChaneValues(sequenceBeginIndex+2 to SequenceEndIndex-1)
                        trialLabels(trialIndex:trialIndex+sequenceTrialCount-1)=tempTrialLabels;
                        %update trialTargetness
                        trialTargetness(trialIndex:trialIndex+sequenceTrialCount-1)=(tempTrialLabels==targetLabel);
                        %update trialSampleIndices to the sum of
                        %firstUnprocessedTimeIndex plus tChangeLoc(sequenceBeginIndex+2 to sequenceEndIndex-1)-1
                        trialSampleTimeIndices(trialIndex:trialIndex+sequenceTrialCount-1)=firstUnprocessedTimeIndex+tChangeLoc((sequenceBeginIndex+2):sequenceEndIndex-1)-1;
                        %update trialIndex equal to the old value of trialIndex plus sequenceTrialCount
                        trialIndex=trialIndex+sequenceTrialCount;
                        
                    end
                    
                    
                    
                end
                %update firstUnprocessedTimeIndex
                firstUnprocessedTimeIndex=firstUnprocessedTimeIndex+tChangeLocAll(tChangeValuesispositiveIndices(sequenceEndIndex)+1)-1;
                
                
                
            end
        end
        %check if complete sequence count equal to zero
        if(completedSequenceCount==0)
            if(~isempty(tChangeLocAllRaw))
                %if tChangeLocAllRaw is empty,update firstUnprocessedTimeIndex
                %to firstUnprocessedTimeIndex plus tChangeLocALLRaw(1) -2
                firstUnprocessedTimeIndex=firstUnprocessedTimeIndex+tChangeLocAllRaw(1)-2;
            else
                %otherwise,update firstUnprocessedTimeIndex to
                %lastSampleTimeIndex
                firstUnprocessedTimeIndex=lastSampleTimeIndex;
            end
        end
        
    end
    
catch exception
    keyboard;
    
end