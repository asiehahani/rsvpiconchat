%% initializeMachineLearning
% initializeMachineLearning is a script that does the initialization of the
% machine learning steps. It initializes triggerPartitioner,
% featureExtraction and decisionMaker. For decisionMaker, an
% RSVPKeyboardTask object is created.
%
% triggerPartitioner partitions the trigger signal into sequences and trials.
%
% featureEtraction applies operations on the EEG trials to classify each
% trial, it creates features to be used in decision making.
% 
% decisionMaker uses the features extracted from featureExtraction and
% utilizes the language model to make a symbol decision. It initializes the
% RSVPKeyboardTask object which initializes the language model and
% a decisionClass object which makes the decisions after the fusion.
% RSVPKeyboardTask object also keeps the progress of the session.
%
%%

%% Loads trial symbol list.
imageStructs=xls2Structs('iconImageList.xls');

%% Initializes the triggerPartitioner
processingStruct.triggerPartitioner.firstUnprocessedTimeIndex=1;
processingStruct.triggerPartitioner.TARGET_TRIGGER_OFFSET= RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;
processingStruct.triggerPartitioner.sequenceEndID=imageStructs(strcmpi({imageStructs.Name},'SequenceEnd')).ID;
processingStruct.triggerPartitioner.fixationID=imageStructs(strcmpi({imageStructs.Name},'Fixation')).ID;
processingStruct.triggerPartitioner.windowLengthinSamples=round(RSVPKeyboardParams.windowDuration*fs);
if(strcmpi(amplifierStruct.DAQType,'noAmp'))
    amplifierStruct.triggerPartitionerStruct=processingStruct.triggerPartitioner;
end


%% Initializes the featureExtraction and loads the calibration file
% If the session type is not calibration, a calibration file containing the
% trained classifier and kernel density estimators. 
processingStruct.featureExtraction=[];
processingStruct.featureExtraction.fs=fs;
processingStruct.featureExtraction.rejectSequenceFlag=0;
processingStruct.featureExtraction.rejectSequenceInfo=[];


if(exist('sessionID','var'))
    processingStruct.featureExtraction.sessionID=sessionID;
end

if(sessionID~=1) %If session is not calibration
    [machineCalibrationFile,sessionFolder]=uigetfile('*.mat','Please Select the Calibration File','MultiSelect', 'off','Data\');
    load([sessionFolder '\' machineCalibrationFile]);
    processingStruct.featureExtraction.Flow=featureExtractionProcessFlow;
end


%% Initializes the RSVPKeyboardTask object to be used in decisionMaker.
switch sessionID
    case 1    % Calibration
        RSVPKeyboardTaskObject=CalibrationTask(imageStructs,RSVPKeyboardParams);
    case 2    % Spell
        RSVPKeyboardTaskObject=SpellingTask(imageStructs,RSVPKeyboardParams,scoreStruct);
    case 3    % CopyPhrase
        RSVPKeyboardTaskObject=CopyPhraseTask(imageStructs,RSVPKeyboardParams,scoreStruct);
        RSVPKeyboardTaskObject.currentInfo.showTarget=1;
    case 4    % CopyPhrase
        RSVPKeyboardTaskObject=CopySentenceTask(imageStructs,RSVPKeyboardParams,scoreStruct);
        RSVPKeyboardTaskObject.currentInfo.showTarget=1;
    case 5    % MasteryTask
        RSVPKeyboardTaskObject=MasteryTask(imageStructs,RSVPKeyboardParams,scoreStruct);
        RSVPKeyboardTaskObject.currentInfo.showTarget=1;
end
