function [completedFlag,correctSection,incorrectSection]=checkSentenceTypingCorrectness(targetPhrase,typedPhrase)
%% Determining correctSection and incorrectSection
typedIntervals1=[0 find(typedPhrase=='_')];
typedIntervals2=[0 find(targetPhrase=='_')];

correctSection=[];
for count=1:min(length(typedIntervals1),length(typedIntervals2))-1
if strcmpi(targetPhrase(typedIntervals2(count)+1:typedIntervals2(count+1)-1),typedPhrase(typedIntervals1(count)+1:typedIntervals1(count+1)-1)) 
correctSection=[correctSection typedPhrase(typedIntervals1(count)+1:typedIntervals1(count+1)-1) '_'];
end
end
incorrectSection=typedPhrase(length(correctSection)+1:end);


%% Determining the completness of the phrase.
completedFlag=strcmpi(correctSection,targetPhrase);

 