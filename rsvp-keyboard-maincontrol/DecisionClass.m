%% DecisionClass
% DecisionClas is the class that handles decision making. It uses the
% features extracted from featureExtraction (results structure) and
% language model (via languageModelWrapper class), and makes decisions
% after applying the corresponding fusion. It also keeps track of
% decisions.
%%

classdef DecisionClass < handle
    %% Properties of the DecisionClass
    properties
        % decisionHistory keeps a history of the decisions. It is a
        % linkedList of currentInfo structure.
        decisionHistory
        
        % epochList is a linkedList of decisions. It has the same structure
        % as decisionHistory, however it might be resetted in CopyTask if
        % the phrase changes.
        epochList
        
        % decisionStoppingCriteria is a struct of criteria indicating when a
        % decision should be made. It contains the fields of Typing from
        % RSVPKeyboard parameters, e.g confidenceThreshold,
        % confidenceFunction, maximumNumberofSequences.
        decisionStoppingCriteria
        
        % languageModelWrapperObject is a languageModelWrapper object handles
        % access to the language model.
        languageModelWrapperObject
        
        % trialIDs is vector of symbol IDs which are allowed to be
        % stimulus.
        trialIDs
        
        % scoreStruct - A calibration file containing the kernel density
        % estimators. 
        scoreStruct
        
        % currentInfo is a structure that keeps the current status. Examples
        % of its fields are decided, LMprobs, posteriorProbs,
        % localSequenceCounter.
        currentInfo
        
        decList
        dec
        i1
        i2
    end
    
    %% Methods of DecisionClass
    methods
        %% self = DecisionClass(languageModelWrapperObject,scoreStruct,decisionStoppingParams)
        % Constructor for the DecisionClass. This constructor is called
        % from RSVPKeyboardTask class to control the decision making.
        % The inputs of the constructor
        %   languageModelWrapperObject - languageModelWrapper object handles
        %   access to the language model.
        %
        %   scoreStruct - A calibration file containing the kernel density
        %   estimators. 
        %
        %   decisionStoppingCriteria - struct of criteria of when a decision 
        %   should be made. It contains the fields of Typing from
        %   RSVPKeyboard parameters, e.g confidenceThreshold,
        %   confidenceFunction, maximumNumberofSequences.
        function self = DecisionClass(languageModelWrapperObject,scoreStruct,decisionStoppingParams)
            self.languageModelWrapperObject=languageModelWrapperObject;
            if(exist('scoreStruct','var'))
                self.scoreStruct=scoreStruct;
            else
                self.scoreStruct=[];
            end
            self.decList=[45 52 94 45 2];
            self.dec=45;
            self.i1=1;
            self.i2=1;
            self.trialIDs = self.languageModelWrapperObject.trialIDs;
            self.pInitializeCurrentInfo(); 
            self.pSetupDecisionStoppingCriteria(decisionStoppingParams);
            self.decisionHistory=linkedList;
            self.epochList=linkedList;
        end
        
        %% [tempDecision,epochEndedFlag]=makeDecision(self,results)
        % makeDecision updates the current status using the results structure,
        % checks the decision making criteria, if the epoch ended it 
        % makes a decision.
        %
        % Input:
        %       results - a structure containing one dimensional features
        %       corresponding to each trial
        %
        %       results.trialLabels - the labels of the trials (vector of
        %       indices)
        %
        %       results.decideNextFlag - a boolean indicator indicates if a
        %       new sequence is expected to be shown.
        %
        %       results.completedSequenceCount - the number of sequences
        %       completed and contained in the results structure
        %
        %       results.duration - the duration of the sequence(s)
        %
        % Outputs:
        %       tempDecision - a structure containing the decision
        %       information.
        %       
        %       tempDecision.decided - index of the decided symbol or empty
        %       if epoch didn't end.
        %
        %       epochEndedFlag - boolean indicating if the epoch finished, i.e
        %       a decision is made, or not.
        %
        function [tempDecision,epochEndedFlag]=makeDecision(self,results)
            self.updateCurrentInfo(results);
            epochEndedFlag=self.checkDecisionStoppingCriteria();
            if(epochEndedFlag)
                tempDecision.posteriorProbs=self.currentInfo.posteriorProbs;
                [~, tempDecision.decided]=max(tempDecision.posteriorProbs);
                ind=find(self.decList==self.dec);
                if self.dec==45
                    tempDecision.decided=45;
                    self.dec=self.decList(ind(self.i1)+1);
                    self.i1=self.i1+1;
                elseif self.dec==52
                    tempDecision.decided=52;
                    self.dec=self.decList(ind(self.i2)+1);
                    self.i2=self.i2+1;
                else
                    tempDecision.decided=self.dec;
                    self.dec=self.decList(ind+1);
                end

                self.currentInfo.decided=tempDecision.decided;
                
                self.pSaveCurrentInfo();
                
                self.pFinalizeEpoch(tempDecision);
                
                self.pInitializeCurrentInfo();
            else
                tempDecision.posteriorProbs=self.currentInfo.posteriorProbs;
                tempDecision.decided=[];
            end
        end
        
        %% getLanguageModelProbabilities(self)
        % getLanguageModelProbabilities queries the language model
        % probabilities via languageModelWrapper object and updates
        % currentInfo property.
        function getLanguageModelProbabilities(self)
            self.currentInfo.LMprobs = self.languageModelWrapperObject.getProbs();
            self.currentInfo.posteriorProbs=self.currentInfo.LMprobs;
        end
        
        %% resetEpochList(self)
        % Resets the epochList property
        function resetEpochList(self)
            self.epochList=linkedList;
            
        end
    end
    
    methods (Access = private)
        %% pInitializeCurrentInfo(self)
        % Initializes the epoch's information container, currentInfo
        % property, at the beginning of an epoch.
        function pInitializeCurrentInfo(self)
            self.currentInfo.localSequenceCounter = 0;
            self.getLanguageModelProbabilities();
            self.currentInfo.decided=[];
            self.currentInfo.trialLogCondpdf4Target(length(self.trialIDs))=linkedList;
            self.currentInfo.trialLogCondpdf4Nontarget(length(self.trialIDs))=linkedList;
            self.currentInfo.repetitionCounts=zeros(length(self.trialIDs),1);
        end
        
        %% pFinalizeEpoch(self,decision)
        % After an epoch ends, it updates the language model and clears the
        % linkedList objects.
        function pFinalizeEpoch(self,decision)
            for(symbolIndex=1:length(self.trialIDs))
                self.currentInfo.trialLogCondpdf4Target(symbolIndex).empty();
                self.currentInfo.trialLogCondpdf4Nontarget(symbolIndex).empty();
            end
            self.languageModelWrapperObject.update(decision);
        end
        
        %% pSetupDecisionStoppingCriteria(self,decisionStoppingParams)
        % Initializes decision stopping criteria according to the input
        % structure which should be passed from RSVPKeyboardParams.Typing
        function pSetupDecisionStoppingCriteria(self,decisionStoppingParams)
            self.decisionStoppingCriteria=decisionStoppingParams;
        end
        
        %% pSaveCurrentInfo(self)
        % Reformats currentInfo and updates corresponding histories.
        % (decisionHistory an epocList)
        function pSaveCurrentInfo(self)
            output=self.pReformatCurrentInfo2Save();
            self.decisionHistory.insertEnd(output);
            self.epochList.insertEnd(output);
        end
        
        %% output=pReformatCurrentInfo2Save(self)
        % Reformats currentInfo by converting linkedList objects to cell
        % vectors.
        %
        % output - structure containing the same information as
        % currentInfo, with linkedLists replaced by cell vectors.
        function output=pReformatCurrentInfo2Save(self)
            output = self.currentInfo;
            output.trialLogCondpdf4Target=cell(length(self.trialIDs),1);
            output.trialLogCondpdf4Nontarget=cell(length(self.trialIDs),1);
            for(symbolIndex=1:length(self.trialIDs))
                output.trialLogCondpdf4Target{symbolIndex} = cell2mat(self.currentInfo.trialLogCondpdf4Target(symbolIndex).toCell);
                output.trialLogCondpdf4Nontarget{symbolIndex} = cell2mat(self.currentInfo.trialLogCondpdf4Nontarget(symbolIndex).toCell);
            end
        end
        
        %% updateCurrentInfo(self,results)
        % Using the scores obtained from ERP classification, the
        % probabilistic information is updated.
        %
        % Input:
        %       results - a structure containing one dimensional features
        %       corresponding to each trial
        %
        %       results.trialLabels - the labels of the trials (vector of
        %       indices)
        %
        %       results.decideNextFlag - a boolean indicator indicates if a
        %       new sequence is expected to be shown.
        %
        %       results.completedSequenceCount - the number of sequences
        %       completed and contained in the results structure
        %
        %       results.duration - the duration of the sequence(s)
        function updateCurrentInfo(self,results)
            if(~isempty(self.scoreStruct))
                self.addTrials(results);
            end
            self.computePosteriorProbs();
        end
        
        %% addTrials(self,results)
        % Using the scores in results structure, conditional pdf estimate
        % history is updated.
        function addTrials(self,results)
            if(results.completedSequenceCount>0)
                for(trialIndex=1:length(results.trialLabels))
                    currentTrialID=results.trialLabels(trialIndex);
                    
                    pT=self.scoreStruct.conditionalpdf4targetKDE.probs(results.scores(trialIndex));
                    pN=self.scoreStruct.conditionalpdf4nontargetKDE.probs(results.scores(trialIndex));
                    if(results.trialLabels(trialIndex)<=length(self.trialIDs))
                        self.currentInfo.repetitionCounts(currentTrialID)=self.currentInfo.repetitionCounts(currentTrialID)+1;
                        if(self.scoreStruct.probThresholdTarget < pT || self.scoreStruct.probThresholdNontarget < pN)
                            
                            self.currentInfo.trialLogCondpdf4Target(currentTrialID).insertEnd(log(pT));
                            self.currentInfo.trialLogCondpdf4Nontarget(currentTrialID).insertEnd(log(pN));
                        end
                    end
                end
                self.currentInfo.localSequenceCounter=self.currentInfo.localSequenceCounter+results.completedSequenceCount;
            end
        end
        
        %% computePosteriorProbs(self)
        % Using the current conditional pdf estimate history and the
        % language model probabilities, the fusion is made and posterior
        % probabilities are calculated. The result is stored in currentInfo
        % property.
        function computePosteriorProbs(self)
            logP=zeros(length(self.trialIDs),1);
            for(symbolIndex=1:length(self.trialIDs))
                logP(symbolIndex)=sum(cell2mat(self.currentInfo.trialLogCondpdf4Target(symbolIndex).toCell)-cell2mat(self.currentInfo.trialLogCondpdf4Nontarget(symbolIndex).toCell))+...
                    log(self.currentInfo.LMprobs(symbolIndex))-log(1-self.currentInfo.LMprobs(symbolIndex));
            end
            logP(logP==Inf)=log(realmax('double'));
            
            posteriorProbs=exp(logP-max(logP));
            self.currentInfo.posteriorProbs=posteriorProbs/sum(posteriorProbs);
        end
        
        %% epochEndedFlag=checkDecisionStoppingCriteria(self)
        % Check if the epoch ended, by checking decision stoppping criteria
        % with the currentInfo.
        % 
        % Output:
        %       epochEndedFlag - boolean indicating if the epoch finished, i.e
        %       a decision is made, or not.
        function epochEndedFlag=checkDecisionStoppingCriteria(self)
            epochEndedFlag=false;
            
            epochEndedFlag = epochEndedFlag | (self.currentInfo.localSequenceCounter >= self.decisionStoppingCriteria.MaximumNumberofSequences);
            
            epochEndedFlag = epochEndedFlag | (feval(self.decisionStoppingCriteria.ConfidenceFunction,self.currentInfo.posteriorProbs)>self.decisionStoppingCriteria.ConfidenceThreshold);
            
            epochEndedFlag = epochEndedFlag & (self.currentInfo.localSequenceCounter >= self.decisionStoppingCriteria.MinimumNumberofSequences);
        end
    end   
end