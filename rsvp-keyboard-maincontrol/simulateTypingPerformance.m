%% simulationResults=simulateTypingPerformance(scoreStruct,imageStructs,sessionType,RSVPKeyboardParams)
% Applies simulation of the copy task to estimate the typing performance. It uses the scores
% obtained after cross validation and their KDEs to sample new score samples for simulation. In
% principle a copy phrase task is performed using the random sampling of scores replacing the EEG
% trials.
%
%   Inputs:
%       scoreStruct - structure containing the KDE objects to be used in the estimation of conditional
%       probability densities and probability thresholds for the scores.
%
%           scoreStruct.conditionalpdf4targetKDE - kde1d object constructed using the scores
%           corresponding to the target symbols
%
%           scoreStruct.conditionalpdf4nontargetKDE - kde1d object constructed using the scores
%           corresponding to the target symbols
%
%           scoreStruct.probThresholdTarget - probability threshold on target KDE to reject a trial as an outlier
%
%           scoreStruct.probThresholdNontarget - probability threshold on nontarget KDE to reject a trial as an outlier
%
%       symbolStructArray - a struct vector containing list of symbols or images used in
%       presentations. Loaded using xls2Structs function called on imageList.xls.
%
%       RSVPKeyboardParams - RSVPKeyboard parameters from the parameter file RSVPKeyboardParameters.m.
%
%   Outputs:
%
%       simulationResults - structure containing the results of the simulation
%
%             simulationResults.successfullyCompletedFlag - h+2 dimensional tensor containing
%             booleans indicating the successful completion (h is the number of hyperparameters to
%             search over). First dimension is for different Monte Carlo simulations, second
%             dimension is for different phrases, the rest of the dimensions are for the
%             hyperparameters being searched over.
%
%             simulationResults.sequenceCounter - the number of sequences that was shown for each
%             phrase (same dimensionality as simulationResults.successfullyCompletedFlag)
%
%             simulationResults.typingDuration - the estimated duration for each phrase (same dimensionality as simulationResults.successfullyCompletedFlag)
%
%             simulationResults.targetPhraseLength - one dimensional vector containing the length of
%             phrases to be used in simulation.
%
%%

function simulationResults=simulateTypingPerformance(scoreStruct,imageStructs,sessionType,RSVPKeyboardParams)
presentationParameters
if(~exist('RSVPKeyboardParams','var') || isempty(RSVPKeyboardParams))
    RSVPKeyboardParameters
end

[simulationGridSearchParameters{1:length(RSVPKeyboardParams.Simulation.HyperparameterValues)}]=ndgrid(RSVPKeyboardParams.Simulation.HyperparameterValues{1:length(RSVPKeyboardParams.Simulation.HyperparameterValues)});

simulationGridSearchParameters = reshape(cat(length(simulationGridSearchParameters)+1,simulationGridSearchParameters{:}),numel(simulationGridSearchParameters{1}),length(RSVPKeyboardParams.Simulation.HyperparameterValues));

simulationGridSearchParameterIndices=zeros(size(simulationGridSearchParameters));
for(hyperparameterIndex=1:size(simulationGridSearchParameters,2))
    [~,simulationGridSearchParameterIndices(:,hyperparameterIndex)]=ismember(simulationGridSearchParameters(:,hyperparameterIndex),RSVPKeyboardParams.Simulation.HyperparameterValues{hyperparameterIndex});
end

firstIterationBool=true;
for(repetition=1:RSVPKeyboardParams.Simulation.MonteCarloRepetitionCount)
    tic;
    
    for(simulationIndex=1:size(simulationGridSearchParameters,1))
        for(hyperparameterIndex=1:size(simulationGridSearchParameters,2))
            eval(['RSVPKeyboardParams.' RSVPKeyboardParams.Simulation.HyperparameterNames{hyperparameterIndex} '=' num2str(simulationGridSearchParameters(simulationIndex,hyperparameterIndex)) ';']);
        end
        
        RSVPKeyboardTaskObject=feval(sessionType,imageStructs,RSVPKeyboardParams,scoreStruct,1);
        
        imageStructIDList=[imageStructs.ID];
        deleteCharacterID=imageStructIDList(strcmp({imageStructs.Name},'DeleteCharacter'));
        
        results.decideNextFlag=1;
        results.trialLabels=[];
        results.completedSequenceCount=0;
        results.duration=0;
        [~,decision]=decisionMaker(results,RSVPKeyboardTaskObject);
        
        continueMainLoop=true;
        while continueMainLoop
            
            if(isempty(RSVPKeyboardTaskObject.currentInfo.phrase.incorrectSection))
                targetSymbol=imageStructIDList(strcmp({imageStructs.Name},RSVPKeyboardTaskObject.currentInfo.phrase.target(1:end-1)));

                
            else %Backspace
                targetSymbol=deleteCharacterID;
            end
            
            resultTargetness=decision.nextSequence.trials==targetSymbol;
            targetCount=sum(resultTargetness);
            
            results.scores=zeros(length(resultTargetness),1);
            results.scores(resultTargetness)=scoreStruct.conditionalpdf4targetKDE.getSample(targetCount);
            results.scores(~resultTargetness)=scoreStruct.conditionalpdf4nontargetKDE.getSample(length(resultTargetness)-targetCount);
            results.trialLabels=decision.nextSequence.trials;
            results.completedSequenceCount=1;
            results.duration=length(resultTargetness)*presentationStruct.Duration.Trial+presentationStruct.Duration.Fixation;
            
            [continueMainLoop,decision]=decisionMaker(results,RSVPKeyboardTaskObject);
        end
        
        sessionInfo = RSVPKeyboardTaskObject.saveTaskHistory();
        
        if(firstIterationBool)
            simulationResults.successfullyCompletedFlag=zeros([RSVPKeyboardParams.Simulation.MonteCarloRepetitionCount,length(sessionInfo.taskHistory),cellfun(@length,RSVPKeyboardParams.Simulation.HyperparameterValues)]);
            simulationResults.sequenceCounter=simulationResults.successfullyCompletedFlag;
            simulationResults.typingDuration=simulationResults.successfullyCompletedFlag;
            simulationResults.targetPhraseLength=zeros(length(sessionInfo.taskHistory),1);
            for(phraseIndex=1:length(sessionInfo.taskHistory))
                simulationResults.targetPhraseLength(phraseIndex)=length(sessionInfo.taskHistory{phraseIndex}.target);
            end
            firstIterationBool=false;
        end
        
        for(phraseIndex=1:length(sessionInfo.taskHistory))
            resultLocation=num2cell([repetition,phraseIndex,simulationGridSearchParameterIndices(simulationIndex,:)]);
            simulationResults.successfullyCompletedFlag(resultLocation{:})=sessionInfo.taskHistory{phraseIndex}.successfullyCompletedFlag;
            simulationResults.sequenceCounter(resultLocation{:})=sessionInfo.taskHistory{phraseIndex}.sequenceCounter;
            simulationResults.typingDuration(resultLocation{:})=sessionInfo.taskHistory{phraseIndex}.typingDuration;
        end
    end
    disp(['Simulation ' num2str(repetition) '/' num2str(RSVPKeyboardParams.Simulation.MonteCarloRepetitionCount) ' completed in ' num2str(toc) ' seconds.']);
end
end