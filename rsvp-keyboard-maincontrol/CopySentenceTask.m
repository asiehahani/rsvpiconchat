%% CopyPhraseTask
% CopyPhraseTask is the controller class for copy-phrase task and it inherits CopyTask. It decides on the next trials to show, decides if the session is
% complete, decides if copying status of a phrase, makes decision, prepares the feedback screen to
% send to the presentation and saves information to be saved.
%
%%
classdef CopySentenceTask < CopyTask
    %% Constant properties of the CopyPhraseTask class
    properties (Constant)
        % copyPhraseSentencesFile is the name of the txt file containing the list of the sentences to be
        % used in the copyphrase experiment. (string)
        copyPhraseSentencesFile='copyphraseFullSentences.txt';
        
        % copyPhraseSentencesFile4Simulation is the name of the txt file containing the list of the
        % sentences to be used in the simulation mode for copyphrase task. (string)
        copyPhraseSentencesFile4Simulation='copyphraseSentences4Simulation.txt';
    end
    
    %% Properties of the CopyPhraseTask class
    properties
        % fullPhraseList is a cell vector of structs containing preTarget, target and postTarget
        % fields corresponding to the phrase list to be used.
        fullPhraseList
    end
    
    %% Methods of the CopyPhraseTask class
    methods
        %% self=CopyPhraseTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode)
        % Constructor for the CopyPhraseTask.
        % The inputs of the constructor
        %   symbolStructArray - a struct vector containing list of symbols
        %   or images used in presentations. Loaded using xls2Structs
        %   function called on imageList.xls.
        %
        %   RSVPKeyboardParams - RSVPKeyboard parameters from the
        %   parameter file RSVPKeyboardParameters.m.
        %
        %   scoreStruct - A calibration file containing the kernel density
        %   estimators.
        %
        %   operationMode - Mode of operation.
        %                   0 : session
        %                   1 : simulation
        %
        function self=CopySentenceTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode)
            if(~exist('scoreStruct','var'))
                scoreStruct=[];
            end
            if(~exist('operationMode','var'))
                operationMode=0;
            end
            
            self=self@CopyTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode);
            
            self.pInitializeTaskUpdateCriteria(RSVPKeyboardParams.copyphrase.StoppingCriteria);
            
            self.currentInfo.phraseIndex=1;
            self.pInitializeCurrentPhraseInfo();
            self.reformatPresentationData();
            self.nextSequenceInfoStruct.Rule = RSVPKeyboardParams.Typing.nextSequenceDecisionRule;
            self.nextSequenceInfoStruct.NumberofTrials=RSVPKeyboardParams.Typing.NumberofTrials;
        end
        
        %% checkTaskUpdateCriteria(self)
        % checkTaskUpdateCriteria checks if the session is ended by reaching the end of the sentence
        % list. It also inherits the corresponding function from the parent class, CopyTask.
        function checkTaskUpdateCriteria(self)
            self.checkTaskUpdateCriteria@CopyTask();
            
            if(self.currentInfo.phrase.endedFlag)
                if(self.currentInfo.phraseIndex==length(self.fullPhraseList))
                    self.currentInfo.taskEndedFlag=true;
                else
                    self.currentInfo.phraseIndex = self.currentInfo.phraseIndex+1;
                end
            end
        end
    end
    
    methods (Access = protected)
        %% pInitializeTaskUpdateCriteria(self,copyphraseStoppingCriteria)
        % Inherits the task update criteria from the copyTask class.
        %
        % Input:
        %       copyphraseStoppingCriteria - the structure containing the stopping criteria as its
        %       fields. (RSVPKeyboardParams.copyphrase.StoppingCriteria)
        function pInitializeTaskUpdateCriteria(self,copyphraseStoppingCriteria)
            self.pInitializeTaskUpdateCriteria@CopyTask(copyphraseStoppingCriteria);
        end
        
        %% pInitializeCurrentPhraseInfo(self)
        % Initializes the next phrase to be copied by loading the phrase from the fullPhraseList.
        function pInitializeCurrentPhraseInfo(self)
            if(~self.currentInfo.taskEndedFlag)
                self.currentInfo.phrase.preTarget=self.fullPhraseList{self.currentInfo.phraseIndex}.preTarget;
                self.currentInfo.phrase.target=self.fullPhraseList{self.currentInfo.phraseIndex}.target;
                self.currentInfo.phrase.postTarget=self.fullPhraseList{self.currentInfo.phraseIndex}.postTarget;
                self.currentInfo.phrase.targetfields=self.fullPhraseList{self.currentInfo.phraseIndex}.targetfields;
                self.currentInfo.phrase.field=self.fullPhraseList{self.currentInfo.phraseIndex}.field;

                self.pInitializeCurrentPhraseInfo@CopyTask();
            end
        end
        
        %% pReadSentenceList(self)
        % Reads the phrase list from the respective sentence list file and initializes the
        % fullPhraseList property.
        function pReadSentenceList(self)
            tempSentenceList=linkedList;
            if(self.operationMode)
                fid=fopen(self.copyPhraseSentencesFile4Simulation);
            else
                fid=fopen(self.copyPhraseSentencesFile);
            end
            sentence=fscanf(fid,'%s,');
            while(~isempty(sentence))
                fieldBegining=find(sentence=='(');
                fieldEnd=find(sentence==')');
                phrase.targetfields=sentence(fieldBegining+1:fieldEnd-1);
                phrase.target=sentence(1:fieldBegining-1);
                phrase.preTarget=[];
                phrase.postTarget=[];
                phrase.field='FullPhrase';
                tempSentenceList.insertEnd(phrase);
                sentence=fscanf(fid,'%s,');
            end
            fclose(fid);
            self.fullPhraseList=tempSentenceList.toCell();
        end
        
        %% pUpdateCurrentPhraseInfo(self,results)
        % Updates the status and information of the phrase, i.e updates phrase field of the
        % currentInfo property, and checks the task update criteria.
        function pUpdateCurrentPhraseInfo(self,results)
            self.currentInfo.phrase.typingDuration=self.currentInfo.phrase.typingDuration+results.duration;
            self.currentInfo.phrase.sequenceCounter=self.currentInfo.phrase.sequenceCounter+results.completedSequenceCount;
            
            self.currentInfo.phrase.typedText=self.languageModelWrapperObject.getText();
            decisionfields=self.languageModelWrapperObject.getStates;
%             state=self.languageModelWrapperObject.state;
            [self.currentInfo.phrase.successfullyCompletedFlag,self.currentInfo.phrase.correctSection,self.currentInfo.phrase.incorrectSection]=checkSentenceTypingCorrectness(self.currentInfo.phrase.target,self.currentInfo.phrase.typedText);
            allFields=find(decisionfields=='_');
            numberOfCorrectFields= length(find(self.currentInfo.phrase.correctSection=='_'));
            if ~isempty(decisionfields) && numberOfCorrectFields~=0
            self.currentInfo.phrase.decisionfieldspositive=decisionfields(1:allFields(numberOfCorrectFields));
            self.currentInfo.phrase.decisionfieldsnegative=decisionfields(allFields(numberOfCorrectFields)+1:end);
            elseif ~isempty(decisionfields) && numberOfCorrectFields==0
            self.currentInfo.phrase.decisionfieldspositive=[];
            self.currentInfo.phrase.decisionfieldsnegative=decisionfields;
            else
            self.currentInfo.phrase.decisionfieldspositive=[];
            self.currentInfo.phrase.decisionfieldsnegative=[];
            end
%             if ~isempty(decisionfields) && ~isempty(state)
%                 if isempty(strfind(self.currentInfo.phrase.targetfields,state))
%                     self.currentInfo.phrase.nulC=0;
%                 else 
%                     self.currentInfo.phrase.nulC=length(allFields)+1;
%                 end
%             end
            self.checkTaskUpdateCriteria();
            
        end
        
    end
    
    
end