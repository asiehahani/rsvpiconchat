%% startAcquisition
% Start amplifiers
%   in case gUSBAmp : call startAmps to starts data acquisition
%                     return success : 0/1 0 represents fail , 1 presents
%                     success
%   in case noAmp   : record starting time
%%
switch RSVPKeyboardParams.DAQType
    case 'gUSBAmp'
        [success]=startAmps(amplifierStruct,recordingFilename,'Index');
    case 'noAmp'
        amplifierStruct.lastAcquisitionTimeStamp=handleVariable;
        amplifierStruct.lastAcquisitionTimeStamp.data=tic;
end