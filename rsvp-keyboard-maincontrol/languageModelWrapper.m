%% languageModelWrapper Class
% languageModelWrapper is a Matlab interface for the language model. It sets up the language model
% probabilities to be used in fusion. In addition to original language model, it can handle
% additional symbols and perform modifications on the original probability mass.
%%

classdef languageModelWrapper < handle
    %% Properties of the languageModelWrapper Class
    properties
        %%
        % languageModelClient is the btlm object. It is the Matlab interface for the language model.
        languageModelClient
        
        %%
        % trialIDs is the vector of symbol IDs corresponding to trial symbols.
        trialIDs
        
        %%
        % trialNames is the cell vector of symbol names corresponding to trial symbols.
        trialNames
        
        
        %%
        %
        trialLanguageModelSymbols
        languageModelWrapperParameters
        languageModelClientTrialIndices
        
        lastPosteriorProbs
        lastDecided
        lastDeleted
        
        specialSymbolIndices
        typingQueue
        state
        allField
        operationModeLanguageModel
        consequtiveState
        getStates
    end
    
    methods
        function self=languageModelWrapper(languageModelClient,imageStructs,languageModelWrapperParameters)
            self.languageModelClient=languageModelClient;
            self.operationModeLanguageModel='spell';
            self.consequtiveState={'verb','subject','object','subjmod','objmod'};
            self.state=self.consequtiveState{1};
            self.getStates=[];
            trialStructIndices=([imageStructs.IsTrial]==1);
            self.trialNames={imageStructs(trialStructIndices).Name};
            self.trialIDs=[imageStructs(trialStructIndices).ID];
            self.trialLanguageModelSymbols={imageStructs(trialStructIndices).LanguageModelSymbol};
            self.allField={imageStructs(trialStructIndices).Field};
            languageModelSymbols=languageModelClient.symbol;
            self.languageModelClientTrialIndices=zeros(1,length(self.trialIDs));
            for(trialIndex=1:length(self.trialIDs))
                temp=find(strcmp(self.trialLanguageModelSymbols(trialIndex),languageModelSymbols));
                if(~isempty(temp))
                    self.languageModelClientTrialIndices(trialIndex)=temp;
                end
            end
            self.languageModelWrapperParameters=languageModelWrapperParameters;
            self.lastPosteriorProbs=ones(1,length(self.trialIDs))/length(self.trialIDs);
            self.lastDecided=0;
            self.typingQueue=linkedList;
        end
        
        function p=getProbs(self)
            
            p=zeros(length(self.trialIDs),1);
            tempProbsLM=self.languageModelClient.probs';
            %p(self.trialLanguageModelSymbols)
            p=tempProbsLM(1:length(p));
            
            if ~all(~isnan(p))
                p(1:length(p))=1/length(p);
            end
            %             if ~strcmpi(self.state,'field')
            %                 p(1:5)=1/5;
            %                 p(6:end)=0;
            %             else
            p(end)=0;
%             p(end-1)=0;
            
            
            fieldID=[];
            fieldID=[fieldID find(~cellfun('isempty',strfind(self.allField,self.state)))];
            fieldID=unique(fieldID);
            
            if(self.typingQueue.elementCount~=0)
                p(end)=self.languageModelWrapperParameters.fixedProbability.DeleteCharacter;
                fieldID=[fieldID find(strcmpi(self.allField,'all'))];
%             if ~strcmpi(self.operationModeLanguageModel,'copyPhrase')
%                 p(end-1)=self.languageModelWrapperParameters.fixedProbability.NullCharacter;
%                 fieldID=[fieldID find(strcmpi(self.allField,'none'))];
%             end
            end
%             p=p/sum(p);

            pFiledID=p(fieldID);
            if all(~pFiledID)
                pFiledID(1:length(pFiledID))=1/length(pFiledID);
            end
            pTemp=pFiledID/sum(pFiledID);
           
            p=zeros(size(p));
            p(fieldID)=pTemp;
            
            %             end
            
        end
        
        function update(self,decision)
            
            self.lastPosteriorProbs=decision.posteriorProbs;
            self.lastDecided=decision.decided;
            if(self.languageModelClientTrialIndices(decision.decided)>0)
                
                %                 if ~strcmpi(self.state,'field')
                self.getStates=[self.getStates self.state '_'];
                self.languageModelClient.update(self.trialNames{decision.decided});
                self.typingQueue.insertEnd(listNode(decision.decided));
                if ~strcmpi(self.operationModeLanguageModel,'copyPhrase')
                    direction='next'; 
                    self.changeState(direction);
                end
                
               
               
                %                 else
                %                     ind=find(ismember(self.consequtiveState,self.trialNames{[self.trialIDs]==decision.decided}));
                %                     if ~isempty(ind)
                %                         nodeNum=length(self.consequtiveState)-ind;
                %                         self.consequtiveState(ind)=[];
                %                         if nodeNum==0
                %                         self.typingQueue.remove(self.typingQueue.Tail);
                %                         self.languageModelClient.undo();
                %                         else
                %                         self.typingQueue.removeInMiddle(self.typingQueue.Tail,nodeNum);
                %                         self.languageModelClient.removeInMiddle(ind);
                %                         end
                %                     end
                %                 end
                %self.text=[self.text self.trialTexts{decision.decided}];
            else
                if(strcmp(self.trialNames{decision.decided},'DeleteCharacter'))
                    if ~isempty(self.getStates)
                        ind=find(self.getStates=='_');
                        if length(ind)>1
                        self.getStates=self.getStates(1:ind(end-1));
                        else
                          self.getStates=[];
                        end
                    end
                    self.languageModelClient.undo();
                    if(self.typingQueue.elementCount~=0)
                        self.typingQueue.remove(self.typingQueue.Tail);
                    end
                    
                    if ~strcmpi(self.operationModeLanguageModel,'copyPhrase')
                        direction='back';
                        self.changeState(direction);
                    end
                    
                end
                
            end
            
            
            
        end
        
        function text=getText(self)
            
            cell=self.typingQueue.toCell;
            text=[];
            for count=1:length(cell)
                text=[text self.trialNames{cell{count}} '_'];
            end
            
            
            %             textTemp=cell2mat(self.trialTexts(cell2mat(self.typingQueue.toCell)));
            %              text=[];
            %              for count=1:length(textTemp)
            %                  text=[text self.trialNames{strcmp(self.trialTexts,textTemp(count))}];
            %                  text=[cellstr(text) '_']
            %              end
            if(isempty(text))
                text='';
            end
            
        end
        
        function reset(self)
            
            self.languageModelClient.reset;
            self.languageModelClient.alloc;
            self.lastPosteriorProbs=ones(1,length(self.trialIDs))/length(self.trialIDs);
            self.lastDecided=0;
            self.lastDeleted=0;
            self.typingQueue=linkedList;
            self.getStates=[];
        end
        
        function changeState(self,direction)
            
            ind=find(ismember(self.consequtiveState,self.state));
            if strcmpi(direction,'next')
                if ind<length(self.consequtiveState)
                    self.state=self.consequtiveState{ind+1};
                else
                    self.state=self.consequtiveState{1};
                end
            else
                if ind>1
                    self.state=self.consequtiveState{ind-1};
                end
             end
        end
            
            function initializeState(self,text1,text2,field,targetFields)
                
                self.reset;
                intervals=[0 find(text1=='_')];
                for(characterIndex=1:length(intervals)-1)
                    symbol2update=text1(intervals(characterIndex)+1:intervals(characterIndex+1)-1);
                    self.languageModelClient.update(self.trialLanguageModelSymbols{strcmpi(self.trialLanguageModelSymbols,symbol2update)});
                end
                intervals=[0 find(text2=='_')];
                for(characterIndex=1:length(intervals)-1)
                    symbol2update=text2(intervals(characterIndex)+1:intervals(characterIndex+1)-1);
                    self.languageModelClient.update(self.trialLanguageModelSymbols{strcmpi(self.trialLanguageModelSymbols,symbol2update)});
                end
                if ~strcmpi(field,'FullPhrase')
                    self.state=field;
                    self.operationModeLanguageModel='copyPhrase';
                else
                    i=1;
                    self.consequtiveState={'verb','subject','object','subjmod','objmod'};

                    for count=1:length(self.consequtiveState)
                        if strfind(targetFields,self.consequtiveState{count})
                            tempStates{i}=self.consequtiveState{count};
                            i=i+1;
                        end
                    end
                    self.consequtiveState=tempStates;
                    self.state=self.consequtiveState{1};
                end
                %             fieldID=[];
                %                 fieldID=[fieldID find(~cellfun('isempty',strfind(self.allField,self.state)))];
                %                 fieldID=unique(fieldID);
                %                 fieldID=[fieldID find(strcmpi(self.allField,'all'))];
                %                 fieldID =fieldID(fieldID>5);
            end
            
        
    end
end
