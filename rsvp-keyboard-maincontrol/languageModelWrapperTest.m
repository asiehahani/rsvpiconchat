  % iconlm.test()
        %   Runs a battery of unit tests for this module.
        %
        % Example:
        %   iconlm.test() % Assert for failures
            % Reset and Init
% 
global BCIframeworkDir
BCIframeworkDir='.';
addpath(genpath('.'))



            disp('Running tests...');
            imageStructs = xls2Structs('iconImageList.xls');
            languageModelWrapperParameters.specialProbCalcType='fixed';
            languageModelWrapperParameters.fixedProbability.DeleteCharacter=0.05;
            languageModelWrapperParameters.LANGUAGEMODELSCALINGHACK=0.5;
            
         

            languageModelClient=iconlm(imageStructs,'crowd.stats'); 
            languageModelClient.initRemote;
            languageModelClient.alloc;
            
            
            
            
            
            % languageModelWrapper
            self = languageModelWrapper(languageModelClient,imageStructs,languageModelWrapperParameters);                     
            assert(isempty(setdiff(self.trialNames,{imageStructs(1:28).Name})));
            assert(isempty(setdiff(self.trialIDs,[imageStructs(1:28).ID])));
            assert(isempty(setdiff(self.trialLanguageModelSymbols,{imageStructs(1:28).LanguageModelSymbol})));
            assert(isempty(setdiff(self.trialTexts,{imageStructs(1:28).Text})));
            assert(length(self.lastPosteriorProbs)==length(self.trialIDs));
            assert(self.lastDecided==0);
            
            
            assert(length(self.getProbs())==length(self.trialIDs));
            
           % reset
            %self = languageModelWrapper(btlm,imageStructs,languageModelWrapperParameters);   
            self.reset();
            assert(self.lastDecided==0);
            assert(self.lastDeleted==0);
            
            % update  & getText
           % self = languageModelWrapper(btlm,imageStructs,languageModelWrapperParameters);   
            decision.decided=10;
            decision.posteriorProbs=randn(1,28);
            self.update(decision);
            text=self.getText();
            assert(strcmpi(text,'eat_'))            
            
            display('success')
            self.initializeState('i_','veri_thirsti_','verb');
            self.getText()
            self.update(15);
           
           