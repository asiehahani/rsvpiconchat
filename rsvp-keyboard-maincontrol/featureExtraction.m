%% function results = featureExtraction(dataBufferObject,triggerPartitionerStruct,featureExtractionStruct)
% Extracts the feature(s) from the data.
%   Inputs : dataBufferObject : an object that stores  data
%            triggerPartitionerStruct :
%            featureExtractionStruct : a struct for feature extraction
%
%   Outputs : results : a struct with following properties :
%             scores :
%             trialLabels : the label for each trial
%             completedSequenceCount : number of completed sequence
%             decideNextflag : a flag that can tell whether
%                              completedSequenceCount >0
%%
function results = featureExtraction(dataBufferObject,...
    cleanDataBufferObject,...
    artifactInfoBufferObject,...
    triggerPartitionerStruct,...
    featureExtractionStruct,...
    useArtifactFilteredData,...
    artifactFilteringEnabled)


if(featureExtractionStruct.sessionID==1)
    results.scores=[];
    results.trialLabels=[];
else
    if(featureExtractionStruct.completedSequenceCount>0 && featureExtractionStruct.rejectSequenceFlag==0)
        if useArtifactFilteredData && artifactFilteringEnabled
            trialData=zeros(triggerPartitionerStruct.windowLengthinSamples,cleanDataBufferObject.numberofChannels,length(featureExtractionStruct.trialSampleTimeIndices));
            for(trialIndex=1:length(featureExtractionStruct.trialSampleTimeIndices))
                trialData(:,:,trialIndex) = cleanDataBufferObject.getOrderedData(featureExtractionStruct.trialSampleTimeIndices(trialIndex),featureExtractionStruct.trialSampleTimeIndices(trialIndex)+triggerPartitionerStruct.windowLengthinSamples-1);
            end
        else  %% means if useArtifactFilteredData=0 and we want to use dataBuffer
            trialData=zeros(triggerPartitionerStruct.windowLengthinSamples,dataBufferObject.numberofChannels,length(featureExtractionStruct.trialSampleTimeIndices));
            for(trialIndex=1:length(featureExtractionStruct.trialSampleTimeIndices))
                trialData(:,:,trialIndex) = dataBufferObject.getOrderedData(featureExtractionStruct.trialSampleTimeIndices(trialIndex),featureExtractionStruct.trialSampleTimeIndices(trialIndex)+triggerPartitionerStruct.windowLengthinSamples-1);
            end
        end
        
        results.scores=featureExtractionStruct.Flow.operate(trialData);
        results.trialLabels=featureExtractionStruct.trialLabels;
    else
        results.scores=[];
        results.trialLabels=[];
        results.duration=0;
    end
    
end

% if(featureExtractionStruct.completedSequenceCount>0 || featureExtractionStruct.rejectSequenceFlag==1)
 if(featureExtractionStruct.completedSequenceCount>0)
    results.completedSequenceCount=featureExtractionStruct.completedSequenceCount;
    results.decideNextFlag=true;
    results.duration=(featureExtractionStruct.trialSampleTimeIndices(length(featureExtractionStruct.trialSampleTimeIndices))-featureExtractionStruct.trialSampleTimeIndices(1)+triggerPartitionerStruct.windowLengthinSamples)/featureExtractionStruct.fs;
else
    results.decideNextFlag=false;
end



