%% sendDecisionPacket(decision)
% sendDecisionPacket is the function that communicates with the presentation. Input decision is a
% structure that might contain a subset of the following fields to be sent to the presentation.
% Possible fields which can be sent to the presentation are,
%
% * decision.decided - Index of the decided symbol
% * decision.nextSequence.target - Index of the target symbol to be shown
% * decision.nextSequence.trials - Index of the trial symbols to be shown
% * decision.feedback - cell vector containing the list of feedback texts. Each element is a
% structure with Type, for the color of text, and Text fields.
%
%%

function sendDecisionPacket(decision,imageStructs)
global main2presentationCommObject
global BCIpacketStruct

persistent NT
persistent PT
persistent loopCounti

if isempty(loopCounti)
    loopCounti=2;
end

packet2send.header =  BCIpacketStruct.HDR.MESSAGE;
packet2send.data = '';

if(isfield(decision,'decided') && ~isempty(decision.decided))
    packet2send.data = [packet2send.data 'D=[' sprintf('%d,',decision.decided) ';'];
    packet2send.data(end-1)=']';
    NT=1;
    PT=1;
    if decision.decided==100
        DC=0;
    else
        DC=1;
    end
end
if(isfield(decision,'showTarget') && ~isempty(decision.showTarget))
    packet2send.data = [packet2send.data 'ST=[' sprintf('%d,',decision.showTarget) ';'];
    packet2send.data(end-1)=']';
end
if(isfield(decision,'nextSequence'))
    if(isfield(decision.nextSequence,'target') && ~isempty(decision.nextSequence.target))
        packet2send.data = [packet2send.data sprintf('T=%d',decision.nextSequence.target) ';'];
    end
    if(isfield(decision.nextSequence,'trials') && ~isempty(decision.nextSequence.trials))
        packet2send.data = [packet2send.data 't=[' sprintf('%d,',decision.nextSequence.trials) ';'];
        packet2send.data(end-1)=']';
    end
end
if(isfield(decision,'feedback'))
%     save('decision','decision')
    for(feedbackIndex=1:length(decision.feedback))
        switch decision.feedback{feedbackIndex}.Type
            
%             case 'fullSentence'
%                 feedbackHeader='6';
% %                 greyID=imageStructs(find(strcmpi({imageStructs.Name},'greyIcon'))).ID;
% %                 whiteID=imageStructs(find(strcmpi({imageStructs.Name},'whiteIcon'))).ID;
% %                 feedbackHeader=[feedbackHeader ',' num2str(greyID) ',' num2str(greyID) ',' num2str(greyID) ',' num2str(greyID) ',' num2str(greyID)];
%                 intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
%                 
%                 for count=1:length(intervals)-1
%                     packet2send.data=[packet2send.data 'l{' num2str(count) '}=''' decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1) ''';'];
%                 end
                
            case 'neutral'
                feedbackHeader='0';
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                    wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                    feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
                                
            case 'positive'
                
                %%%%% check if it is full phrase and arrange the target
                %%%%% sentence
                
                if isfield(decision.feedback{feedbackIndex},'TargetFields')
                intervals=[0 find(decision.feedback{feedbackIndex}.TargetFields=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.TargetFields(intervals(count)+1:intervals(count+1)-1);
                    packet2send.data=[packet2send.data 'tf{' num2str(count) '}=''' word ''';'];
                end
                end
                
                feedbackHeader='1';
                
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
                
                
                
                
                
            case 'negative'
                feedbackHeader='2';
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
            case 'next'
                feedbackHeader='3';
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                    wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                    feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
             case 'nextNegative'                 
                 feedbackHeader='4';
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                    word
                    wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                    feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
                if isfield(decision.feedback{feedbackIndex},'decisionfieldsnegative')
                intervals=[0 find(decision.feedback{feedbackIndex}.decisionfieldsnegative=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.decisionfieldsnegative(intervals(count)+1:intervals(count+1)-1);
                    packet2send.data=[packet2send.data 'dfn{' num2str(count) '}=''' word ''';'];
                end
                if(isfield(decision,'nextSequence'))
                if(isfield(decision.nextSequence,'trials') && ~isempty(decision.nextSequence.trials))
%                 if NT
% %                 loopCounti=loopCounti+1;
% %                 if loopCounti>5
% %                    loopCounti=2; 
% %                 end
%                 end
                packet2send.data = [packet2send.data 'NT=[' sprintf('%d,',NT) ';'];
                packet2send.data(end-1)=']';
                NT=0;
                end
                end
                
                end
              case 'nextPositive'
                feedbackHeader='5';
                intervals=[0 find(decision.feedback{feedbackIndex}.Text=='_')];
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.Text(intervals(count)+1:intervals(count+1)-1);
                    wordID=imageStructs(find(strcmpi({imageStructs.Name},word))).ID;
                    feedbackHeader=[feedbackHeader ',' num2str(wordID)];
                end
                
                if isfield(decision.feedback{feedbackIndex},'decisionfieldspositive')
%                 k={'verb' 'subject' 'object' 'subjmod' 'objmod'};
%                 ind(1:5)=0;
%                 for i=1:length(k)
%                     fieldCount=strfind(decision.feedback{feedbackIndex-2}.TargetFields,k{i});
%                     if ~isempty(fieldCount)
%                         ind(i)=1;
%                     end
%                 end
                intervals=[0 find(decision.feedback{feedbackIndex}.decisionfieldspositive=='_')];
%                 nextFieldInd=length(intervals);
%                 if ind(nextFieldInd)==0
%                 end
                for count=1:length(intervals)-1
                    word=decision.feedback{feedbackIndex}.decisionfieldspositive(intervals(count)+1:intervals(count+1)-1);
                    packet2send.data=[packet2send.data 'dfp{' num2str(count) '}=''' word ''';'];
                end
                if(isfield(decision,'nextSequence'))
                if(isfield(decision.nextSequence,'trials') && ~isempty(decision.nextSequence.trials))
                if length(decision.feedback)<4
%                 if PT
% %                 if ind(loopCounti)==0
% %                     nullC=0;
% %                 else
% %                     nullC=length(intervals);
% %                 end
% %                 if DC
% %                 loopCounti=loopCounti+1;
% %                 end
% %                 if loopCounti>5
% %                    loopCounti=2; 
% %                 end
%                 nullC=decision.feedback{feedbackIndex}.nulC;
                packet2send.data = [packet2send.data 'nullC=[' sprintf('%d,',length(intervals)) ';'];
                packet2send.data(end-1)=']';
%                 end
                packet2send.data = [packet2send.data 'PT=[' sprintf('%d,',PT) ';'];
                packet2send.data(end-1)=']';      
                PT=0;
                end
                end
                end
                end
                
                   
        end      
        packet2send.data=[packet2send.data 'f{' num2str(feedbackIndex) '}=' '[' feedbackHeader '];'];
    end
end 

if isfield(decision, 'posteriorProbs')

    packet2send.data = [packet2send.data,'p=[',sprintf('%d,',decision.posteriorProbs),';'];
    packet2send.data(end - 1) = ']';

end

[success] = t_sendBCIpacket(main2presentationCommObject,packet2send);
