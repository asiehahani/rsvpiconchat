%% CopyTask
% CopyTask inherits RSVPKeyboardTask and it is the parent class for the CopyPhraseTask and
% MasteryTask classes. It decides on the next trials to show, decides if the session is
% complete, decides if copying status of a phrase, makes decision, prepares the feedback screen to
%send to the presentation and saves information to be saved.
%
%%
classdef CopyTask < RSVPKeyboardTask
    %% Methods of the CopyTask class
    methods
        %% self=CopyTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode)
        % Constructor for the CopyTask. This constructor is called
        % from individual tasks to make common initializations of CopyTask.
        % The inputs of the constructor
        %   symbolStructArray - a struct vector containing list of symbols
        %   or images used in presentations. Loaded using xls2Structs
        %   function called on imageList.xls.
        %
        %   RSVPKeyboardParams - RSVPKeyboard parameters from the
        %   parameter file RSVPKeyboardParameters.m.
        %
        %   scoreStruct - A calibration file containing the kernel density
        %   estimators.
        %
        %   operationMode - Mode of operation.
        %                   0 : session
        %                   1 : simulation
        %
        function self=CopyTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode)
            self=self@RSVPKeyboardTask(symbolStructArray,RSVPKeyboardParams,scoreStruct,operationMode);
            self.pReadSentenceList();
            self.taskHistory=linkedList;
        end
        
        %% updateDecisionState(self,results)
        % updateDecisionState updates the decisionClass object which makes
        % decisions using the results obtained from the feature extraction
        % stage. If the current phrase ended it prepares the next phrase and initializes the
        % decisionClass object for the next phrase.
        %
        % Input:
        %       results - a structure containing one dimensional features
        %       corresponding to each trial
        %
        %       results.trialLabels - the labels of the trials (vector of
        %       indices)
        %
        %       results.decideNextFlag - a boolean indicator indicates if a
        %       new sequence is expected to be shown.
        %
        %       results.completedSequenceCount - the number of sequences
        %       completed and contained in the results structure
        %
        %       results.duration - the duration of the sequence(s)
        %
        function updateDecisionState(self,results)
            self.updateDecisionState@RSVPKeyboardTask(results);
            self.pUpdateCurrentPhraseInfo(results);
            if(self.currentInfo.epochEndedFlag)
                self.reformatPresentationData();
            end
            
            if(self.currentInfo.phrase.endedFlag)
                self.pSavePhraseInfo();
                
                self.decisionObj.resetEpochList();
                self.pInitializeCurrentPhraseInfo();
            end
        end
        
        %% nextSequence=decideNextTrials(self)
        % Decide on the next sequence to present.
        %
        %
        % Output:
        %       nextSequence - a structure containing the information on
        %       the next sequence.
        %
        %           nextSequence.trials - the ordered list of trial indices
        %           (vector)
        %
        %           nextSequence.target - the index corresponding to the
        %           target symbol
        %
        function nextSequence=decideNextTrials(self)
            self.reformatPresentationData();
            fullSet=self.decisionObj.trialIDs;
            switch self.nextSequenceInfoStruct.Rule
                case 'Random'
                    nextSequence.trials=fullSet(randperm(length(fullSet),self.nextSequenceInfoStruct.NumberofTrials));
                case 'Posterior'
                    [~,sortedTrialIndices]=sort(self.decisionObj.currentInfo.posteriorProbs,'descend');
                    selectedSet=fullSet(sortedTrialIndices(1:self.nextSequenceInfoStruct.NumberofTrials));
                    nextSequence.trials=selectedSet(randperm(length(selectedSet)));
                case 'nonZero'
                    noZeroProbs= find(self.decisionObj.currentInfo.posteriorProbs~=0);
                    while length(noZeroProbs)<10
                        tempFullSet=10:length(fullSet);
                        randInd=randperm(length(tempFullSet));
                        selectedSet=tempFullSet(randInd(1:9));
                        if isrow(noZeroProbs)
                            noZeroProbs=[noZeroProbs selectedSet];
                        else
                            noZeroProbs=[noZeroProbs; selectedSet'];
                        end
                        noZeroProbs=unique(noZeroProbs);
                    end
                    if length(noZeroProbs)>20
                        tempSet=fullSet(noZeroProbs);
                        [~,sortedTrialIndices]=sort(tempSet,'descend');
                        ind1=find(self.currentInfo.phrase.target=='_');
                        ind2=find(self.currentInfo.phrase.correctSection=='_');
                        if isempty(ind2)
                            target=self.currentInfo.phrase.target(1:ind1(1));
                        else
                            ind3=find(ind1==ind2(end)); 
                            target=self.currentInfo.phrase.target(ind1(ind3)+1:ind1(ind3+1));
                        end
                        
                        targetID=find(strcmpi({self.symbolStructArray.Name},target(1:end-1))==1);
                        tempSet=tempSet(sortedTrialIndices(1:20));
                        deleteID=100;
                        if self.decisionObj.currentInfo.posteriorProbs(deleteID)~=0 && ~ismember(deleteID,tempSet)
                            tempSet(1)=deleteID;
                        end
                        
                        if ~ismember(targetID,tempSet)
                            tempSet(end)=targetID;
                        end
                        tempSet=tempSet(randperm(length(tempSet)));
                        nextSequence.trials=tempSet;
                    else
                        ind1=find(self.currentInfo.phrase.target=='_');
                        ind2=find(self.currentInfo.phrase.correctSection=='_');
                        if isempty(ind2)
                            target=self.currentInfo.phrase.target(1:ind1(1));
                        else
                            ind3=find(ind1==ind2(end)); 
                            target=self.currentInfo.phrase.target(ind1(ind3)+1:ind1(ind3+1));
                        end
                        
                        targetID=find(strcmpi({self.symbolStructArray.Name},target(1:end-1))==1);
                        tempSet=fullSet(noZeroProbs);
                        deleteID=100;

                         if self.decisionObj.currentInfo.posteriorProbs(deleteID)~=0 && ~ismember(deleteID,tempSet)
                            tempSet(1)=deleteID;
                         end
                        
                        if ~ismember(targetID,tempSet)
                            if tempSet(end)==deleteID
                               tempSet(end-1)= targetID;
                            else
                            tempSet(end)=targetID;
                            end
                        end
                        if ~ismember(targetID,tempSet)
                            tempSet(end)=targetID;
                        end
                        tempSet=tempSet(randperm(length(tempSet)));
                        nextSequence.trials=tempSet;
                    end
            end
        end
        
        %% checkTaskUpdateCriteria(self)
        % Checks if the phrase ended by reaching one of the stopping criteria or successful
        % completion of the phrase.
        function checkTaskUpdateCriteria(self)
            self.checkTaskUpdateCriteria@RSVPKeyboardTask();
            
            self.currentInfo.phrase.endedFlag=false;
            
            self.currentInfo.phrase.endedFlag=self.currentInfo.phrase.endedFlag | (self.currentInfo.phrase.typingDuration > self.taskUpdateCriteria.MaximumEstimatedPhraseTime);
            
            self.currentInfo.phrase.endedFlag= self.currentInfo.phrase.endedFlag | (self.currentInfo.phrase.sequenceCounter > self.taskUpdateCriteria.AvgMaximumNumberofSequencesperChar*length(self.currentInfo.phrase.target));
            
            self.currentInfo.phrase.endedFlag= self.currentInfo.phrase.endedFlag | self.currentInfo.phrase.successfullyCompletedFlag;
            
            self.currentInfo.phrase.endedFlag= self.currentInfo.phrase.endedFlag | (self.taskUpdateCriteria.MaximumLengthOfIncorrectSection <= length(self.currentInfo.phrase.incorrectSection));
            
             self.currentInfo.phrase.endedFlag= self.currentInfo.phrase.endedFlag | (length(find(self.currentInfo.phrase.decisionfieldspositive=='_'))+length(find(self.currentInfo.phrase.decisionfieldsnegative=='_'))==length(find(self.currentInfo.phrase.targetfields=='_')));
            
            self.currentInfo.phrase.endedFlag = self.currentInfo.phrase.endedFlag & self.currentInfo.epochEndedFlag;
        end
        
        %% reformatPresentationData(self)
        % reformatPresentationData updates the feedback text to be used in presentation.
        function reformatPresentationData(self)
            if(~self.operationMode)
                nextFeedbackIndex=1;
                self.currentInfo.decision.feedback={};
                if(~isempty(self.currentInfo.phrase.preTarget))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='neutral';
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.preTarget;
                    nextFeedbackIndex=nextFeedbackIndex+1;
                end
                
                self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='positive';
                self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.target;
                if(isfield(self.currentInfo.phrase,'targetfields'))
                self.currentInfo.decision.feedback{nextFeedbackIndex}.TargetFields=self.currentInfo.phrase.targetfields;
                end
                nextFeedbackIndex=nextFeedbackIndex+1;
                
%                 if(~isempty(self.currentInfo.phrase.targetStates))
%                     self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='targetStates';
%                     self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.targetStates;
%                     nextFeedbackIndex=nextFeedbackIndex+1;
%                 end
                
                if(~isempty(self.currentInfo.phrase.postTarget))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='neutral';
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.postTarget;
                    nextFeedbackIndex=nextFeedbackIndex+1;
                end
                
                self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='next';
                self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.preTarget;
                nextFeedbackIndex=nextFeedbackIndex+1;
                
                if(~isempty(self.currentInfo.phrase.correctSection))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='nextPositive';
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.correctSection;
                    if(isfield(self.currentInfo.phrase,'decisionfieldspositive'))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.decisionfieldspositive=self.currentInfo.phrase.decisionfieldspositive;
                    end
                    nextFeedbackIndex=nextFeedbackIndex+1;
                end
                
                if(~isempty(self.currentInfo.phrase.incorrectSection))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Type='nextNegative';
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.Text=self.currentInfo.phrase.incorrectSection;
                    if(isfield(self.currentInfo.phrase,'decisionfieldsnegative'))
                    self.currentInfo.decision.feedback{nextFeedbackIndex}.decisionfieldsnegative=self.currentInfo.phrase.decisionfieldsnegative;
                    end
                end
                
                
            end
        end
        
        %% sessionInfo=saveTaskHistory(self,saveLocation)
        % Saves the history and information of the task to the output
        % variable and to file if a file to save is given.
        %
        % Input:
        %       saveLocation - file address to save the information into,
        %       it can be omitted.
        %
        %
        % Output:
        %        sessionInfo - variable form of the information which is to
        %        be saved
        %
        function sessionInfo=saveTaskHistory(self,saveLocation)
            if(~self.currentInfo.taskEndedFlag)
                self.pSavePhraseInfo();
            end
            sessionInfo.taskHistory=self.taskHistory.toCell();
            
            sessionInfo.typingDuration=self.currentInfo.typingDuration;
            sessionInfo.globalSequenceCounter=self.currentInfo.globalSequenceCounter;
            sessionInfo.totalSessionDuration=toc(self.currentInfo.globalTic);
            
            sessionInfo.sessionType=class(self);
            if(exist('saveLocation','var'))
                save([saveLocation '\taskHistory.mat'],'sessionInfo');
            end
        end
    end
    
    methods (Access = protected)
        
        %% pInitializeTaskUpdateCriteria(self,copyphraseStoppingCriteria)
        % Initializes task update criteria for the copy tasks. It contains the stopping criteria to
        % pass to the next phrase when it is not successful. It also inherits the task stopping
        % criteria from the parent class RSVPKeyboardTask.
        function pInitializeTaskUpdateCriteria(self,copyphraseStoppingCriteria)
            self.pInitializeTaskUpdateCriteria@RSVPKeyboardTask();
            
            self.taskUpdateCriteria.MaximumEstimatedPhraseTime=copyphraseStoppingCriteria.MaximumEstimatedPhraseTime;
            self.taskUpdateCriteria.SequenceLimitScale=copyphraseStoppingCriteria.SequenceLimitScale;
            self.taskUpdateCriteria.AvgMaximumNumberofSequencesperChar=self.taskUpdateCriteria.SequenceLimitScale * self.decisionObj.decisionStoppingCriteria.MaximumNumberofSequences;
            self.taskUpdateCriteria.MaximumLengthOfIncorrectSection=copyphraseStoppingCriteria.MaximumLengthOfIncorrectSection;
        end
        
        
        %% pInitializeCurrentInfo(self)
        % Sets up the initial values of the currentInfo, which is the information container for the status of the task.
        function pInitializeCurrentPhraseInfo(self)
            self.currentInfo.phrase.typingDuration=0;
            self.currentInfo.phrase.sequenceCounter=0;
            self.currentInfo.phrase.endedFlag=false;
            self.currentInfo.phrase.successfullyCompletedFlag=false;
            self.currentInfo.phrase.correctSection='';
            self.currentInfo.phrase.incorrectSection='';
            self.currentInfo.phrase.typedText='';
            
            
            self.languageModelWrapperObject.initializeState(self.currentInfo.phrase.preTarget,self.currentInfo.phrase.postTarget,self.currentInfo.phrase.field,self.currentInfo.phrase.targetfields);
            self.decisionObj.getLanguageModelProbabilities();
        end
        
        %% pReadSentenceList(self)
        % Abstract container function for pReadSentenceList in the subclasses.
        function pReadSentenceList(self)
        end
        
        %% pUpdateCurrentPhraseInfo(self,results)
        % Abstract container function for pUpdateCurrentPhraseInfo in the subclasses.
        function pUpdateCurrentPhraseInfo(self,results)
        end
        
        %% pSavePhraseInfo(self)
        % pSavePhraseInfo prepares current phrase information to be recorded and inserts it into
        % taskHistory.
        function pSavePhraseInfo(self)
            savedPhrasePacket=self.currentInfo.phrase;
            savedPhrasePacket.epochList=self.decisionObj.epochList.toCell();
            self.taskHistory.insertEnd(savedPhrasePacket);
        end
        
    end
    
end