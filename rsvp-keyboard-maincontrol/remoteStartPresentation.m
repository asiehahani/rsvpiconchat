%% remoteStartPresentation
%   is a script that creates main2presentation TCPIP object and calls
%   the decisionMaker function for making decison with featureExtraction
%   result.
%
%   See also t_main2presentationCommInitialize, TCPIP_Parameters,
%   generateArtificialTriggers
%%
global main2presentationCommObject
global BCIpacketStruct

dos('startPresentation.bat &');
TCPIP_Parameters
[success,main2presentationCommObject,BCIpacketStruct]=t_main2presentationCommInitialize(RSVPKeyboardParams.mainIP,RSVPKeyboardParams.mainPort);

results.decideNextFlag=1;
results.trialLabels=[];
results.completedSequenceCount=0;
results.duration=0;
[~,decision]=decisionMaker(results,RSVPKeyboardTaskObject,imageStructs);

if(strcmp(amplifierStruct.DAQType,'noAmp'))
    generateArtificialTriggers(amplifierStruct,decision);
end



