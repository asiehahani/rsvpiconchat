%% initializeOfflineAnalysis
% Prepare the presentation for offline analysis.
%
%%

% Load the inputFilterCoef.mat file.
load 'inputFilterCoef.mat';

% Load the paramters of the program.
RSVPKeyboardParameters
signalProcessingParameters

% Load the imageList.xls file.
imageStructs = xls2Structs('iconImageList.xls');

% Initialize the triggerPartitioner struct.
triggerPartitioner.TARGET_TRIGGER_OFFSET = RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;
triggerPartitioner.sequenceEndID = imageStructs(strcmpi({imageStructs.Name},'SequenceEnd')).ID;
triggerPartitioner.fixationID = imageStructs(strcmpi({imageStructs.Name},'Fixation')).ID;
triggerPartitioner.windowLengthinSamples = round(RSVPKeyboardParams.windowDuration*fs);
triggerPartitioner.firstUnprocessedTimeIndex = 1;

% Initialise the frontendFilteringFlag variable.
frontendFilteringFlag = mainBuffer.frontendFilteringFlag;

% Set the value of the featureExtractionProcessFlow variable to a new processFlow object.
featureExtractionProcessFlow = processFlow;

% Iterate over the length of the RSVPKeyboardParams.FeatureExtraction.operatorHandles variable.
for processNodeIndex = 1:length(RSVPKeyboardParams.FeatureExtraction.operatorHandles)
    
    % Check if the RSVPKeyboardParams.FeatureExtraction.operationParameters{processNodeIndex} variable is empty.
    if isempty(RSVPKeyboardParams.FeatureExtraction.operationParameters{processNodeIndex})
        
        % Set the value of the tempNode variable to a new processNode object.
        tempNode = processNode(RSVPKeyboardParams.FeatureExtraction.operatorHandles{processNodeIndex},RSVPKeyboardParams.FeatureExtraction.operationModes(processNodeIndex));
        
    else
        
        % Set the value of the tempNode variable to a new processNode object.
        tempNode = processNode(RSVPKeyboardParams.FeatureExtraction.operatorHandles{processNodeIndex},RSVPKeyboardParams.FeatureExtraction.operationModes(processNodeIndex),RSVPKeyboardParams.FeatureExtraction.operationParameters{processNodeIndex});
        
    end
    
    % Append the tempNode variable to the featureExtractionProcessFlow object.
    featureExtractionProcessFlow.add(tempNode);
    
end

% Set the value of the crossValidationObject variable to a new crossValidation object.
crossValidationObject = crossValidation(RSVPKeyboardParams.CrossValidation);