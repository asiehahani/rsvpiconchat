%% quitRSVPKeyboard
%  this script creates and sends the stop packet to stop RSVPkeyboeard script
%
%   See also t_sendBCIpacket, closeAmps
%%
if(strcmp(amplifierStruct.DAQType,'gUSBAmp'))
    [success]=closeAmps(amplifierStruct);
end
thisPacket.header=BCIpacketStruct.HDR.STOP;
thisPacket.data=[];
[success] = t_sendBCIpacket(main2presentationCommObject,thisPacket);

delete(main2presentationCommObject);