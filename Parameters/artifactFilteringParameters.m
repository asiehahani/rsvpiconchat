%% artifactFilteringParameters
% Contains user adjustable parameter file corresponding to artifact
% filtering
%    
%    It creates the following structure,
%      
%       artifactFilteringParams: Contains the parameters for the artifactRmoval function inside artifactFiltering function.
%       The following fields are set as parameters,
%          *artifactFilteringParams.trh1-  threshold for channel rejection & interpolation section  
%          *artifactFilteringParams.trh2-  threshold for epoch rejection section
%          *artifactFilteringParams.trh3-  threshold for component rejection section 
%          *artifactFilteringParams.trh4-  threshold for channels in epochs rejection & interpolation section
%          *artifactFilteringParams.lpf1-   lowpass band for rejecting ICs
%          *artifactFilteringParams.lpf2-   lowpass band for rejecting ICs

%          *artifactFilteringParams.bufferDurationSec - Length of the buffer in seconds
%          *artifactFilteringParams.frontendFilteringFlag - Enables (1)/disables (0) the
%           frontend filtering before buffering
%

%% parameters Related to the artifactRemoval function
artifactFilteringParams.trh1=3;
artifactFilteringParams.trh2=3;
artifactFilteringParams.trh3=2.6;
artifactFilteringParams.trh3eyeBlinkDetector=3;
artifactFilteringParams.trh4=2.5;
artifactFilteringParams.lpf1=0;
artifactFilteringParams.lpf2=0.0222;

%% parameters which are required for "RSVP iconCHAT" that artifactFiltering function should recieve

% range1=3;          % range of trigger values for stimuluses: [range1,range2] 
% range2=7;        
% triggerValue.sequenceOn=1;  % trigger value for sequence on 
% trialPerSequence=5 ;  % number of trials in each sequence
% trialWindow=100;  % length of each trial in samples (approximation)
% seQuenceLength=trialPerSequence*trialWindow;     % length of each sequence in samples (approximation)

%% parameters which are required for "RSVP keyboard" that artifactFiltering function should recieve
artifactFilteringParams.trh1d=3.5;  % threshold for channel rejection & interpolation section  
% seQuenceLength=RSVPKeyboardParams.Calibration.NumberofTrials*trialWindow;
artifactFilteringParams.contaminatedSamplesPercentage=0.5;
% artifactFilteringParams.useCleanedData=0;
% artifactFilteringParams.rejectSequence=0;






