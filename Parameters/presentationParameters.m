%% presentationParameters
% Parameter file corresponding to presentation.

%% General Screen Information
% The index of the monitor to use in case multiple monitors are connected. Default (1)
presentationStruct.presentationScreenIndex=1;

%%
% Parameter to start the presentation as full screen or not. 
% 
% * 0 : Windowed
% * 1 : Full screen
%
presentationStruct.fullScreenFlag=1;

%% 
% Parameter to lock the keyboard such that only Psychtoolbox reads the keyboard input. (1 (default): Lock, 0 : Do not lock)
presentationStruct.lockKeyboard=0;

%% Presentation Durations and Duty Cyycles
% Duration (in seconds) and duty cycle of the target symbol for calibration
%
% Default duration is 2 seconds and default duty cycle is 1, which means target symbol will be shown
% for 2 seconds with no blank screen following the target symbol.
presentationStruct.Duration.Target=3;
presentationStruct.DutyCycle.Target=1;

%%
% Duration (in seconds) and duty cycle of the decision screen, which shows the decided symbol after
% a decision.
presentationStruct.Duration.StringTarget=5;
presentationStruct.DutyCycle.StringTarget=1;
presentationStruct.Duration.CompleteSentence=5;
presentationStruct.DutyCycle.CompleteSentence=1;

%%
% Duration (in seconds) and duty cycle of the fixation cross
%
% Default duration is 1 second and default duty cycle is 1, which means fixation will be shown
% for 1 second with no blank screen following the fixation symbol.
presentationStruct.Duration.Fixation=1;
presentationStruct.DutyCycle.Fixation=1;

%%
% Duration (in seconds) and duty cycle of the trials
%
% Default duration is 0.2 second and default duty cycle is 0.75, which means trial symbol will be
% shown for 150 ms followed by a 50 ms blank screen. For the trials it is not safe to set the
% duty cycle to 1.
presentationStruct.Duration.Trial=0.2;
presentationStruct.DutyCycle.Trial=0.75;

%% 
% Duration (in seconds) and duty cycle of the blank screen to separate blocks, and number of trials
% per block to split a sequence into blocks.
presentationStruct.Duration.BlankScreen=1;
presentationStruct.DutyCycle.BlankScreen=1;
presentationStruct.SequenceBreakInterval=28;

%%
% Duration (in seconds) and duty cycle of the mastery task level complete sign.
presentationStruct.Duration.MasteryTaskLevelUpSign=3;
presentationStruct.DutyCycle.MasteryTaskLevelUpSign=1;

%%
% Duration (in seconds) and duty cycle of the sequence end trigger pulse. It is only useful for trigger purposes
% to mark the end of a sequence.
presentationStruct.Duration.SequenceEnd=0.3;
presentationStruct.DutyCycle.SequenceEnd=1;

%%
% Duration (in seconds) and duty cycle of the decision screen, which shows the decided symbol after
% a decision.
presentationStruct.Duration.Decision=2;
presentationStruct.DutyCycle.Decision=1;

%%
% Duration (in seconds) and duty cycle of the pause screen which listens to keyboard to continue. These
% durations indicate the periodic flashes of the pause screen.
presentationStruct.Duration.activePauseBar=1;
presentationStruct.DutyCycle.activePauseBar=0.5;

%% Font and Style of the Text
% TextFont should be a string containing the font name. However some fonts might not be available or
% might not show up properly.
% Tested and confirmed fonts: 'Arial' (default), 'Calibri', 'Courier New'
presentationStruct.TextFont='Arial'; 

%%
% TextStyle is a number indicating the style of the text in {0,1,...,7}
%
% * 0 : normal
% * 1 : bold
% * 2 : italic
% * 4 : underline
% * 3 = 1+2 : bold & italic
% 
presentationStruct.TextStyle=0; %0=normal,1=bold,2=italic,4=underline, 1+2=bold&italic

%% 
% Text size of the stimulus (default = 300), including target, fixation and trials.
presentationStruct.StimulusTextSize=300;


%% Colors 
% Colors in RGB.
% 
% * [0,0,0] : Black
% * [255,255,255] : White
% * [0,0,255] : Blue
% * [255,0,0] : Red
% * [0,255,0] : Green
%

%% 
% Background color (default : black)
presentationStruct.backgroundColor=[255,255,255];

%%
% Stimulus color (default : white)
presentationStruct.Color.Stimulus=[0, 0, 0]; 

%%
% Target color (default : blue)
presentationStruct.Color.Target=[0 0 0];

%%
% String Target color (default : blue)
presentationStruct.Color.StringTarget=[0 0 0];

%%
% Decision color (default green)
presentationStruct.Color.Decision=[0 0 0];

%%

presentationStruct.StringTarget.TextSize=100;

%% Indicators
% Text to indicate a target, appearing on the left side of the stimulus, with adjustable text,
% enabling flag, color and size.
presentationStruct.TargetIndicator.Text='NEXT TARGET:               ';
presentationStruct.TargetIndicator.Enabled=1;
presentationStruct.TargetIndicator.Color=[0 0 0];
presentationStruct.TargetIndicator.TextSize=50;

%%
% Text to indicate a target, appearing on the left side of the stimulus, with adjustable text,
% enabling flag, color and size.
presentationStruct.CompleteSentence.Text='COMPLETE THE SENTECE';
presentationStruct.CompleteSentence.Enabled=1;
presentationStruct.CompleteSentence.Color=[0 0 0];
presentationStruct.CompleteSentence.TextSize=50;

%%
% Text to indicate a decision, appearing on the left side of the stimulus, with adjustable text,
% enabling flag, color and size.
presentationStruct.DecisionIndicator.Text='DECISION:               ';
presentationStruct.DecisionIndicator.Enabled=1;
presentationStruct.DecisionIndicator.Color=[0 0 0];
presentationStruct.DecisionIndicator.TextSize=50;

%% 
% Parameters to adjust the feedback text appearing on the top of the screen.
%
% * Neutral color represents the color scheme for neutral text (default : white)
% * Positive color represents the color scheme for correct selections (default : green)
% * Negative color represents the color scheme for incorrect selections (default : red)
% * TextSize and vSpacing adjusts the font size and vertical spacing, respectively.
% * Border represents the line which can be drawn after the feedback text.
%
presentationStruct.typingFeedbackBar.neutralColor=[0,0,0];
presentationStruct.typingFeedbackBar.positiveColor=[0,0,0];
presentationStruct.typingFeedbackBar.negativeColor=[0,0,0];
presentationStruct.typingFeedbackBar.TextSize=40;
presentationStruct.typingFeedbackBar.vSpacing=2;
presentationStruct.typingFeedbackBar.BorderEnabled=1;
presentationStruct.typingFeedbackBar.BorderInfo.width=10;
presentationStruct.typingFeedbackBar.BorderInfo.color=[0,0,0];


%%
% Parameters to adjust the pauseBar on the bottom of the screen.
presentationStruct.pauseBar.inactiveText='Press Space Bar or Enter to pause,\nEsc to quit';
presentationStruct.pauseBar.inactiveRelativeXLocation='center';
presentationStruct.pauseBar.inactiveRelativeYLocation=0.8;
presentationStruct.pauseBar.inactiveTextSize=20;
presentationStruct.pauseBar.inactiveColor=[0 0 0];
presentationStruct.pauseBar.vSpacing=1.5;
presentationStruct.pauseBar.activeText='Press Space Bar or Enter to continue,\nEsc to quit';
presentationStruct.pauseBar.activeTextSize=30;
presentationStruct.pauseBar.activeRelativeXLocation='center';
presentationStruct.pauseBar.activeRelativeYLocation='center';
presentationStruct.pauseBar.activeColor=[0 0 0];


%%
presentationStruct.decisionProbabilityFeedback.isVisible = true; % Boolean; determines if the decision probability feedback text is visible when a decision is made.
presentationStruct.decisionProbabilityFeedback.fontSize = 18; % Integer; determines the size of the decision probability feedback text (and also affects the spacing between the lines).
presentationStruct.decisionProbabilityFeedback.generalColor = [0,255,0]; % (1x3) array of integers between [0,255]; determines the color of the decision probability feedback text.
presentationStruct.decisionProbabilityFeedback.decidedColor = [0,255,255]; % (1x3) array of integers between [0,255]; determines the color of a decided item in the decision probability feedback text.
presentationStruct.decisionProbabilityFeedback.indicator = '*'; % Char; symbol used to indicate magnitude of probability.
presentationStruct.decisionProbabilityFeedback.maxIndicators = 50; % Positive integer; number of indicators that represents 100% probability.
presentationStruct.decisionProbabilityFeedback.x_shift = 0; % Real; determines the percentage of horizontal shift of the decision probability feedback text, relative to the bounds of the screen. 0% will cause the left of the text to touch the left of the screen, while 100% will cause the right of the text to touch the right of the screen.
presentationStruct.decisionProbabilityFeedback.y_shift = 50; % Real; determines the percentage of vertical shift of the decision probability feedback text, relative to the bounds of the screen. 0% will cause the top of the text to touch the top of the screen, while 100% will cause the bottom of the text to touch the bottom of the screen.