%% RSVPKeyboard
% RSVPKeyboard.m starts the RSVP Keyboard, BCI typing system. It initializes the data acquisition
% using gTec, starts the language model server (btlm) and starts a separate MATLAB session for the
% presentation. All of the signal processing and machine learning modules are handled in this
% routine. Presentation information transmitted to the presentation MATLAB. The contextual
% probabilities are updated and acquired by communicating with the language model server. See
% \Documents\RSVPKeyboardOperationManual.pdf for more detailed information on the operation of
% RSVPKeyboard system.
%%

%% Initialization of parameters
% Initializes the parameters, queries the user input for subject and session and sets up the
% recording folders and files.
global BCIframeworkDir

BCIframeworkDir='.';
addpath(genpath('.'))

RSVPKeyboardParameters

projectID='CSL_RSVPKeyboard';
subjectID=getSubjectInformation;
[sessionID,sessionName]=getSessionInformation;

setPathandFilenames

%% Initialization of data acquisition
% Initializes the data acquisition device, applies a trigger test and starts signal quality check
% GUI.
initializeDAQ
if(success==0),return,end

DAQSignalCheck
if ~continueMainBCIFlag
    [success]=stopAmps(amplifierStruct);
    [success]=closeAmps(amplifierStruct);
    return;
end

%% Initialization signal processing and machine learning
% Initializes the buffers, filters, feature extraction, language model and decision maker.
initializeSignalProcessing
initializeMachineLearning
artifactFilteringParameters
clear artifactFiltering

%% Start recording
startAcquisition

%% Start presentation MATLAB
remoteStartPresentation


continueMainLoop=true;

while continueMainLoop
    %% SignalProcessing
    % Reads the data and trigger from the amplifer applies the frontEnd filter
    % to the data and stores the data in the dataBuferObject and the trigger in
    % triggerBufferObject.
    signalProcessing(amplifierStruct,...
        dataBufferObject,...
        triggerBufferObject,...
        frontendFilter);
    
    %% Trigger Control and Partitioning
    % Based on the triggers collected in the buffer, checks the completion of the sequences and
    % partitions the sequence into trials accordingly.
    [processingStruct.triggerPartitioner.firstUnprocessedTimeIndex,processingStruct.featureExtraction.completedSequenceCount,processingStruct.featureExtraction.trialSampleTimeIndices,processingStruct.featureExtraction.trialTargetness,processingStruct.featureExtraction.trialLabels]=triggerDecoder(triggerBufferObject,processingStruct.triggerPartitioner);
    
    %% Artifacts
    % Based on the setting detects the artifacts and tries to remove them from
    % the signal.
    processingStruct.featureExtraction=artifactFiltering(amplifierStruct,...
        dataBufferObject,...
        cleanDataBufferObject,...
        artifactInfoBufferObject,...
        triggerBufferObject,...
        fs,...
        processingStruct.triggerPartitioner,...
        processingStruct.featureExtraction,...
        numberOfChannels,...
        artifactFilteringParams,...
        RSVPKeyboardParams.artifactFiltering.rejectSequence,...
        sessionID,...
        RSVPKeyboardParams.artifactFiltering.enabled);
    
    %% Feature Extraction
    % Extracts the feature(s) corresponding to trials from the filtered EEG.
    results = featureExtraction(dataBufferObject,...
        cleanDataBufferObject,...
        artifactInfoBufferObject,...
        processingStruct.triggerPartitioner,...
        processingStruct.featureExtraction,...
        RSVPKeyboardParams.artifactFiltering.useArtifactFilteredData,...
        RSVPKeyboardParams.artifactFiltering.enabled);
    %% Decision making
    % Makes decision based on the extracted features and language model. The decisions and the next
    % trials to show is sent to the presentation MATLAB via TCP/IP.
    
    [continueMainLoop,decision]=decisionMaker(results,RSVPKeyboardTaskObject,imageStructs);
    
            
    
    %% Generate artificial triggers
    % In no amplifier mode ('noAmp'), triggers are generated artificially if no input file is selected.
    if(strcmp(amplifierStruct.DAQType,'noAmp'))
        generateArtificialTriggers(amplifierStruct,decision);
    end
    
    %% Check packets from presentation
    % Checks if the RSVP Keyboard is stopped by receiving a stop packet from the presentation.
    t_receivePresentationPacket
    
    pause(0.01);
end

%% Save information and quit
saveInformation
quitRSVPKeyboard