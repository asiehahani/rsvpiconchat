addpath(genpath('..\rsvp-keyboard'))
rmpath(genpath('\Data'))

counter = 0;

dirList = dir('Data');
dirList = dirList(3:end,1);

for dirCounter = 1: size(dirList,1)
    cd('Data')
    cd(dirList(dirCounter).name);
    innerDirList = dir();
    innerDirList = innerDirList(3:end,1);
    for innerDirCounter = 1: size(innerDirList,1)
        cd(innerDirList(innerDirCounter).name);
        %         dataFoldername = ['\Data\',dirList(dirCounter).name,'\',innerDirList(innerDirCounter).name];
        dataFoldername = pwd;
        dataFile = dir('*Amp1_Run.daq');
        dataFilename = ['\',dataFile(1).name];
        disp(pwd)
        cd ..
        cd ..
        cd ..
        offlineAnalysis(1,dataFilename,dataFoldername);
        counter = counter + 1;
        
        close all
        % run your calibration code
        % save the results
        %         cd ..
        cd('Data')
        cd(dirList(dirCounter).name);
    end
    cd ..
    cd ..
end
% cd ..