% ICONLM object interface to an icon-based language model.
%
%   Given a language model and a history of selected icons, the
%   ICONLM object provides, for each icon, the probability of that
%   icon occuring next.
%
%  this = iconlm(vocabList, modelFileName)
%   Creates a new ICONLM object and calls the initLocal method to
%   load language model statistics from the given modelFileName based
%   on the allowed/requested vocabulary list.
%
%  Example: Select the most likely icon and print the resulting string.
%
%	a = iconlm(imageStructs, "crowd.stats"); % Create the ICONLM object
%	for i = 1 : 10 % Choose the next 10 most likely icons
%       s = a.sortedSymbols; % Get the next most likely icons
%       fprintf('%s', s{1}); % The next most likely icon is s{1}
%       a.update(s{1}); % Select the most likely icon
%   end
%	fprintf('\n');
%	clear a; % Destroy and clean up the ICONLM object
%
% HELP iconlm.method for additional details.
classdef iconlm < handle
    
    % Read-only properties for backwards-compatibility ONLY
    properties (GetAccess = public, SetAccess = public, Hidden = true)

        symbol; % CellStr -- strings of icon names; index is unique ID
        alloc; % Function handle for reset()
        initLocal; % Function handle for reset()
        initRemote; % Function handle for reset()
        
    end

    properties (Access = public)
        
        gramCounts; % Map -- char -> int32; icon_name -> count
        currState; % CellStr -- icon_names currently shown/chosen
        prevStates; % Cell of Cells ( { {} {} {} } ) -- past states
        
    end
    
    methods (Access = public)
        
        function this = iconlm(vocabList, modelFileName)
        % this = iconlm(vocabList, modelFileName)
        %	Creates a new ICONLM object and calls the initLocal method to
        %   load language model statistics from the given modelFileName
        %   based on the allowed/requested vocabulary list.
        %
        % Use 'clear this' to release resources and dispose of the
        % ICONLM object.
        
            % Enforce arguments
            if nargin ~= 2
                error('Invalid number of arguments');
            end
        
            % Init properties
            this.symbol = {};
            this.alloc = @this.reset;
            this.initLocal = @this.reset;
            this.initRemote = @this.reset;
            this.gramCounts = containers.Map('KeyType','char','ValueType','int32');
            this.currState = {};
            this.prevStates = {};
            
            % Store the words with associated indices/IDs
            this.symbol = cell(size(vocabList));
            for wordNum = 1 : length(vocabList)
                this.symbol{wordNum} = vocabList(wordNum).iconlmsymbols;
            end
            % Get contents of the language model file
            file = fopen(modelFileName);
            fileContents = textscan(file, '%s %s %s %s %s %s', ...
                'Delimiter', ',');
            fclose(file);
            
            % Loop through the file contents and store our
            % vocabulary (icons) and counts (statistics)
            numRows = size(fileContents{1}, 1);
            numCols = size(fileContents, 2);
            for row = 1 : numRows
                
                % Reset the gram and count for this row
                gram = {};
                count = 0;
                
                % Is this gram relevant?  If it's not empty, it should
                % contain only icons in our vocabulary.
                isRelevant = true;
                for col = 1 : numCols
                    val = fileContents{col}{row};
                    if size(strtrim(val), 2) > 0 && ...
                        (isnan(str2double(val)) || strcmp(val, 'i'))
                        if size(this.index(val), 1) == 0
                            isRelevant = false;
                            break;
                        end
                    end
                end
                if isRelevant == false
                    continue; % If this is an irrelevant gram, skip it
                end
                
                % Decode the gram and count
                for col = 1 : numCols
                    
                    val = fileContents{col}{row};
                    
                    % Is this a word?
                    if isnan(str2double(val)) || strcmp(val, 'i')
                        
                        % Skip whitespace
                        if size(strtrim(val), 2) > 0
                        
                            % Update our gram
                            gram = [gram val]; %#ok<AGROW>
                            
                        end
                    
                    % It's a parsable number
                    elseif ~isnan(str2double(val))
                        
                        % This is our count
                        count = str2double(val);
                        
                    end
                end
                
                % Store this gram and count
                this.gramCounts(iconlm.getGram(gram)) = count;
            end
        end
        
        function idx = index(this, icon)
        % idx = this.index(icon)
        %	Returns the unique identifier associated with the given icon.
        
            % Enforce arguments
            if nargin ~= 2
                error('Invalid number of arguments');
            end
        
            idx = find(strcmp(icon, this.symbol));
        end
        
        function probs = probs(this, state)
        % probs = this.probs(state)
        %   Returns an Nx1 array of type double that holds
        %   Pr(this.symbol{i}) where i <= N and the model has N
        %   icon.  These are the probabilities that this.symbol{i}
        %   will occur next, given the state.  If a state
        %   is not specified, the current state (this.currState)
        %   will be used instead.  The sum of all probabilities
        %   in the array will be 1.
        
            % Use the current state if we weren't given one
            if nargin ~= 2
                state = this.currState;
            end
            
            % Generate the combinations and counts
            probs = zeros(1, length(this.symbol));
            total = 0;
            for i = 1: length(this.symbol)
                combo = iconlm.getGram([state this.symbol{i}]);
                if isKey(this.gramCounts, combo)
                    probs(i) = this.gramCounts(combo);
                end
                total = total + probs(i);
            end
            
            % Transform the counts into probabilities
            probs = probs / total;
        end

        function reset(this)
        % this.reset
        %   Resets the object's current and past states to empty; does
        %   not affect the vocabulary or language model statistics.
            this.currState = {};
            this.prevStates = {};
        end
        
        
        function indices = sortedIndices(this, state)
        % indices = this.sortedIndices(state)
        %   Returns the icon indices for a given language model
        %   state in decending order of the likelihood of occurring
        %   next.  The state argument is optional; if it is not supplied,
        %   the current state (this.currState) will be used instead.
            
            % Use the current state if we weren't given one
            if nargin ~= 2
                state = this.currState;
            end
            
            % Generate the probabilities for each icon
            probs = this.probs(state);
            
            % Sort the icons by probabilities, descending
            [~, indices] = sort(probs, 'descend');
        end
        
        function icons = sortedSymbols(this, state)
        % icons = this.sortedSymbols(state)
        %   Returns the icons for a given language model
        %   state in decending order of the likelihood of occurring
        %   next.  The state argument is optional; if it is not supplied,
        %   the current state (this.currState) will be used instead.
            
            % Use the current state if we weren't given one
            if nargin ~= 2
                state = this.currState;
            end
            
            % Generate the probabilities for each icon
            probs = this.probs(state);
            
            % Sort the icons by probabilities, descending
            [~, sortIndex] = sort(probs, 'descend');
            icons = this.symbol(sortIndex);
        end
        
        function newState = undo(this)
        % newState = this.undo()
        %   Sets this.currState to the previous model state and
        %   returns this value. This effectively undoes/deletes the
        %   most recent symbol update.
            if isempty(this.prevStates)
                this.currState = {};
            else
                this.currState = this.prevStates{end};
                this.prevStates = this.prevStates(1 : end - 1);
            end
            newState = this.currState;
        end
        
        function newState = removeInMiddle(this, ind)
        % newState = this.removeInMiddle(ind)
        %   Sets this.currState to another state by deleting a symbol in
        %   certain place (defined by ind) in state memory and returns this
        %   value.
            this.currState(ind) = [];
            newState = this.currState;
        end
        
        function [newState K P] = update(this, icon, state)
        % [newState K P] = this.update(icon, state)
        %   Updates the language model and adds icon to the
        %   selection history.  Returns a new language model state
        %   and saves this state to this.currState. The state input
        %   parameter is optional; if not supplied, the current state
        %   (this.state) will be used instead.  K and P are unused
        %   parameters provided ONLY for backwards compatibility.
        
            % Use the current state if we weren't given one
            if nargin ~= 3
                state = this.currState;
            elseif nargin ~= 2 % Enforce minimum arguments: icon
                error('Invalid number of arguments');
            end
            
            % Unused return values
            K = 0;
            P = 0;
            
            % Is this an icon or an icon ID?  Convert to an icon
            % if necessary.
            if ~ischar(icon)
                icon = this.symbol{icon};
            end
            
            % Cache the current state as a previous state
            this.prevStates{size(this.prevStates, 2) + 1} = ...
                this.currState;
            
            % Update and return the current state
            state{size(state, 2) + 1} = icon;
            this.currState = state;
            newState = this.currState;
        end
        
    end
    
    methods (Static = true)
        
        function gram = getGram(icons)
        % gram = iconlm.getGram(icons)
        %	Returns the ordered string that uniquely
        %   identifies this combination of icons.
        %
        %  Example:
        %   getGram({'a' 'b'}) % Returns '_a_b'
        %   getGram({'c' 'a' 't'}) % Returns '_a_c_t'
        
            % Enforce arguments
            if nargin ~= 1
                error('Invalid number of arguments');
            end
        
            gram = '';
            for sym = sort(icons)
                gram = strcat(gram, '_', sym);
            end
            gram = gram{1};
        end
        
        function test()
        % iconlm.test()
        %   Runs a battery of unit tests for this module; xls2Structs()
        %   and the @iconlm test files MUST be accessible from the
        %   current path.
        %
        %  Example:
        %   iconlm.test() % Assert for failures
        
            % Reset and init
            clc;
            disp('Running tests...');
            testVocab = xls2Structs('testVocab.xls');
            testModelFileName = 'test.stats';
            
            % iconlm()
            lm = iconlm(testVocab, testModelFileName);
            assert(length(lm.symbol) == 32); % 32 icons in the XLS
            assert(strcmp(lm.symbol{1}, 'need') == 1); % First icon in XLS
            assert(strcmp(lm.symbol{32}, 'BlankScreen') == 1); % Last icon
            
            % index()
            lm = iconlm(testVocab, testModelFileName);
            assert(lm.index('eat') == 5); % According to XLS positioning
            assert(lm.index('lunch') == 9);
            
            % probs()
            lm = iconlm(testVocab, testModelFileName);
            probs = lm.probs();
            assert(length(lm.symbol) == length(probs)); % 1 prob per icon
            assert(sum(probs) == 1.0); % Probabilities add to 1.0
            
            % sortedSymbols()
            lm = iconlm(testVocab, testModelFileName);
            symbols = lm.sortedSymbols();
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'i') == 1); % Most probable: "i"
            assert(strcmp(symbols{2}, 'coffee') == 1);
            assert(strcmp(symbols{3}, 'need') == 1);
            
            % sortedSymbols(state)
            lm = iconlm(testVocab, testModelFileName);
            symbols = lm.sortedSymbols({'eat'});
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'lunch') == 1);
            assert(strcmp(symbols{2}, 'i') == 1);
            symbols = lm.sortedSymbols(); % Current state is unchanged
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'i') == 1); % Most probable: "i"
            assert(strcmp(symbols{2}, 'coffee') == 1);
            assert(strcmp(symbols{3}, 'need') == 1);
            
            % sortedIndices()
            lm = iconlm(testVocab, testModelFileName);
            indices = lm.sortedIndices();
            assert(length(lm.symbol) == length(indices)); % All icons
            assert(indices(1) == 10); % Based on XLS position of "i"
            assert(indices(2) == 2); % XLS position of "coffee"
            assert(indices(3) == 1); % XLS position of "need"
            
            % update(char)
            lm = iconlm(testVocab, testModelFileName);
            lm.update('eat');
            symbols = lm.sortedSymbols();
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'lunch') == 1);
            assert(strcmp(symbols{2}, 'i') == 1);
            probs = lm.probs();
            assert(sum(probs) == 1.0); % Total of 1.0
            assert(probs(lm.index('lunch')) == 0.5); % 50% is "lunch"
            assert(probs(lm.index('i')) == 0.5); % Other 50% is "i"
            
            % update(int)
            lm = iconlm(testVocab, testModelFileName);
            lm.update(5);
            symbols = lm.sortedSymbols();
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'lunch') == 1);
            assert(strcmp(symbols{2}, 'i') == 1);
            probs = lm.probs();
            assert(sum(probs) == 1.0); % Total of 1.0
            assert(probs(lm.index('lunch')) == 0.5); % 50% is "lunch"
            assert(probs(lm.index('i')) == 0.5); % Other 50% is "i"
            
            % undo()
            lm = iconlm(testVocab, testModelFileName);
            lm.update('eat');
            lm.undo(); % Back to base probabilities
            symbols = lm.sortedSymbols();
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'i') == 1); % Most probable: "i"
            assert(strcmp(symbols{2}, 'coffee') == 1);
            assert(strcmp(symbols{3}, 'need') == 1);
            
            % reset()
            lm = iconlm(testVocab, testModelFileName);
            lm.update('eat');
            lm.reset(); % Back to base probabilities
            symbols = lm.sortedSymbols();
            assert(length(lm.symbol) == length(symbols)); % All icons
            assert(strcmp(symbols{1}, 'i') == 1); % Most probable: "i"
            assert(strcmp(symbols{2}, 'coffee') == 1);
            assert(strcmp(symbols{3}, 'need') == 1);
            
            % iconlm.getGram()
            gram = iconlm.getGram({'a' 'b'}); % Commented examples
            assert(strcmp(gram, '_a_b') == 1);
            gram = iconlm.getGram({'c' 'a' 't'});
            assert(strcmp(gram, '_a_c_t') == 1);
            
            % Success
            disp('success!');
        end
        
    end
    
end