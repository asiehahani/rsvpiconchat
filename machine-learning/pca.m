classdef pca < handle
    % Principal Component Analysis
    %
 
    
    properties (SetAccess = private)
        minimumRelativeEigenvalue=0;
        numberOfPrincipalComponents=[];
    
        projectionMatrix
    end
    
    
    methods
        function self=pca(parameters)
            %parameters is the external parameter struct for the model. It may
            %contain parameters.lambda to set the shrinkage parameter (default = 1) and
            %parameters.gamma to set the regularization parameter (default = 1).
            %parameters.positiveLabel represent the label of the positive
            %class for the two class case (same type as the label vector elements). (default = 1)
            if(nargin > 0)
                if(isfield(parameters,'minimumRelativeEigenvalue'))
                    self.minimumRelativeEigenvalue=parameters.minimumRelativeEigenvalue;
                end
                if(isfield(parameters,'numberOfPrincipalComponents'))
                    self.numberOfPrincipalComponents=parameters.numberOfPrincipalComponents;
                end
%                 if(isfield(parameters,'positiveLabel'))
%                    self.positiveLabel=parameters.positiveLabel; 
%                 ends
            end
            
            
        end
        
        function learn(self,data,extra)
            % learn(data,labels,parameters) function learns the model, i.e. trains PCA, from the data given
            %data is a d x N matrix, where d
            % is the number of dimensions and N is the number of samples. data
            % is used to learn the model corresponding to given parameters.
            %
            
            data=bsxfun(@minus,data,mean(data,2));
            [U,E]=eig(data*data');
            
            eigenvalues=diag(E);
            
            
            selectedEigenvalues=eigenvalues(end)*self.minimumRelativeEigenvalue<=eigenvalues;
            
            selectedEigenvalues(1:end-self.numberOfPrincipalComponents)=false;
            
            self.projectionMatrix=U(:,selectedEigenvalues).';            
            
        end
        
        function output=operate(self,data)
            % operate(data) function tests or projects the new samples given by
            % data. It requires that the learn function had run.
            %
            %INPUT data is a d x N matrix, where d
            % is the number of dimensions and N is the number of samples. data
            % is used to learn the model corresponding to given parameters.
            %
            %INPUT output is an 1 x N vector containing the scores obtained after testing.
            
            %THIS IS GOING TO CONTAIN THE OPERATION, i.e TESTING, PROJECTION
            
            output=self.projectionMatrix*data;
        end
        
    end        
        
    

    
    
end
