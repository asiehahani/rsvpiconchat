classdef downsampleObject < handle
    properties 
        downsampleOrder=2;
        
    end
    
    methods
        function self=downsampleObject(downsampleOrder)
            if(nargin==1)
            self.downsampleOrder=downsampleOrder;
            end
        end
        
        function learn(self,data,extra)
            
        end
        
        function output=operate(self,data)
            output=downsample(data,self.downsampleOrder);
        end
    end
end