%% initializeRSVPKeyboardPresentation
% Initialization part of the RSVPKeyboardPresentation. It sets up the TCP/IP communication to
% receive information from the main side or to send stop command. It sets up the parameters and
% initializes the Psychtoolbox.
%%

global BCIpacketStruct
global presentation2mainCommObject

addpath(genpath('..\.'));

presentationParameters
RSVPKeyboardParameters

presentationInfo=presentationStruct;

if(~standaloneFlag)
    tcpipParameters
    [success,presentation2mainCommObject,BCIpacketStruct]=t_presentation2mainCommInitialize(RSVPKeyboardParams.mainIP,RSVPKeyboardParams.mainPort);
end
presentationInfo.imageStructs=xls2Structs('iconImageList.xls');


% presentationInfo.imageStructs=xls2Structs('imageList.xls');

presentationInfo.blankScreen.Type='Blank';

presentationQueue=linkedList;

importantKeys.list={'esc','space','return'};
importantKeys.code=KbName(importantKeys.list);
importantKeys.actionValue=[0,1,1];
if(presentationInfo.lockKeyboard)
ListenChar(2);
end
PsychHID('KbQueueCreate');
PsychHID('KbQueueStart');

AssertOpenGL;

presentationInfo.screens=Screen('Screens');
if(length(presentationInfo.screens)==1)
    presentationInfo.screenNumber=presentationInfo.screens;
else
    if(presentationStruct.presentationScreenIndex<=max(presentationInfo.screens))
        presentationInfo.screenNumber=presentationStruct.presentationScreenIndex;
    else
        presentationInfo.screenNumber=max(presentationInfo.screens);
    end
end

% This preference setting selects the high quality text renderer on
% each operating system: It is not really needed, as the high quality
% renderer is the default on all operating systems, so this is more of
% a "better safe than sorry" setting.
Screen('Preference', 'TextRenderer', 1);

presentationInfo.resolution=Screen('Resolution', presentationInfo.screenNumber);

Screen('Preference', 'VisualDebugLevel', 3);  %Hides Psychtoolbox entry screen

if(presentationStruct.fullScreenFlag==0)
    presentationInfo.windowRect=[presentationInfo.resolution.width/4,presentationInfo.resolution.height/4,presentationInfo.resolution.width*3/4,presentationInfo.resolution.height*3/4];
else
    presentationInfo.windowRect=[];
    HideCursor(presentationInfo.screenNumber);
end
[presentationInfo.window,presentationInfo.windowRect]= Screen('OpenWindow',presentationInfo.screenNumber,presentationStruct.backgroundColor,presentationInfo.windowRect);

presentationInfo.interFlipInterval = Screen('GetFlipInterval',presentationInfo.window);
Screen('TextFont',presentationInfo.window, presentationInfo.TextFont);
Screen('TextStyle', presentationInfo.window, presentationInfo.TextStyle);

presentationInfo.imageTextures=makeTextures(presentationInfo.imageStructs,presentationInfo.window);

initializeParallelPortTriggerSender(RSVPKeyboardParams.parallelPortIOList);

presentationInfo.TARGET_TRIGGER_OFFSET=RSVPKeyboardParams.TARGET_TRIGGER_OFFSET;

% presentationInfo.adjustedTargetDurations=getAdjustedDuration(presentationInfo.targetDuration,presentationInfo.interFlipInterval,presentationInfo.dutyCycle);
% presentationInfo.adjustedFixationDurations=getAdjustedDuration(presentationInfo.fixationDuration,presentationInfo.interFlipInterval,presentationInfo.dutyCycle);
% presentationInfo.adjustedTrialDurations=getAdjustedDuration(presentationInfo.trialDuration,presentationInfo.interFlipInterval,presentationInfo.dutyCycle);

%sca
% blankScreenNode=screenNode(presentationInfo.window,presentationInfo.interFlipInterval);
% presentationQueue.insertEnd(listNode(blankScreenNode));
