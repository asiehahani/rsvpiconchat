%% function insertStimulusNode(presentationQueue,stimulusStruct,presentationInfo,stimulusType,backgroundNode)
% Initialises and inserts a new stimulus node into the specified presentation queue, using the provided data.
%
%   The inputs of the function:
%
%       presentationQueue - Object, queue which contains the nodes being processed by the presentation. The new stimulus node is added to this queue.
%
%       stimulusStruct - Struct, contains information about the stimulus.
%
%       presentationInfo - Struct, contains information about the presentation.
%
%       stimulusType - String, labels the stimulus type.
%
%       backgroundNode - Object, contains information pertaining to a graphical representation of the stimulus.
%
%%

function insertStimulusNode(presentationQueue,stimulusStruct,presentationInfo,stimulusType,backgroundNode)

    % Initialise the duration of the stimulus.
    duration = presentationInfo.Duration.(stimulusType);
    
    % Sets the values of the variable active and the variable paasive as determined by the getAdjustedDuration function.
    [active,passive] = getAdjustedDuration(duration,presentationInfo.interFlipInterval,presentationInfo.DutyCycle.(stimulusType));

    % Check if the variable active is not equal to 0.
    if active ~= 0
        
        % Sets the value of the variable mainScreenNode to a screenNode object.
        mainScreenNode = screenNode(presentationInfo.window,presentationInfo.interFlipInterval,active);
        
        % Checks if the stimulusType variable has the value 'activePauseBar' (ignoring case) or if the stimulusStruct struct is not empty.
        if strcmpi(stimulusType,'activePauseBar') || ~isempty(stimulusStruct)
            
            if isfield(stimulusStruct,'Type')
                stimulusBased=stimulusStruct.Type;
            else
                stimulusBased='Non';
            end
            
            if strcmpi(stimulusBased,'Image')
               tempElement=imageElement(stimulusType,stimulusStruct,presentationInfo);
            else
                % Sets the value of the tempElement variable to a new textElement object.
                tempElement = textElement(stimulusType,stimulusStruct,presentationInfo);
            end
            
            % Adds the tempElement object as a graphic element to the mainScreenNode object.
            mainScreenNode.addGraphicElement(tempElement);

            % Checks if the backgroundNode object is not empty.
            if ~isempty(backgroundNode)
                
                % Adds the backgroundNode object as a graphic element to the mainScreenNode object.
                mainScreenNode.addGraphicElement(backgroundNode);
                
            end

            % Checks the value of the stimulusType variable.
            switch stimulusType
                
                case 'Target' % If the value is 'Target', the stimulus node is given a target trigger value.
                    
                    % Sets the trigger value of the mainScreenNode object to the target trigger value, as designated by (stimulusStruct.ID + presentationInfo.TARGET_TRIGGER_OFFSET).
                    mainScreenNode.setTriggerValue(stimulusStruct.ID + presentationInfo.TARGET_TRIGGER_OFFSET);

                case {'Trial','Fixation'} % If the value is 'Trial' or 'Fixation', the stimulus node is given a default trigger value.
                    
                    % Sets the trigger value of the mainScreenNode object to the default trigger value, as designated by stimulusStruct.ID.
                    mainScreenNode.setTriggerValue(stimulusStruct.ID);

                otherwise
                    
                    % Do nothing.
                    
            end
            
            presentationQueue.insertEnd(listNode(mainScreenNode));
            
        end
         
    end
    
    % Check if the variable passive is not equal to 0.
    if passive ~= 0
        
        % Sets the value of the variable mainScreenNode to a screenNode object.
        mainScreenNode = screenNode(presentationInfo.window,presentationInfo.interFlipInterval,passive);
        
        % Checks if the backgroundNode object is not empty.
        if ~isempty(backgroundNode)
            
            % Appends the backgroundNode object as a graphic element to the mainScreenNode object.
            mainScreenNode.addGraphicElement(backgroundNode);
            
        end
        
        % Casts the mainScreenNode object to a listNode object and then appends it to the presentationQueue object.
        presentationQueue.insertEnd(listNode(mainScreenNode));

    end
    
end




% function insertStimulusNode(presentationQueue,stimulusStruct,presentationInfo,stimulusType)
% [active,passive]=getAdjustedDuration(presentationInfo.Duration.(stimulusType),presentationInfo.interFlipInterval,presentationInfo.Duration.DutyCycle);
% if(active~=0)
%     stimulusStruct.Duration=active;
%     presentationQueue.insertEnd(listNode(stimulusStruct));
% end
% if(passive~=0)
%     S=presentationInfo.blankScreen;
%     S.Duration=passive;
%     presentationQueue.insertEnd(listNode(S));
%
% end