%% RSVPKeyboardPresentation
% Controls the presentation side of the RSVP Keyboard. It is a script that runs on a separate
% Matlab, communicates with main part of RSVP Keyboard and does the presentation using Psychtoolbox.
% To be able to communicate with the main part standaloneFlag should be set to 0. It updates the
% items to be shown on the screen according to the information coming from the main side.
%%

if(~exist('standaloneFlag','var'))
    standaloneFlag=1;
end
try
    initializeRSVPKeyboardPresentation
    currentStimulusNode=presentationQueue.Head;
    
    tic;
    presentationContinueFlag=true;
    isPausedFlag=true;
    while presentationContinueFlag
        if(~isempty(currentStimulusNode))
            currentStimulusNode=processPresentationQueue(currentStimulusNode);
        else
            
            currentStimulusNode=presentationQueue.Tail;
            if(~isempty(currentStimulusNode))
                presentationQueue.removeBefore(currentStimulusNode.Prev);
            end
            
            action=checkKeyboardOperations(importantKeys);
            switch action
                case 0 % Escape
                    presentationContinueFlag=0;
                    
                case 1 % Space OR Enter/Return
                    isPausedFlag = ~isPausedFlag;
                    if(isPausedFlag)
                        [presentationContinueFlag,~] = updatePresentationQueue(presentationQueue,presentationInfo,'pause',standaloneFlag);
                    end
                    
                case 2 % No key
                    if(isPausedFlag)
                        [presentationContinueFlag,~] = updatePresentationQueue(presentationQueue,presentationInfo,'pause',standaloneFlag);
                    else
                        [presentationContinueFlag,isPausedFlag] = updatePresentationQueue(presentationQueue,presentationInfo,'sequence',standaloneFlag);
                    end
            end
            if(isempty(currentStimulusNode))
                currentStimulusNode=presentationQueue.Head;
            end
        end
    end
    quitPresentation
    
    
catch exception
    logError(exception,0);
    quitPresentation
    
    
end