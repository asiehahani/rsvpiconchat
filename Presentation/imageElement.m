classdef imageElement < handle
    properties
        XPosition='center'
        YPosition='center'
        image
        imageRect
        imgAdress
        windowRect
        imgSize
        XCursor
        YCursor
        windowHandle
        Parent
        Children
        mood
        tex
        first
        targetLocations
        decisionLocations
    end
    methods
        function self=imageElement(type,imageAdress,presentationInfo)
            self.Children=linkedList;
            if(nargin>0)
                self.windowHandle=presentationInfo.window;
                self.windowRect=presentationInfo.windowRect;
                switch type
                    case {'LevelUpSign'}
                        if isfield(presentationInfo,'imageRectSize')
                            self.imgSize=presentationInfo.imageRectSize;
                        else
                            self.imgSize=150;
                            
                        end
                        if(ischar(imageAdress))
                            self.imgAdress=imageAdress;
                        else
                            self.imgAdress=imageAdress.Stimulus;
                        end
                        
                        %                         self.image = imread(self.imgAdress);
                        self.image=presentationInfo.imageTextures.image{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        self.tex=presentationInfo.imageTextures.tex{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        
                    case {'Trial','Stimulus','Fixation','Target','Decision','StringTarget'}
                        if isfield(presentationInfo,'imageRectSize')
                            self.imgSize=presentationInfo.imageRectSize;
                        else
                            self.imgSize=presentationInfo.StimulusTextSize;
                            
                        end
                        if(ischar(imageAdress))
                            self.imgAdress=imageAdress;
                        else
                            self.imgAdress=imageAdress.Stimulus;
                        end
                        %                         a=imageAdress.Stimulus;
                        self.image=presentationInfo.imageTextures.image{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        self.tex=presentationInfo.imageTextures.tex{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        
                    case 'typingFeedback'
                        if isfield(presentationInfo,'imageRectSize')
                            self.imgSize=presentationInfo.imageRectSize;
                        else
                            self.imgSize=presentationInfo.StimulusTextSize;
                            
                        end
                        
                        if(ischar(imageAdress))
                            self.imgAdress=imageAdress;
                        else
                            self.imgAdress=imageAdress.Stimulus;
                        end
                        self.imgSize=150;
                        
                        switch imageAdress.mood
                            case 'positive'
                                self.mood=1; 
                                if isfield(imageAdress,'targetLocations')
                                    self.targetLocations=imageAdress.targetLocations;
                                end
                         
                            case 'negative'
                                self.mood=2; 
                              
                            case 'next'
                                self.mood=3;
                                if imageAdress.first==1
                                    self.first=1;
                                else
                                    self.first=0;
                                end
                          
                            case 'neutral'
                                self.mood=0;
                                       
                            case 'nextNegative'
                                if isfield(imageAdress,'decisionLocations')
                                    self.decisionLocations=imageAdress.decisionLocations;
                                end
                                self.mood=4;
                              
                            case 'nextPositive'
                                if isfield(imageAdress,'decisionLocations')
                                    self.decisionLocations=imageAdress.decisionLocations;
                                end
                                self.mood=5;
                         
%                             case 'targetStates'
%                               self.mood=6;
% % %                               self.targetLocations=imageAdress.targetLocations;
                                
                        end
                        self.XPosition=0;
                        self.YPosition=0;        
                        self.image=presentationInfo.imageTextures.image{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        self.tex=presentationInfo.imageTextures.tex{strcmpi(presentationInfo.imageTextures.Address,self.imgAdress)};
                        
                        
                end
            end
        end
        
        function addGraphicElement(self,element)
            self.Children.insertEnd(listNode(element));
            element.Parent=self;
        end
        
        function Draw(self)
            if(~isempty(self.Parent))
                switch class(self.Parent)
                    case 'imageElement'
                        self.XPosition=self.Parent.XCursor;
                        self.YPosition=self.Parent.YCursor;
                end
            end
            if ~isnumeric(self.XPosition)
                rect=round(self.windowRect/2);
                if ischar(self.imgSize)
                    self.imgSize=size(self.image);
                end
                rect=rect(3:end);
                if(isempty(self.imgAdress))
                    self.XCursor=self.XPosition;
                    self.YCursor=self.YPosition;
                    self.imageRect=[self.XCursor,self.YCursor,self.XCursor,self.YCursor];
                else
                     rect=rect-round(self.imgSize/2);
                     self.imageRect=[rect,rect+self.imgSize];
                    Screen('DrawTexture', self.windowHandle, self.tex, [],self.imageRect);
                    
                    
                end
            else
                switch self.mood                        
                            
                    case 3
                        if self.first==1
                        self.imageRect(1,1)= 0;
                        self.imageRect(1,3)= self.imgSize;
                        self.imageRect(1,2)= self.imgSize;
                        self.imageRect(1,4)= 2*self.imgSize;
                        self.XCursor=self.imgSize;
                        self.YCursor=self.imgSize;
                        else
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.imgSize+self.YPosition;
                        self.XCursor=self.imgSize+self.XPosition;
                        self.YCursor=self.imgSize;
                        end
                    case 4
                        if ~isempty(self.decisionLocations)
                          switch self.decisionLocations
                                    case 'verb'
                                        self.XPosition=2*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'subject'
                                        self.XPosition=0;
                                        self.YPosition=self.imgSize;
                                    case 'object'
                                        self.XPosition=4*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'objmod'
                                        self.XPosition=3*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'subjmod'
                                        self.XPosition=self.imgSize;
                                        self.YPosition=self.imgSize;
                         end
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;        
                        self.XCursor=0;
                        self.YCursor=0;
                        else
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;
                        self.XCursor=self.imgSize+self.XPosition;
                        self.YCursor=self.imgSize;
                        end
                    case 5
                        if ~isempty(self.decisionLocations)
                          switch self.decisionLocations
                                    case 'verb'
                                        self.XPosition=2*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'subject'
                                        self.XPosition=0;
                                        self.YPosition=self.imgSize;
                                    case 'object'
                                        self.XPosition=4*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'objmod'
                                        self.XPosition=3*self.imgSize;
                                        self.YPosition=self.imgSize;
                                    case 'subjmod'
                                        self.XPosition=self.imgSize;
                                        self.YPosition=self.imgSize;
                         end
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;        
                        self.XCursor=0;
                        self.YCursor=0;
                        else
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;
                        self.XCursor=self.imgSize+self.XPosition;
                        self.YCursor=self.imgSize;
                        end
                    otherwise

                        if ~isempty(self.targetLocations)
                          switch self.targetLocations
                                    case 'verb'
                                        self.XPosition=2*self.imgSize;
                                        self.YPosition=0;
                                    case 'subject'
                                        self.XPosition=0;
                                        self.YPosition=0;
                                    case 'object'
                                        self.XPosition=4*self.imgSize;
                                        self.YPosition=0;
                                    case 'objmod'
                                        self.XPosition=3*self.imgSize;
                                        self.YPosition=0;
                                    case 'subjmod'
                                        self.XPosition=self.imgSize;
                                        self.YPosition=0;
                         end
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;        
                        self.XCursor=0;
                        self.YCursor=0;
                        else
                        self.imageRect(1,1)= self.XPosition;
                        self.imageRect(1,3)= self.imgSize+self.XPosition;
                        self.imageRect(1,2)= self.YPosition;
                        self.imageRect(1,4)= self.YPosition+self.imgSize;
                        self.XCursor=self.imgSize+self.XPosition;
                        self.YCursor=0;
                        end
                        
                end
                Screen('DrawTexture', self.windowHandle, self.tex, [],self.imageRect);
                
                
                
            end
            
            currentNode=self.Children.Head;
            for(elementIndex=1:self.Children.elementCount)
                currentNode.Data.Draw;
                currentNode=currentNode.Next;
            end
        end
        
        function copyObject=copy(self)
            copyObject=imageElement;
            S=properties(self);
            for(propertyIndex=1:length(S))
                copyObject.(S{propertyIndex})=self.(S{propertyIndex});
            end
            
            if(~isempty(self.Children))
                copyObject.Children=linkedList;
                currentNode=self.Children.Head;
                while(~isempty(currentNode))
                    copyObject.Children.insertEnd(listNode(copy(currentNode.Data)));
                    copyObject.Children.Tail.Data.Parent=copyObject;
                    currentNode=currentNode.Next;
                end
            end
        end
        
    end
end