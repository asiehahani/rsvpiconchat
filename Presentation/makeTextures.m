function imageTextures=makeTextures(imageStructs,window)

ind= strcmpi({imageStructs.Type},'Image');
imageTextures.Address={imageStructs(ind).Stimulus};
for count=1:length(imageTextures.Address)
    image=imread(imageTextures.Address{count});
    imageTextures.image{count}=image;
    imageTextures.tex{count}=Screen('MakeTexture', window, image);
end


