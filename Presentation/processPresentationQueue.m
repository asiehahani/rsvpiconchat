%% nextStimulusNode=processPresentationQueue(currentStimulusNode)
% Processes the presentation queue and displays currentStimulusNode
% Input:    currentStimulusNode - screenNode to be presented
% Output:   nextStimulsNode - the screenNode which is to be presented as next
%%
function nextStimulusNode=processPresentationQueue(currentStimulusNode)
currentStimulusNode.Data.Draw;
currentStimulusNode.Data.Flip;
sendTrigger(currentStimulusNode.Data.triggerValue);

nextStimulusNode=currentStimulusNode.Next;
if(isempty(nextStimulusNode))
    currentStimulusNode.Data.setFlipTime([]);
else
    currentStimulusNode.Data.setFlipTime(nextStimulusNode.Data);
end
